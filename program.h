/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef PROGRAM_H_
#define PROGRAM_H_

#include <glib.h>
#include <glib-object.h>

#include "vector.h"
#include "cbuf.h"
#include "filecache.h"

G_BEGIN_DECLS

typedef enum {
    SPECTRA_PROGRAM_LINK_FAILURE,
    SPECTRA_PROGRAM_EXEC_FAILURE,
    SPECTRA_PROGRAM_MISSING_INDEPENDENT_VARIABLES,
    SPECTRA_PROGRAM_NON_REAL
} SpectraProgramError;

enum spectra_program_state {
    SPECTRA_PROGRAM_STATE_INITIALIZED,
    SPECTRA_PROGRAM_STATE_COMPILE_IN_PROGRESS,
    SPECTRA_PROGRAM_STATE_COMPILED,
    SPECTRA_PROGRAM_STATE_COMPILE_FAILED
};

#define PROGRAM_ADDR_INVALID (~0)

#define SPECTRA_TYPE_PROGRAM_CONTEXT (spectra_program_context_get_type())
G_DECLARE_FINAL_TYPE(SpectraProgramContext, spectra_program_context,
                     SPECTRA, PROGRAM_CONTEXT, GObject)

#define SPECTRA_TYPE_PROGRAM (spectra_program_get_type())
G_DECLARE_FINAL_TYPE(SpectraProgram, spectra_program,
                     SPECTRA, PROGRAM, GObject)

void spectra_program_print(SpectraProgram *prog);

//SpectraProgram *spectra_program_new(void);
SpectraProgram *spectra_program_new(SpectraConstantBuffer *cbuf);

/*******************************************************************************
 **
 ** declares a new input variable and returns the index where it can be sourced
 ** from.  If the input variable already exists then its pre-existing index is
 ** returned.  the only difference between this function and
 ** spectra_program_input_index is that spectra_program_input_index returns
 ** PROGRAM_ADDR_INVALID for non-existant inputs instead of creating them.
 **
 ** do note that this function does not take any effects of
 ** INST_OP_PUSH_INPUT/INST_OP_POP_INPUT into effect.
 **
 ******************************************************************************/
guint spectra_program_declare_input(SpectraProgram *prog, gchar const *sym);

guint spectra_program_input_count(SpectraProgram const *prog);

// allocates a new memory cell and returns its address (index)
guint spectra_program_mem(SpectraProgram *prog);

// loads the given literal into memory and returns the address
guint spectra_program_literal(SpectraProgram *prog, SpectraVector *valp);
guint spectra_program_symbol(SpectraProgram *prog, gchar const *sym_name);
guint spectra_program_load_input(SpectraProgram *prog, gchar const *input_name);

// returns the index of the given independent variable
guint spectra_program_input_index(SpectraProgram *prog, gchar const *sym);

gchar const *spectra_program_input_name(SpectraProgram *prog, guint index);

guint spectra_program_add(SpectraProgram *prog, guint lhs, guint rhs);
guint spectra_program_mult(SpectraProgram *prog, guint lhs, guint rhs);
guint spectra_program_div(SpectraProgram *prog, guint dividend, guint divisor);
guint spectra_program_exp(SpectraProgram *prog, guint logarithm);
guint spectra_program_ln(SpectraProgram *prog, guint val);

guint spectra_program_sample_table(SpectraProgram *prog,
                                   guint table_index, guint time);
guint spectra_program_sample_vector_table(SpectraProgram *prog,
                                          guint table_index, guint time);

void spectra_program_state_transition(SpectraProgram *prog,
                                      enum spectra_program_state state);

gboolean spectra_program_check_state(SpectraProgram const *prog,
                                     enum spectra_program_state expect);

guint spectra_program_terminate(SpectraProgram *prog, guint output_index);

guint spectra_program_output_index(SpectraProgram const *prog);

void spectra_program_push_input(SpectraProgram *prog, gchar const *name, guint value);
void spectra_program_pop_input(SpectraProgram *prog);

/*******************************************************************************
 **
 ** function invokation
 ** ident is the name of the function to invoke
 ** args is an array of guints which represent indices to memory locations of
 ** arguments.
 **
 ******************************************************************************/
guint spectra_program_invoke(SpectraProgram *prog,
                             gchar const *ident,
                             GArray *args);

int spectra_program_context_link_full(SpectraProgramContext *ctxt,
                                      SpectraProgram *prog,
                                      GError **error,
                                      GPtrArray *initializers);

/*
 * convenience function that builds an array of initializers for
 * you then calls spectra_program_env_link_full.
 *
 * arguments: name, followed by prog_sym_tp then either a spectra_scalar or a
 *            function depending on what the symbol type was
 *            terminate with a NULL name.
 */
int spectra_program_context_link(SpectraProgramContext *ctxt,
                                 SpectraProgram *prog,
                                 GError **error,
                                 ...);


SpectraProgramContext *spectra_program_context_new_full(guint mem_sz,
                                                        guint func_tbl_sz);
SpectraProgramContext *spectra_program_context_new(SpectraProgram *prog);

SpectraVector* spectra_program_execute(SpectraProgram *prog,
                                       SpectraProgramContext *ctxt,
                                       GPtrArray *inputs, GError **err);

// returns TRUE if the program is able to accept new instructions
gboolean spectra_program_compiling(SpectraProgram *prog);

gboolean spectra_program_can_execute(SpectraProgram *prog);

/*******************************************************************************
 **
 ** this does not return a new reference; you will have to call
 ** g_object_ref yourself.  In retrospect i probably should've made it
 ** return a new reference.
 **
 ******************************************************************************/
SpectraVector* spectra_program_context_read_vec(SpectraProgramContext const *ctxt,
                                                guint addr);

/*******************************************************************************
 **
 ** this function reads the vector from the given address and converts
 ** it to a real number using spectra_vector_as_real.  If the given
 ** value cannot be represented as a real number, NULL will be
 ** returned.
 **
 ** if the value returned is not NULL, then it will be a new reference
 ** which the caller needs to unref themself.  This is
 ** currently contrary to spectra_program_context_read_vec's refcount
 ** semantics, which do not return a new reference (in retrospect i
 ** should have made it return a new refernce).
 **
 ******************************************************************************/
SpectraVector* spectra_program_context_read_real(SpectraProgramContext const *ctxt,
                                                 guint addr);


/*******************************************************************************
 **
 ** returns the address of the given string literal.  If it is not already in
 ** the program's string table then it will be added.
 **
 ******************************************************************************/
guint spectra_program_string_literal(SpectraProgram *prog, gchar const *str);
gchar const *spectra_program_string_literal_index(SpectraProgram *prog, guint index);

SpectraFile *spectra_program_context_load_file(SpectraProgramContext *ctxt,
                                               gchar const *path, GError **err);

/*******************************************************************************
 **
 ** adds the given tabel to the given program and returns the index which it may
 ** be referenced by.
 **
 ******************************************************************************/
guint spectra_program_add_table(SpectraProgram *prog, GBytes *tbl,
                                spectra_scalar sample_frequency);

guint spectra_program_add_vector_table(SpectraProgram *prog, GPtrArray *tbl,
                                       spectra_scalar sample_frequency);

enum spectra_program_argument_class {
    SPECTRA_PROGRAM_MEMORY,
    SPECTRA_PROGRAM_STRING
};

struct spectra_program_argument {
    enum spectra_program_argument_class tp;
    guint index;
};


/*******************************************************************************
 **
 ** ownership of the returned SpectraVector* reference is transferred to the
 ** caller
 **
 ******************************************************************************/
typedef
SpectraVector*(*spectra_function)(SpectraProgram*,SpectraProgramContext*,guint,
                                  struct spectra_program_argument const*,
                                  GError **err);

G_END_DECLS

#endif
