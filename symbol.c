/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include "symbol.h"

static void prog_sym_free(struct prog_sym *sym) {
    g_free(sym);
}

struct prog_sym *prog_sym_new(void) {
    struct prog_sym *sym = g_malloc0(sizeof(struct prog_sym));
    g_ref_count_init(&sym->cnt);
    return sym;
}

struct prog_sym *prog_sym_ref(struct prog_sym *sym) {
    g_ref_count_inc(&sym->cnt);
    return sym;
}

void prog_sym_unref(struct prog_sym *sym) {
    if (g_ref_count_dec(&sym->cnt))
        prog_sym_free(sym);
}

struct symbol_table *symbol_table_new(void) {
    struct symbol_table *tbl = g_malloc(sizeof(struct symbol_table));
    g_ref_count_init(&tbl->cnt);
    tbl->syms = g_ptr_array_new();
    return tbl;
}

static void symbol_table_unref_sym(gpointer sym_void, gpointer user_dat) {
    struct prog_sym *symp = sym_void;
    prog_sym_unref(symp);
}

static void symbol_table_free(struct symbol_table *tbl) {
    g_ptr_array_foreach(tbl->syms, symbol_table_unref_sym, NULL);
    g_ptr_array_unref(tbl->syms);
    g_free(tbl);
}

struct symbol_table *symbol_table_ref(struct symbol_table *tbl) {
    g_ref_count_inc(&tbl->cnt);
    return tbl;
}

void symbol_table_unref(struct symbol_table *tbl) {
    if (g_ref_count_dec(&tbl->cnt))
        symbol_table_free(tbl);
}

static gboolean check_symbol_name(gconstpointer sym_void, gconstpointer name_void) {
    struct prog_sym const *sym = sym_void;
    gchar const *name = name_void;
    return strncmp(name, sym->name, PROG_SYM_MAX) == 0;
}

static int symbol_table_query_index(struct symbol_table *tbl,
                                    char const *symbol_name) {
    guint index;
    if (g_ptr_array_find_with_equal_func(tbl->syms, symbol_name,
                                         check_symbol_name, &index)) {
        return index;
    } else {
        return -1;
    }
}

struct prog_sym *symbol_table_query(struct symbol_table *tbl,
                                    char const *symbol_name) {
    int index = symbol_table_query_index(tbl, symbol_name);
    if (index >= 0)
        return g_ptr_array_index(tbl->syms, index);
    else
        return NULL;
}

void symbol_table_add_symbol(struct symbol_table *table,
                             struct prog_sym *symbol) {
    g_ptr_array_add(table->syms,  prog_sym_ref(symbol));
}

void symbol_table_foreach(struct symbol_table *table,
                          GFunc function, gpointer user_data) {
    g_ptr_array_foreach(table->syms, function, user_data);
}

struct prog_sym *symbol_table_index_const(struct symbol_table const *table, unsigned index) {
    return g_ptr_array_index(table->syms, index);
}

struct prog_sym *symbol_table_index(struct symbol_table *table, unsigned index) {
    return g_ptr_array_index(table->syms, index);
}

unsigned symbol_table_len(struct symbol_table const *table) {
    return table->syms->len;
}

