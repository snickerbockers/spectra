/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <stdarg.h>
#include <math.h>

#include "vector.h"

#include "cbuf.h"

#define SPECTRA_CBUF_ERROR (spectra_cbuf_error_quark())
G_DEFINE_QUARK(spectra-cbuf-error-quark, spectra_cbuf_error)

struct spectra_constant {
    enum spectra_constant_type tp;
    gchar ident[SPECTRA_CONSTANT_MAX];
    guint index;
};

struct spectra_constant_val {
    enum spectra_constant_type tp;
    union {
        gpointer function;
        SpectraVector *vecp;
    };
};

struct _SpectraConstantBuffer {
    GObject parent;
    GArray *syms;
    GArray *vals;
};

G_DEFINE_FINAL_TYPE(SpectraConstantBuffer,
                    spectra_constant_buffer, G_TYPE_OBJECT)

static void spectra_constant_buffer_clear_value(gpointer ptr) {
    struct spectra_constant_val *valp = ptr;
    if (valp->tp == SPECTRA_CONSTANT_TYPE_VECTOR && valp->vecp)
        g_object_unref(valp->vecp);
}

static void spectra_constant_buffer_init(SpectraConstantBuffer *cbuf) {
    cbuf->syms = g_array_new(FALSE, FALSE, sizeof(struct spectra_constant));
    cbuf->vals = g_array_new(FALSE, FALSE, sizeof(struct spectra_constant_val));
    g_array_set_clear_func(cbuf->vals, spectra_constant_buffer_clear_value);
}

static void spectra_constant_buffer_finalize(GObject *gobj) {
    SpectraConstantBuffer *cbuf = SPECTRA_CONSTANT_BUFFER(gobj);
    g_array_unref(cbuf->syms);
    g_array_unref(cbuf->vals);
    G_OBJECT_CLASS(spectra_constant_buffer_parent_class)->finalize(gobj);
}

static void
spectra_constant_buffer_class_init(SpectraConstantBufferClass *class) {
    G_OBJECT_CLASS(class)->finalize = spectra_constant_buffer_finalize;
}

SpectraConstantBuffer *spectra_constant_buffer_new(void) {
    return g_object_new(SPECTRA_TYPE_CONSTANT_BUFFER, NULL);
}

void spectra_constant_buffer_define(SpectraConstantBuffer *cbuf, ...) {
    va_list argp;
    va_start(argp, cbuf);
    gchar const *sym;
    while ((sym = va_arg(argp, gchar const*))) {
        enum spectra_constant_type type =
            va_arg(argp, enum spectra_constant_type);

        struct spectra_constant c_sym;
        g_strlcpy(c_sym.ident, sym, sizeof(c_sym.ident));
        c_sym.index = cbuf->vals->len;
        struct spectra_constant_val value = { .tp = type };

        switch (type) {
        case SPECTRA_CONSTANT_TYPE_FUNCTION:
            value.function = va_arg(argp, gpointer);
            break;
        case SPECTRA_CONSTANT_TYPE_VECTOR:
            value.vecp = g_object_ref(va_arg(argp, SpectraVector*));
            break;
        default:
            g_error("unrecognized constant type %02x", (unsigned)type);
        }

        g_array_append_val(cbuf->vals, value);
        g_array_append_val(cbuf->syms, c_sym);
    }
}

guint spectra_constant_buffer_get_index(SpectraConstantBuffer *cbuf,
                                        gchar const *sym) {
    guint sym_index;
    for (sym_index = 0; sym_index < cbuf->syms->len; sym_index++) {
        struct spectra_constant const *curs =
            &g_array_index(cbuf->syms, struct spectra_constant, sym_index);
        if (strcmp(sym, curs->ident) == 0) {
            return curs->index;
        }
    }
    return SPECTRA_ADDR_INVALID;
}

gchar const *spectra_constant_buffer_reverse_query(SpectraConstantBuffer *cbuf,
                                                   guint index) {
    if (index < cbuf->syms->len)
        return g_array_index(cbuf->syms, struct spectra_constant, index).ident;
    else
        return NULL;
}


static struct spectra_constant_val*
spectra_constant_buffer_value(SpectraConstantBuffer *cbuf,
                              guint index, GError **errp) {
    if (index < cbuf->vals->len)
        return &g_array_index(cbuf->vals, struct spectra_constant_val, index);
    else {
        g_set_error(errp, SPECTRA_CBUF_ERROR,
                    SPECTRA_CONSTANT_BUFFER_INVALID_INDEX,
                    "index %08x is not defined in the given constant buffer %p",
                    index, cbuf);
        return NULL;
    }
}

gpointer spectra_constant_buffer_function(SpectraConstantBuffer *cbuf,
                                          guint index, GError **errp) {
    struct spectra_constant_val *value =
        spectra_constant_buffer_value(cbuf, index, errp);
    if (value) {
        if (value->tp == SPECTRA_CONSTANT_TYPE_FUNCTION)
            return value->function;
        else {
            g_set_error(errp, SPECTRA_CBUF_ERROR,
                        SPECTRA_CONSTANT_BUFFER_WRONG_TYPE,
                        "invalid constant buffer symbol type for index %u: "
                        "expected SPECTRA_CONSTANT_TYPE_FUNCTION but got %02x",
                        index, (int)value->tp);
            return NULL;
        }
    } else {
        return NULL;
    }
}

SpectraVector *spectra_constant_buffer_vector(SpectraConstantBuffer *cbuf,
                                              guint index, GError **errp) {
    struct spectra_constant_val *value =
        spectra_constant_buffer_value(cbuf, index, errp);
    if (value) {
        if (value->tp == SPECTRA_CONSTANT_TYPE_VECTOR)
            return value->vecp;
        else {
            g_set_error(errp, SPECTRA_CBUF_ERROR,
                        SPECTRA_CONSTANT_BUFFER_WRONG_TYPE,
                        "invalid constant buffer symbol type for index %u: "
                        "expected SPECTRA_CONSTANT_TYPE_VECTOR but got %02x",
                        index, (int)value->tp);
            return NULL;
        }
    } else {
        return NULL;
    }
}
