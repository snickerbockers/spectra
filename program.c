/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <err.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#include <glib.h>

#include "vector.h"
#include "symbol.h"
#include "program.h"
#include "tokens.h"
#include "parser.h"
#include "filecache.h"

/*******************************************************************************
 **
 ** glib DestroyNotify implementation forr struct inst.  This will
 ** free any heap-allocated memory belonging to the given inst.  it
 ** will *not* free inst itself.
 **
 ******************************************************************************/
static void
spectra_program_inst_cleanup(gpointer inst);

static void spectra_program_context_write(SpectraProgramContext *ctxt,
                                          guint addr, SpectraVector *valp);


#define SPECTRA_PROGRAM_ERROR (spectra_program_error_quark())
G_DEFINE_QUARK(spectra-program-error-quark, spectra_program_error)

enum inst_opcode {
    INST_OP_INVALID,

    INST_OP_LOAD_LITERAL,
    INST_OP_LOAD_CONST,
    INST_OP_LOAD_INPUT,

    INST_OP_SAMPLE_TABLE,
    INST_OP_SAMPLE_VECTOR_TABLE,

    INST_OP_ADD,
    INST_OP_SUB,
    INST_OP_MUL,
    INST_OP_DIV,
    INST_OP_EXP,
    INST_OP_LN,

    INST_OP_FUNCTION_INVOCATION,

    /***************************************************************************
     **
     ** pushes memory/string value onto the argument stack.  There are no
     ** corresponding 'pop' instructions because the argument stack is reset by
     ** each INST_OP_FUNCTION_INVOCATION
     **
     **************************************************************************/
    INST_OP_PUSH_MEMORY,
    INST_OP_PUSH_STRING,

    /***************************************************************************
     **
     ** push/pop a value to/from the beginning of the inputs array.  This is
     ** used by the bind function to mask input lists with new input lists.
     **
     ** Since the arguments are put at the front of the array, this changes the
     ** index of every subsequent element in the inputs array.  Because of that,
     ** the two below rules must *always* be observed when using these
     ** instructions:
     **
     ** * For every push tere is a corresponding pop that happens later
     ** * For every pop there is a corresponding push that happened in the past
     ** * when in the midst of a push/execute/pop sequence, programs must never
     **   access elements that occurred befor the original push since their
     ** * indices have changes.
     **
     **************************************************************************/
    INST_OP_PUSH_INPUT,
    INST_OP_POP_INPUT,

    INST_OP_TERMINATE
};

/*******************************************************************************
 **
 ** MAX_PREREQS - the maximum number of pre-requisites any individual
 **               instruction may rely upon
 ******************************************************************************/
#define MAX_PREREQS 2

struct inst {
    enum inst_opcode op;

    // "memory" addresses, not used by all  ops
    unsigned prereq[MAX_PREREQS];

    union {
        SpectraVector *litp;
        unsigned index; // used for loading constants from the cbuf
    };
    unsigned output;
};

struct _SpectraProgram {
    GObject parent;

    GSList *bytecode;

    guint mem_sz, func_tbl_sz;

    /***************************************************************************
     **
     ** unlike the constants in the SpectraProgramContext's cbuf, strings are
     ** defined as literals and are not accessed via a associated symbol.
     ** Therefore we store them in the program itself.  Fundamentally this is no
     ** different from storing a literal value in an instruction; even though
     ** the array of strings is in program and not the instruction, both
     ** ultimately are just literals that are part of the program.
     **
     **************************************************************************/
    GPtrArray *string_literals;

    // maps input index -> input name
    // you can reverse-query this mapping with spectra_program_input_index
    GPtrArray *inputs;

    enum spectra_program_state state;

    struct symbol_table *symbols;

    /***************************************************************************
     **
     ** sampled signals will need a place to store their lookup tables which
     ** will continue to exist even if the signal is destroyed but the program
     ** is not.  The solution to this is to have an array of pointers to structures
     ** storing the raw data of tables needed by this program.  This
     ** array of pointers will have its GDestroyNotify set to call g_bytes_unref
     ** on every element's table when it is destroyed.
     **
     ** it make seem a bit odd for this to be in SpectraProgram rather than
     ** SpectraProgramContext but the reasoning behind this is that it is
     ** analagous to the literal pools that many compilers create when working
     ** with certain ISAs that don't support 32-bit or 64-bit immediates (my old
     ** fav, the SH-4 comes to mind).
     **
     ** pointers in this array shall be of type `struct spectra_program_table`,
     ** defined below.
     **
     **************************************************************************/
    GPtrArray *tables;

    SpectraConstantBuffer *cbuf;
};

enum spectra_program_table_type {
    SPECTRA_PROGRAM_TABLE_TYPE_SCALAR,
    SPECTRA_PROGRAM_TABLE_TYPE_VECTOR
};

struct spectra_program_table {
    enum spectra_program_table_type tp;
    union {
        GBytes *data;
        GPtrArray *vectors;
    };
    spectra_scalar sample_frequency;
};

guint spectra_program_add_table(SpectraProgram *prog, GBytes *data,
                                spectra_scalar sample_frequency) {
    struct spectra_program_table *tbl =
        g_malloc(sizeof(struct spectra_program_table));
    guint index = prog->tables->len;
    tbl->tp = SPECTRA_PROGRAM_TABLE_TYPE_SCALAR;
    tbl->data = g_bytes_ref(data);
    tbl->sample_frequency = sample_frequency;
    g_ptr_array_add(prog->tables, tbl);
    return index;
}

guint spectra_program_add_vector_table(SpectraProgram *prog, GPtrArray *vecs,
                                       spectra_scalar sample_frequency) {
    struct spectra_program_table *tbl =
        g_malloc(sizeof(struct spectra_program_table));
    guint index = prog->tables->len;
    tbl->tp = SPECTRA_PROGRAM_TABLE_TYPE_VECTOR;
    tbl->vectors = g_ptr_array_ref(vecs);
    tbl->sample_frequency = sample_frequency;
    g_ptr_array_add(prog->tables, tbl);
    return index;
}

guint spectra_program_string_literal(SpectraProgram *prog, gchar const *str) {
    guint index;
    if (!g_ptr_array_find_with_equal_func(prog->string_literals,
                                          str, g_str_equal, &index)) {
        index = prog->string_literals->len;
        g_ptr_array_add(prog->string_literals, g_strdup(str));
    }
    return index;
}

gchar const *spectra_program_string_literal_index(SpectraProgram *prog,
                                                  guint index) {
    g_return_val_if_fail(index < prog->string_literals->len,
                         "ERROR INVALID STRING INDEX");
    return g_ptr_array_index(prog->string_literals, index);
}

gboolean spectra_program_compiling(SpectraProgram *prog) {
    return prog->state == SPECTRA_PROGRAM_STATE_COMPILE_IN_PROGRESS;
}

gboolean spectra_program_can_execute(SpectraProgram *prog) {
    return prog->state == SPECTRA_PROGRAM_STATE_COMPILED;
}

G_DEFINE_FINAL_TYPE(SpectraProgram, spectra_program, G_TYPE_OBJECT)

static void spectra_program_finalize(GObject *gobj) {
    SpectraProgram *prog = SPECTRA_PROGRAM(gobj);

    g_ptr_array_unref(prog->inputs);
    g_ptr_array_unref(prog->string_literals);
    g_slist_free_full(prog->bytecode, spectra_program_inst_cleanup);
    symbol_table_unref(prog->symbols);
    g_ptr_array_unref(prog->tables);
    g_object_unref(prog->cbuf);

    G_OBJECT_CLASS(spectra_program_parent_class)->finalize(gobj);
}

SpectraProgram *spectra_program_new(SpectraConstantBuffer *cbuf) {
    SpectraProgram *prog = g_object_new(SPECTRA_TYPE_PROGRAM, NULL);
    prog->cbuf = g_object_ref(cbuf);
    return prog;
}

static void spectra_program_destroy_table(gpointer ptr) {
    struct spectra_program_table *tbl = ptr;
    g_bytes_unref(tbl->data);
    g_free(tbl);
}

static void spectra_program_init(SpectraProgram *prog) {
    prog->inputs = g_ptr_array_new_with_free_func(g_free);
    prog->symbols = symbol_table_new();
    prog->string_literals = g_ptr_array_new_with_free_func(g_free);

    prog->bytecode = NULL;
    prog->mem_sz = 0;
    prog->state = SPECTRA_PROGRAM_STATE_INITIALIZED;
    prog->tables = g_ptr_array_new_with_free_func(spectra_program_destroy_table);
}

static void spectra_program_class_init(SpectraProgramClass *class) {
    G_OBJECT_CLASS(class)->finalize = spectra_program_finalize;
}

guint spectra_program_mem(SpectraProgram *prog) {
    g_return_val_if_fail(spectra_program_compiling(prog),
                         PROGRAM_ADDR_INVALID);
    return prog->mem_sz++;
}

guint spectra_program_literal(SpectraProgram *prog, SpectraVector *valp) {
    g_return_val_if_fail(spectra_program_compiling(prog),
                         PROGRAM_ADDR_INVALID);

    struct inst *new_inst = g_malloc0(sizeof(struct inst));
    guint dst_addr = spectra_program_mem(prog);
    new_inst->op = INST_OP_LOAD_LITERAL;
    new_inst->litp = g_object_ref(valp);
    new_inst->output = dst_addr;

    prog->bytecode = g_slist_append(prog->bytecode, new_inst);

    return dst_addr;
}

guint spectra_program_symbol(SpectraProgram *prog, gchar const *sym_name) {
    g_return_val_if_fail(spectra_program_compiling(prog),
                         PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    guint cbuf_index = spectra_constant_buffer_get_index(prog->cbuf, sym_name);
    if (cbuf_index == SPECTRA_ADDR_INVALID) {
        // its an input variable
        instp->index = spectra_program_declare_input(prog, sym_name);
        instp->op = INST_OP_LOAD_INPUT;
    } else {
        // it's a cbuf constant
        instp->op = INST_OP_LOAD_CONST;
        instp->index = cbuf_index;
    }

    instp->output = spectra_program_mem(prog);
    prog->bytecode = g_slist_append(prog->bytecode, instp);
    return instp->output;
}

guint spectra_program_load_input(SpectraProgram *prog,
                                 gchar const *input_name) {
    guint input_index = spectra_program_declare_input(prog, input_name);
    g_return_val_if_fail(input_index != PROGRAM_ADDR_INVALID,
                         PROGRAM_ADDR_INVALID);
    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->op = INST_OP_LOAD_INPUT;
    instp->index = input_index;
    instp->output = spectra_program_mem(prog);
    prog->bytecode = g_slist_append(prog->bytecode, instp);
    return instp->output;
}

guint spectra_program_mult(SpectraProgram *prog, guint lhs, guint rhs) {

    g_return_val_if_fail(spectra_program_compiling(prog),
                         PROGRAM_ADDR_INVALID);
    g_return_val_if_fail(lhs < prog->mem_sz && rhs < prog->mem_sz,
                         PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->output = spectra_program_mem(prog);
    instp->op = INST_OP_MUL;
    instp->prereq[0] = lhs;
    instp->prereq[1] = rhs;

    prog->bytecode = g_slist_append(prog->bytecode, instp);

    return instp->output;
}

guint spectra_program_add(SpectraProgram *prog, guint lhs, guint rhs) {

    g_return_val_if_fail(spectra_program_compiling(prog),
                         PROGRAM_ADDR_INVALID);
    g_return_val_if_fail(lhs < prog->mem_sz && rhs < prog->mem_sz,
                         PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->output = spectra_program_mem(prog);
    instp->op = INST_OP_ADD;
    instp->prereq[0] = lhs;
    instp->prereq[1] = rhs;

    prog->bytecode = g_slist_append(prog->bytecode, instp);

    return instp->output;
}

guint spectra_program_div(SpectraProgram *prog, guint dividend, guint divisor) {

    g_return_val_if_fail(spectra_program_compiling(prog),
                         PROGRAM_ADDR_INVALID);
    g_return_val_if_fail(dividend < prog->mem_sz && divisor < prog->mem_sz,
                         PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->output = spectra_program_mem(prog);
    instp->op = INST_OP_DIV;
    instp->prereq[0] = dividend;
    instp->prereq[1] = divisor;

    prog->bytecode = g_slist_append(prog->bytecode, instp);

    return instp->output;
}

guint spectra_program_exp(SpectraProgram *prog, guint logarithm) {
    g_return_val_if_fail(spectra_program_compiling(prog), PROGRAM_ADDR_INVALID);
    g_return_val_if_fail(logarithm < prog->mem_sz, PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->output = spectra_program_mem(prog);
    instp->op = INST_OP_EXP;
    instp->prereq[0] = logarithm;
    prog->bytecode = g_slist_append(prog->bytecode, instp);

    return instp->output;
}

guint spectra_program_ln(SpectraProgram *prog, guint val) {
    g_return_val_if_fail(spectra_program_compiling(prog), PROGRAM_ADDR_INVALID);
    g_return_val_if_fail(val < prog->mem_sz, PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->output = spectra_program_mem(prog);
    instp->op = INST_OP_LN;
    instp->prereq[0] = val;
    prog->bytecode = g_slist_append(prog->bytecode, instp);

    return instp->output;
}

guint spectra_program_terminate(SpectraProgram *prog, guint output_index) {
    g_return_val_if_fail(spectra_program_compiling(prog),
                         PROGRAM_ADDR_INVALID);
    g_return_val_if_fail(output_index < prog->mem_sz,
                         PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->output = output_index;
    instp->op = INST_OP_TERMINATE;
    prog->bytecode = g_slist_append(prog->bytecode, instp);

    return output_index;
}

guint spectra_program_invoke(SpectraProgram *prog,
                             gchar const *ident,
                             GArray *args) {
    g_return_val_if_fail(spectra_program_compiling(prog),
                         PROGRAM_ADDR_INVALID);

    guint inputs[MAX_PREREQS];
    guint argno;
    g_warn_if_fail(g_array_get_element_size(args) ==
                   sizeof(struct spectra_program_argument));
    for (argno = 0; argno < args->len; argno++) {
        struct spectra_program_argument arg =
            g_array_index(args, struct spectra_program_argument, argno);
        g_warn_if_fail((arg.tp == SPECTRA_PROGRAM_MEMORY && arg.index < prog->mem_sz) ||
                       (arg.tp == SPECTRA_PROGRAM_STRING && arg.index < prog->string_literals->len));

        struct inst *instp = g_malloc0(sizeof(struct inst));
        switch (arg.tp) {
        case SPECTRA_PROGRAM_MEMORY:
            instp->op = INST_OP_PUSH_MEMORY;
            break;
        case SPECTRA_PROGRAM_STRING:
            instp->op = INST_OP_PUSH_STRING;
            break;
        default:
            g_warning("invalid argument type %02x in %s", (int)arg.tp, G_STRFUNC);
            continue;
        }
        instp->prereq[0] = arg.index;
        prog->bytecode = g_slist_append(prog->bytecode, instp);
    }
    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->op = INST_OP_FUNCTION_INVOCATION;
    instp->output = spectra_program_mem(prog);
    instp->index = spectra_constant_buffer_get_index(prog->cbuf, ident);
    g_warn_if_fail(instp->index != SPECTRA_ADDR_INVALID);

    prog->bytecode = g_slist_append(prog->bytecode, instp);
    return instp->output;
}

guint spectra_program_sample_table(SpectraProgram *prog,
                                   guint table_index, guint time) {
    g_return_val_if_fail(table_index < prog->tables->len, PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->op = INST_OP_SAMPLE_TABLE;
    instp->prereq[0] = time;
    instp->index = table_index;
    instp->output = spectra_program_mem(prog);

    prog->bytecode = g_slist_append(prog->bytecode, instp);
    return instp->output;
}

guint spectra_program_sample_vector_table(SpectraProgram *prog,
                                          guint table_index, guint time) {
    g_return_val_if_fail(table_index < prog->tables->len, PROGRAM_ADDR_INVALID);

    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->op = INST_OP_SAMPLE_VECTOR_TABLE;
    instp->prereq[0] = time;
    instp->index = table_index;
    instp->output = spectra_program_mem(prog);

    prog->bytecode = g_slist_append(prog->bytecode, instp);
    return instp->output;
}

/*******************************************************************************
 **
 ** SpectraProgramContext
 **
 ******************************************************************************/

struct _SpectraProgramContext {
    GObject parent;

    GPtrArray *mem;
    GPtrArray *func_tbl;
    GArray *argstack;

    SpectraFileCache *filecache;
};

G_DEFINE_FINAL_TYPE(SpectraProgramContext,
                    spectra_program_context, G_TYPE_OBJECT)

static void spectra_program_context_finalize(GObject *gobj) {
    SpectraProgramContext *ctxt = SPECTRA_PROGRAM_CONTEXT(gobj);

    g_ptr_array_unref(ctxt->mem);
    g_ptr_array_unref(ctxt->func_tbl);
    g_array_unref(ctxt->argstack);
    g_object_unref(ctxt->filecache);

    G_OBJECT_CLASS(spectra_program_context_parent_class)->finalize(gobj);
}

static void spectra_program_context_init(SpectraProgramContext *ctxt) {
    ctxt->mem = NULL;
    ctxt->filecache = spectra_file_cache_new();

    ctxt->argstack = g_array_new(FALSE, FALSE,
                                 sizeof(struct spectra_program_argument));
}

static void
spectra_program_context_class_init(SpectraProgramContextClass *class) {
    G_OBJECT_CLASS(class)->finalize = spectra_program_context_finalize;
}

SpectraProgramContext *spectra_program_context_new_full(guint mem_sz, guint func_tbl_sz) {
    SpectraProgramContext *ctxt = g_object_new(SPECTRA_TYPE_PROGRAM_CONTEXT, NULL);

    gpointer *fn_tbl_backing = g_malloc0_n(func_tbl_sz, sizeof(gpointer));

    ctxt->mem = g_ptr_array_new_full(mem_sz, g_object_unref);
    ctxt->func_tbl = g_ptr_array_new_take(fn_tbl_backing, func_tbl_sz, NULL);

    /***************************************************************************
     **
     ** TODO: make them all references to the same underlying instance.  In
     ** order for this to work all vector instances would have to be immutable.
     ** This should already be how spectra works 99% of the time, just need to
     ** make sure the other 1% is identified and handled correctly.
     **
     **************************************************************************/
    guint index;
    for (index = 0; index < mem_sz; index++)
        g_ptr_array_add(ctxt->mem, spectra_real_new(0.0));

    return ctxt;
}

SpectraProgramContext *spectra_program_context_new(SpectraProgram *prog) {
    return spectra_program_context_new_full(prog->mem_sz, prog->func_tbl_sz);
}

static gboolean check_initializer(gconstpointer init_void, gconstpointer name_void) {
    struct prog_sym_init const *init_p = init_void;
    char const *name = name_void;
    return strncmp(init_p->name, name_void, PROG_SYM_MAX) == 0;
}

SpectraVector* spectra_program_execute(SpectraProgram *prog,
                                       SpectraProgramContext *ctxt,
                                       GPtrArray *inputs, GError **err_in) {
    g_return_val_if_fail(spectra_program_can_execute(prog), NULL);

    if (inputs->len < spectra_program_input_count(prog)) {
        g_set_error(err_in, SPECTRA_PROGRAM_ERROR,
                    SPECTRA_PROGRAM_MISSING_INDEPENDENT_VARIABLES,
                    "program requires %u independent variables but only %u "
                    "independent variables were provided",
                    spectra_program_input_count(prog), inputs->len);
        return NULL;
    }

    guint initial_input_count = inputs->len;

    GSList const *list_node;

    for (list_node = prog->bytecode; list_node; list_node = list_node->next) {
        struct inst const *inst_p = list_node->data;

        switch (inst_p->op) {
        case INST_OP_LOAD_LITERAL:
            {
                spectra_program_context_write(ctxt, inst_p->output,
                                              inst_p->litp);
            }
            break;
        case INST_OP_LOAD_CONST:
            {
                GError *err = NULL;
                SpectraVector *vecp =
                    spectra_constant_buffer_vector(prog->cbuf,
                                                   inst_p->index, &err);
                if (err) {
                    g_propagate_error(err_in, err);
                    return NULL;
                } else {
                    spectra_program_context_write(ctxt, inst_p->output, vecp);
                }
            }
            break;
        case INST_OP_LOAD_INPUT:
                if (inst_p->index < inputs->len) {
                    SpectraVector *valp =
                        g_ptr_array_index(inputs, inst_p->index);
                    spectra_program_context_write(ctxt, inst_p->output, valp);
                } else {
                    g_set_error(err_in,
                                SPECTRA_PROGRAM_ERROR,
                                SPECTRA_PROGRAM_EXEC_FAILURE,
                                "cannot access input index %u", inst_p->index);
                }
            break;
        case INST_OP_SAMPLE_TABLE:
            {
                g_warn_if_fail(inst_p->index < prog->tables->len);
                struct spectra_program_table *tbl =
                    g_ptr_array_index(prog->tables, inst_p->index);
                g_return_val_if_fail(tbl->tp ==
                                     SPECTRA_PROGRAM_TABLE_TYPE_SCALAR, NULL);
                gsize n_bytes;
                gconstpointer rawdat = g_bytes_get_data(tbl->data, &n_bytes);
                g_warn_if_fail(n_bytes % sizeof(spectra_scalar) == 0);
                gsize n_samples = n_bytes / sizeof(spectra_scalar);
                SpectraVector *time_vec =
                    spectra_program_context_read_real(ctxt, inst_p->prereq[0]);
                if (!time_vec) {
                    g_set_error(err_in, SPECTRA_PROGRAM_ERROR, SPECTRA_PROGRAM_NON_REAL,
                                "time index for sample instruction is non-real");
                    return NULL;
                }

                spectra_scalar time = spectra_vector_index(time_vec, 0);
                gint index = time * tbl->sample_frequency;
                spectra_scalar value;
                if (index >= 0 && index < n_samples) {
                    memcpy(&value,
                           ((char const*)rawdat) + index * sizeof(spectra_scalar),
                           sizeof(spectra_scalar));
                } else {
                    value = 0.0; // TODO: periodic summation?
                }
                SpectraVector *valp = spectra_real_new(value);
                spectra_program_context_write(ctxt, inst_p->output, valp);
                g_object_unref(valp);
                g_object_unref(time_vec);
            }
            break;
        case INST_OP_SAMPLE_VECTOR_TABLE:
            {
                g_warn_if_fail(inst_p->index < prog->tables->len);
                struct spectra_program_table *tbl =
                    g_ptr_array_index(prog->tables, inst_p->index);
                g_return_val_if_fail(tbl->tp == SPECTRA_PROGRAM_TABLE_TYPE_VECTOR,
                                     NULL);
                SpectraVector *time_vec =
                    spectra_program_context_read_real(ctxt, inst_p->prereq[0]);
                if (!time_vec) {
                    g_set_error(err_in, SPECTRA_PROGRAM_ERROR, SPECTRA_PROGRAM_NON_REAL,
                                "time index for INST_OP_SAMPLE_VECTOR_TABLE "
                                "instruction is non-real");
                    return NULL;
                }

                spectra_scalar time = spectra_vector_index(time_vec, 0);
                gint index = time * tbl->sample_frequency;
                SpectraVector *value;
                if (index >= 0 && index < tbl->vectors->len) {
                    value = g_object_ref(g_ptr_array_index(tbl->vectors, index));
                } else {
                    // TODO: periodic summation?
                    value = spectra_real_new(0.0);
                }
                spectra_program_context_write(ctxt, inst_p->output, value);
                g_object_unref(value);
                g_object_unref(time_vec);
            }
            break;
        case INST_OP_MUL:
            {
                SpectraVector *lhs =
                    spectra_program_context_read_vec(ctxt, inst_p->prereq[0]);
                SpectraVector *rhs =
                    spectra_program_context_read_vec(ctxt, inst_p->prereq[1]);
                SpectraVector* prod = spectra_vector_mult(lhs, rhs);
                spectra_program_context_write(ctxt, inst_p->output, prod);
                g_object_unref(prod);
            }
            break;
        case INST_OP_ADD:
            {
                SpectraVector *lhs =
                    spectra_program_context_read_vec(ctxt, inst_p->prereq[0]);
                SpectraVector *rhs =
                    spectra_program_context_read_vec(ctxt, inst_p->prereq[1]);
                SpectraVector *sum = spectra_vector_add(lhs, rhs);
                spectra_program_context_write(ctxt, inst_p->output, sum);
                g_object_unref(sum);
            }
            break;
        case INST_OP_DIV:
            {
                SpectraVector *lhs =
                    spectra_program_context_read_vec(ctxt, inst_p->prereq[0]);
                SpectraVector *rhs =
                    spectra_program_context_read_vec(ctxt, inst_p->prereq[1]);
                SpectraVector* ratio = spectra_vector_div(lhs, rhs);
                spectra_program_context_write(ctxt, inst_p->output, ratio);
                g_object_unref(ratio);
            }
            break;
        case INST_OP_EXP:
            {
                SpectraVector *logarithm =
                    spectra_program_context_read_vec(ctxt, inst_p->prereq[0]);
                SpectraVector *valp = spectra_vector_exp(logarithm);
                spectra_program_context_write(ctxt, inst_p->output, valp);
                g_object_unref(valp);
            }
            break;
        case INST_OP_LN:
            {
                SpectraVector *exponential =
                    spectra_program_context_read_vec(ctxt, inst_p->prereq[0]);
                SpectraVector *valp = spectra_vector_ln(exponential);
                spectra_program_context_write(ctxt, inst_p->output, valp);
                g_object_unref(valp);
            }
            break;
        case INST_OP_TERMINATE:
            g_warn_if_fail(initial_input_count == inputs->len);
            return g_object_ref(spectra_program_context_read_vec(ctxt, inst_p->output));
        case INST_OP_PUSH_MEMORY:
            {
                struct spectra_program_argument src = {
                    .tp = SPECTRA_PROGRAM_MEMORY,
                    .index = inst_p->prereq[0]
                };
                g_array_append_val(ctxt->argstack, src);
                break;
            }
        case INST_OP_PUSH_STRING:
            {
                struct spectra_program_argument src = {
                    .tp = SPECTRA_PROGRAM_STRING,
                    .index = inst_p->prereq[0]
                };
                g_warn_if_fail(inst_p->prereq[0] < prog->string_literals->len);
                g_array_append_val(ctxt->argstack, src);
                break;
            }
        case INST_OP_FUNCTION_INVOCATION:
            {
                spectra_function impl =
                    spectra_constant_buffer_function(prog->cbuf,
                                                     inst_p->index, err_in);
                if (!impl)
                    return NULL;
                GError *error = NULL;
                SpectraVector *vecp =
                    impl(prog, ctxt, ctxt->argstack->len,
                         (struct spectra_program_argument*)ctxt->argstack->data,
                         &error);
                if (error) {
                    g_propagate_error(err_in, error);
                    return NULL;
                }
                spectra_program_context_write(ctxt, inst_p->output, vecp);
                g_object_unref(vecp);
                g_array_set_size(ctxt->argstack, 0);
            }
            break;
        case INST_OP_PUSH_INPUT:
            /*******************************************************************
             **
             ** TODO: well obviously it makes way more sense to be
             ** pushing/popping at the end rather than the beginning because
             ** that way it doesn't need a memcpy or memmove.  But then i also
             ** need some sort of base pointer register like real ISAs have so
             ** i have a place to index from.  will consider this later, not
             ** that big of a deal since in most cases there's only one input
             ** variable anyways.
             **
             ******************************************************************/
            g_ptr_array_insert(inputs, 0,
                               spectra_program_context_read_vec(ctxt,
                                                                inst_p->index));
            break;
        case INST_OP_POP_INPUT:
            g_ptr_array_remove_index(inputs, 0);
            break;
        default:
            errx(1, "opcode %02x not implemented", inst_p->op);
        }
    }

    g_set_error(err_in, SPECTRA_PROGRAM_ERROR, SPECTRA_PROGRAM_EXEC_FAILURE,
                "program does not terminate");
}

static void print_single_symbol(gpointer sym_void, gpointer user_data) {
    struct prog_sym const *symp = sym_void;
    char const *sym_tp;
    switch (symp->tp) {
    case PROG_SYM_TP_INPUT:
        sym_tp = "input variable";
        break;
    case PROG_SYM_TP_INVALID:
    default:
        sym_tp = "invalid/error";
    }
    printf("\t%s    %s    %02x\n", symp->name, sym_tp, symp->addr);
}

static void print_single_inst(gpointer data, gpointer user_ptr) {
    struct inst *inst = data;
    SpectraProgram *prog = user_ptr;
    switch (inst->op) {
    case INST_OP_LOAD_LITERAL: {
        gchar *strp = spectra_vector_str(inst->litp);
        printf("\tLOAD_LITERAL %s INTO %02x\n", strp, inst->output);
        g_free(strp);
    }
        break;
    case INST_OP_ADD:
        printf("\tADD %02x AND %02x INTO %02x\n",
               inst->prereq[0], inst->prereq[1], inst->output);
        break;
    case INST_OP_SUB:
        printf("\tSUB %02x FROM %02x INTO %02x\n",
               inst->prereq[1], inst->prereq[0], inst->output);
        break;
    case INST_OP_MUL:
        printf("\tMUL %02x AND %02x INTO %02x\n",
               inst->prereq[0], inst->prereq[1], inst->output);
        break;
    case INST_OP_DIV:
        printf("\tDIVIDE %02x BY %02x INTO %02x\n",
               inst->prereq[0], inst->prereq[1], inst->output);
        break;
    case INST_OP_EXP:
        printf("\tEXP(%02x) INTO %02x\n", inst->prereq[0], inst->output);
        break;
    case INST_OP_LN:
        printf("\tLN(%02x) INTO %02x\n", inst->prereq[0], inst->output);
        break;
    case INST_OP_TERMINATE:
        printf("\tTERMINATE EXECUTION, FINAL OUTPUT IS %02x\n", inst->output);
        break;
    case INST_OP_INVALID:
        printf("\t<ERROR - INST_OP_INVALID>\n");
        break;
    case INST_OP_LOAD_CONST:
        {
            gchar const *name =
                spectra_constant_buffer_reverse_query(prog->cbuf, inst->index);
            if (name) {
                printf("\tLOAD CBUF{%02x}->\"%s\" INTO %02x\n",
                       inst->index, name, inst->output);
            } else {
                printf("\tLOAD CBUF{%02x} INTO %02x\n",
                       inst->index, inst->output);
            }
        }
        break;
    case INST_OP_SAMPLE_TABLE:
        printf("\tSAMPLE AT TIME=%02x FROM TABLE %02x INTO %02x\n",
               inst->prereq[0], inst->index, inst->output);
        break;
    case INST_OP_SAMPLE_VECTOR_TABLE:
        printf("\tSAMPLE AT TIME=%02x FROM VECTOR TABLE %02x INTO %02x\n",
               inst->prereq[0], inst->index, inst->output);
        break;
    case INST_OP_LOAD_INPUT:
        printf("\tLOAD INPUT{%02x} INTO %02x\n",
               inst->index, inst->output);
        break;
    case INST_OP_FUNCTION_INVOCATION:
        printf("\tCALL FUNC{%02x}(%02x) - return into %02x\n",
               inst->index, inst->prereq[0], inst->output);
        break;
    case INST_OP_PUSH_MEMORY:
        printf("\tPUSH MEM{%02x}\n", inst->prereq[0]);
        break;
    case INST_OP_PUSH_STRING:
        printf("\tPUSH STRING {%02x}\n", inst->prereq[0]);
        break;
    case INST_OP_PUSH_INPUT:
        printf("\tPUSH %02x ONTO INPUTS\n", inst->index);
        break;
    case INST_OP_POP_INPUT:
        printf("\tPOP FROM INPUTS\n");
        break;
    default:
        printf("\t<ERROR - BAD INSTRUCTION OPCODE>\n");
    }
}

void spectra_program_print(SpectraProgram *prog) {
    printf("PROGRAM REQUIRES %u MEMORY CELLS\n", prog->mem_sz);

    if (prog->symbols->syms->len) {
        printf("PROGRAM SYMBOL TABLE:\n");
        symbol_table_foreach(prog->symbols, print_single_symbol, NULL);
    }
    g_slist_foreach(prog->bytecode, print_single_inst, prog);
}

SpectraVector* spectra_program_context_read_vec(SpectraProgramContext const *ctxt,
                                                guint addr) {
    g_return_val_if_fail(addr < ctxt->mem->len, NULL);

    SpectraVector *vecp = g_ptr_array_index(ctxt->mem, addr);
    g_return_val_if_fail(vecp, NULL);
    return vecp;
}


static void spectra_program_context_write(SpectraProgramContext *ctxt,
                                          guint addr, SpectraVector *valp) {
    g_return_if_fail(addr < ctxt->mem->len);
    gpointer old = g_ptr_array_index(ctxt->mem, addr);
    g_return_if_fail(old);
    g_object_unref(old);
    g_ptr_array_index(ctxt->mem, addr) = g_object_ref(valp);
}

SpectraFile *spectra_program_context_load_file(SpectraProgramContext *ctxt,
                                               gchar const *path, GError **err) {
    return spectra_file_cache_open(ctxt->filecache, path, err);
}

static char const*
spectra_program_state_name(enum spectra_program_state state) {
    switch (state) {
    case SPECTRA_PROGRAM_STATE_INITIALIZED:
        return "SPECTRA_PROGRAM_STATE_INITIALIZED";
    case SPECTRA_PROGRAM_STATE_COMPILE_IN_PROGRESS:
        return "SPECTRA_PROGRAM_STATE_COMPILE_IN_PROGRESS";
    case SPECTRA_PROGRAM_STATE_COMPILED:
        return "SPECTRA_PROGRAM_STATE_COMPILED";
    case SPECTRA_PROGRAM_STATE_COMPILE_FAILED:
        return "SPECTRA_PROGRAM_STATE_COMPILE_FAILED";
    default:
        return "INVALID/NONEXISTANT PROGRAM STATE VALUE";
    }
}

void spectra_program_state_transition(SpectraProgram *prog,
                                      enum spectra_program_state state) {
    if (state == prog->state ||
        prog->state == SPECTRA_PROGRAM_STATE_COMPILED ||
        prog->state == SPECTRA_PROGRAM_STATE_COMPILE_FAILED ||
        ((state == SPECTRA_PROGRAM_STATE_COMPILED ||
          state == SPECTRA_PROGRAM_STATE_COMPILE_FAILED) &&
         prog->state != SPECTRA_PROGRAM_STATE_COMPILE_IN_PROGRESS)) {
        g_warning("%s: illegal state transition: %s to %s\n",
                  G_STRFUNC,
                  spectra_program_state_name(prog->state),
                  spectra_program_state_name(state));
    } else {
        prog->state = state;
    }
}

gboolean spectra_program_check_state(SpectraProgram const *prog,
                                     enum spectra_program_state expect) {
    return prog->state == expect;
}

guint spectra_program_output_index(SpectraProgram const *prog) {

    g_return_val_if_fail(spectra_program_check_state(prog, SPECTRA_PROGRAM_STATE_COMPILED),
                         PROGRAM_ADDR_INVALID);

    GSList const *list_node;
    for (list_node = prog->bytecode; list_node; list_node = list_node->next) {
        struct inst const *inst_p = list_node->data;
        if (inst_p->op == INST_OP_TERMINATE)
            return inst_p->output;
    }

    g_warning("%s - program has no INST_OP_TERMINATE!?!?!?", G_STRFUNC);
    return PROGRAM_ADDR_INVALID;
}

guint spectra_program_input_count(SpectraProgram const *prog) {
    return prog->inputs->len;
}

guint spectra_program_declare_input(SpectraProgram *prog, gchar const *sym) {
    guint index = spectra_program_input_index(prog, sym);
    if (index == PROGRAM_ADDR_INVALID) {
        index = prog->inputs->len;
        g_ptr_array_add(prog->inputs, g_strdup(sym));
    }
    return index;
}

gchar const *spectra_program_input_name(SpectraProgram *prog, guint index) {
    g_return_val_if_fail(index < prog->inputs->len, NULL);
    return g_ptr_array_index(prog->inputs, index);
}

guint spectra_program_input_index(SpectraProgram *prog, gchar const *sym) {
    guint index;
    if (g_ptr_array_find_with_equal_func(prog->inputs, sym,
                                         g_str_equal, &index)) {
        return index;
    } else {
        return PROGRAM_ADDR_INVALID;
    }
}


void spectra_program_push_input(SpectraProgram *prog, gchar const *name, guint addr) {
    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->op = INST_OP_PUSH_INPUT;
    instp->index = addr;
    prog->bytecode = g_slist_append(prog->bytecode, instp);
    g_ptr_array_insert(prog->inputs, 0, g_strdup(name));
}

void spectra_program_pop_input(SpectraProgram *prog) {
    struct inst *instp = g_malloc0(sizeof(struct inst));
    instp->op = INST_OP_POP_INPUT;
    prog->bytecode = g_slist_append(prog->bytecode, instp);
    g_ptr_array_remove_index(prog->inputs, 0);
}

static void
spectra_program_inst_cleanup(gpointer inst) {
    struct inst *as_inst = inst;
    if (as_inst && as_inst->op == INST_OP_LOAD_LITERAL && as_inst->litp) {
        g_object_unref(as_inst->litp);
        as_inst->litp = NULL;
    }
}

SpectraVector*
spectra_program_context_read_real(SpectraProgramContext const *ctxt,
                                  guint addr) {
    SpectraVector *vecp = spectra_program_context_read_vec(ctxt, addr);
    if (vecp)
        return spectra_vector_as_real(vecp);
    else
        return NULL;
}
