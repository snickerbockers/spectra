/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/
#include <err.h>
#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>

#include <glib.h>

#include "tokens.h"

#define SPECTRA_TOKENIZER_ERROR (spectra_tokenizer_error_quark())
G_DEFINE_QUARK(spectra-tokenizer-error-quark, spectra_tokenizer_error)

/*******************************************************************************
 **
 ** string_bounds
 **
 ** parses through the given string and returns the index of the
 ** opening quote and the closing quote into str_first_p and str_last_p,
 ** respectively.
 **
 ** if there are no unescaped quotes in the string then both
 ** *str_first_p and *str_last_p will be set to -1.
 **
 ** if there is an opening quote but not a closing quote then
 ** *str_first_p will contain the index of the opening quote and
 ** *str_last_p will be -1.
 **
 ** The indices returned in *str_first_p and *str_last_p point to the
 ** quotes themselves, so if you're interested only in the contents of
 ** the string then the first character is *str_first_p+1 and the last
 ** character is *str_last_p-1 (assuming that *str_first_p and *str_last_p
 ** are not -1).
 **
 ** In the case that the string contains more than one set of quotes,
 ** only the first two will be written to *str_first_p and *str_last_p.
 ** the onus is on the caller to verify that there is no more text
 ** after *str_last_p.
 **
 ******************************************************************************/
static void string_bounds(char const *str, unsigned n_chars,
                          int *str_first_p, int *str_last_p);

// free the given token
static void token_destroy_notify(void *ptr);

void tokenizer_init(struct tokenizer *tokenizer) {
    memset(tokenizer, 0, sizeof(*tokenizer));
    tokenizer->tokens = NULL;//g_slist_alloc();
    tokenizer->bufmax = 63;// default token limit
    tokenizer->bufnext = 0;
    tokenizer->buf = g_malloc_n(tokenizer->bufmax + 1, sizeof(gchar));
}

void tokenizer_cleanup(struct tokenizer *tokenizer) {
    g_slist_free_full(tokenizer->tokens, token_destroy_notify);
    g_free(tokenizer->buf);

    memset(tokenizer, 0, sizeof(*tokenizer));
}

static void token_destroy_notify(void *ptr) {
    struct token *tkn = ptr;
    g_free(tkn->text);
    g_free(tkn);
}

static bool is_ident(gchar const *txt) {
    if (!(isalpha(*txt) || *txt == '_'))
        return false;
    while (*txt) {
        if (!(isalpha(*txt) || isdigit(*txt) || *txt == '_'))
            return false;
        txt++;
    }
    return true;
}

static bool is_literal(char const *txt) {
    bool has_decimal_point = false;
    if (!isdigit(*txt))
        return false;
    while (*txt) {
        if (*txt == '.') {
            if (has_decimal_point)
                return true;
            else
                has_decimal_point = true;
        } else if (!isdigit(*txt)) {
            return false;
        }
        txt++;
    }
    return true;
}

static bool starts_new_token(gchar ch) {
    return (ch == '+' || ch == '-' || ch == '*' || ch == '/' ||
            ch == '(' || ch == ')' || ch == '{' || ch == '}' ||
            ch == ',' || isspace(ch));
}

static enum token_class pick_token_class(gchar const *txt) {
    if (txt[0] && !txt[1]) {
        switch (txt[0]) {
        case '+':
            return TOKEN_CLASS_PLUS;
        case '-':
            return TOKEN_CLASS_MINUS;
        case '/':
            return TOKEN_CLASS_SLASH;
        case '(':
            return TOKEN_CLASS_OPEN_PAREN;
        case ')':
            return TOKEN_CLASS_CLOSE_PAREN;
        case '{':
            return TOKEN_CLASS_OPEN_BRACE;
        case '}':
            return TOKEN_CLASS_CLOSE_BRACE;
        case ',':
            return TOKEN_CLASS_COMMA;
        }
    }

    int str_first, str_last;
    size_t nch = strlen(txt);
    string_bounds(txt, nch, &str_first, &str_last);
    if (str_first >= 0 && str_last >= 0) {
        if (str_last != nch - 1)
            g_error("corrupted tokenizer state; failure to notice end of string \"%s\"\n", txt);
        else
            return TOKEN_CLASS_STRING;
    }

    if (is_literal(txt))
        return TOKEN_CLASS_LITERAL;

    if (is_ident(txt))
        return TOKEN_CLASS_IDENTIFIER;

    while (*txt)
        if (!isspace(*txt++))
            return TOKEN_CLASS_INVALID;
    return TOKEN_CLASS_WHITESPACE;
}

static bool tokenizer_in_string(struct tokenizer *tokp) {
    unsigned index;
    bool instr = false, escape = false;
    for (index = 0; index < tokp->bufnext; index++) {
        if (tokp->buf[index] == '\\')
            escape = !escape;
        else if (tokp->buf[index] == '"' && !escape)
            instr = !instr;
        else
            escape = false;
    }
    return instr;
}

int tokenizer_push(struct tokenizer *tokenizer, gchar ch, GError **error) {
    if (ch == '"') {
        if (tokenizer->bufnext < tokenizer->bufmax)
            tokenizer->buf[tokenizer->bufnext++] = ch;
        else
            goto bufmax_error;

        int str_first, str_last;
        string_bounds(tokenizer->buf, tokenizer->bufnext, &str_first, &str_last);
        if (str_first >= 0 && str_last >= 0) {
            int success = tokenizer_finalize(tokenizer, error);
            if (success != 0)
                return success;
        }
    } else if (!tokenizer_in_string(tokenizer) && starts_new_token(ch)) {
        int success = tokenizer_finalize(tokenizer, error);
        if (success != 0)
            return success;

        if (!isspace(ch)) {
            struct token *tkn = g_malloc(sizeof(struct token));
            tkn->class = (enum token_class)ch;
            tkn->text = g_malloc(2 * sizeof(gchar));
            tkn->text[0] = ch;
            tkn->text[1] = '\0';
            tokenizer->tokens = g_slist_append(tokenizer->tokens, tkn);
        }
    } else {
        if (tokenizer->bufnext < tokenizer->bufmax)
            tokenizer->buf[tokenizer->bufnext++] = ch;
        else
            goto bufmax_error;
    }

    return 0;

 bufmax_error:
    g_set_error(error, SPECTRA_TOKENIZER_ERROR,
                SPECTRA_TOKENIZER_BUFFER_OVERFLOW,
                "token length exceeds BUFMAX (%d)\n",
                (int)tokenizer->bufmax);
    return -1;
}

char const *token_class_str(enum token_class class) {
    switch (class) {
    case TOKEN_CLASS_INVALID:
        return "INVALID";
    case TOKEN_CLASS_WHITESPACE:
        return "WHITESPACE";
    case TOKEN_CLASS_IDENTIFIER:
        return "IDENTIFIER";
    case TOKEN_CLASS_LITERAL:
        return "LITERAL";
    case TOKEN_CLASS_PLUS:
        return "PLUS";
    case TOKEN_CLASS_MINUS:
        return "MINUS";
    case TOKEN_CLASS_AST:
        return "ASTERISK";
    case TOKEN_CLASS_SLASH:
        return "SLASH";
    case TOKEN_CLASS_OPEN_PAREN:
        return "OPEN_PAREN";
    case TOKEN_CLASS_CLOSE_PAREN:
        return "CLOSE_PAREN";
    case TOKEN_CLASS_OPEN_BRACE:
        return "OPEN_BRACE";
    case TOKEN_CLASS_CLOSE_BRACE:
        return "CLOSE_BRACE";
    case TOKEN_CLASS_COMMA:
        return "COMMA";
    default:
        return "UNKNOWN";
    }
}

int tokenizer_finalize(struct tokenizer *tokenizer, GError **error) {
    if (tokenizer->bufnext > 0) {
        tokenizer->buf[tokenizer->bufnext] = '\0';
        enum token_class class = pick_token_class(tokenizer->buf);
        if (class == TOKEN_CLASS_INVALID) {
            g_set_error(error, SPECTRA_TOKENIZER_ERROR,
                        SPECTRA_TOKENIZER_INVALID_TOKEN,
                        "invalid token: \"%s\"\n", tokenizer->buf);
            return -1;
        } else if (class != TOKEN_CLASS_WHITESPACE) {
            struct token *tkn = g_malloc(sizeof(struct token));
            tkn->class = class;
            tkn->text = g_strdup(tokenizer->buf);

            if (class == TOKEN_CLASS_STRING) {
                char *curs = tkn->text;
                bool escape = false;
                while (*curs) {
                    if (*curs == '\\') {
                        if (curs[1])
                            memmove(curs, curs + 1, strlen(curs + 1) + 1);
                        else {
                            g_set_error(error, SPECTRA_TOKENIZER_ERROR,
                                        SPECTRA_TOKENIZER_UNTERMINATED_ESCAPE,
                                        "string ends in escape");
                            return -1;
                        }
                    }
                    curs++;
                }
            }

            tokenizer->tokens = g_slist_append(tokenizer->tokens, tkn);
        }

        tokenizer->bufnext = 0;
        memset(tokenizer->buf, 0, sizeof(gchar) * (tokenizer->bufmax + 1));
    }

    return 0;
}

static void print_single_token(gpointer datp, gpointer user_data) {
    struct token *tkn = datp;
    g_info("[ %s ] \"%s\"\n", token_class_str(tkn->class), tkn->text);
}

void tokenizer_print_all(struct tokenizer *tokenizer) {
    g_slist_foreach(tokenizer->tokens, print_single_token, NULL);
}

static void string_bounds(char const *str, unsigned n_chars,
                          int *str_first_p, int *str_last_p) {
    bool is_escaped = false;
    int str_first = -1, str_last = -1, index;
    for (index = 0; index < n_chars && str[index]; index++) {
        if (str[index] == '\\') {
            is_escaped = !is_escaped;
        } else if (!is_escaped && str[index] == '"') {
            if (str_first >= 0) {
                if (str_last >= 0) {
                    g_error("token \"%s\" has characters after the end of the string!\n", str);
                    goto the_end;
                } else {
                    str_last = index;
                }
            } else {
                str_first = index;
            }
            is_escaped = false;
        } else {
            is_escaped = false;
        }
    }

 the_end:
    *str_first_p = str_first;
    *str_last_p = str_last;
}
