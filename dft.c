/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <err.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "vector.h"
#include "dft.h"

static GString *spectra_dft_signal_to_str(SpectraSignal *sigp);
static void spectra_dft_signal_accumulate_inputs(SpectraSignal *sigp,
                                                 GPtrArray *inputs);
static void spectra_dft_signal_finalize(GObject *gobj);

static void print_complex_data(GPtrArray *datap, char const *name) {
    if (name)
        printf("%s: [", name);
    else
        printf("[");

    char const *pref=" ";
    unsigned index;
    for (index = 0; index < datap->len; index++) {
        SpectraVector *vecp = g_ptr_array_index(datap, index);
        spectra_scalar mag = spectra_vector_mag(vecp);
        spectra_scalar phase = spectra_vector_phase(vecp);
        if (fabs(mag) <= 0.001)
            printf("%s0.0", pref);
        else if (fabs(phase) <= 0.001)
            printf("%s%f", pref, mag);
        else {
            printf("%s%f*exp(%sj*%f)", pref, mag, phase >= 0.0 ? "" : "-", fabs(phase));
        }
        pref = ", ";
    }
    puts(" ]");
}

unsigned bit_rev_8(unsigned input) {
    unsigned lut[16] = {
        0x0, 0x8, 0x4, 0xc,
        0x2, 0xa, 0x6, 0xe,
        0x1, 0x9, 0x5, 0xd,
        0x3, 0xb, 0x7, 0xf
    };
    return (lut[input & 0xf] << 4) | lut[(input >> 4) & 0xf];
}

unsigned bit_rev_16(unsigned input) {
    return (bit_rev_8(input & 0xff) << 8) | bit_rev_8((input >> 8) & 0xff);
}

unsigned bit_rev_32(unsigned input) {
    return (bit_rev_16(input & 0xffff) << 16) | bit_rev_16((input >> 16) & 0xffff);
}

unsigned bit_rev_n(unsigned input, unsigned n_bits) {
    unsigned bit = 0;//1 << n_bits;//n_bits - 1 ???
    unsigned retval = 0;

    for (bit = 0; bit < n_bits; bit++) {
        retval = (retval << 1) | (input & 1);
        input >>= 1;
    }
    return retval;
}

// in-place bit-reversal permutation
static void bit_rev_scramble_complex(GPtrArray *datp, unsigned len_log2) {
    unsigned len = 1 << len_log2;
    unsigned index_1;
    for (index_1 = 0; index_1 < len; index_1++) {
        unsigned index_2 = bit_rev_n(index_1, len_log2);

        /*
         * don't swap the same two indices twice!  Note that this also
         * filters out cases where index_1 == index_2 but there aren't
         * any side-effects so swapping doesn't do anything in those
         * cases anyways.
         */
        if (index_1 < index_2) {
            gpointer buf = g_ptr_array_index(datp, index_1);
            g_ptr_array_index(datp, index_1) =
                g_ptr_array_index(datp, index_2);
            g_ptr_array_index(datp, index_2) = buf;
        }
    }
}

static unsigned my_log2(unsigned input) {
    if (!input)
        errx(1, "there is no logarithm of zero, dumbass.");
    unsigned bit, last_set_bit = 0;
    for (bit = 0; input && bit < 32; bit++) {
        if (input & (1 << bit))
            last_set_bit = bit;
    }
    return last_set_bit;
}

enum spectra_dft_signal_prop_id {
    SPECTRA_DFT_SIGNAL_PROP_INVERSE = 1
};

struct _SpectraDftSignal {
    SpectraSignal parent;

    gboolean inverse;

    /*
     * not prerequisites in the usual sense (because we have no need for them
     * after the DFT is calculated) but we keep references to these around
     * because we need them for the to_str implementation.
     */
    SpectraSignal *source_sig;
};

G_DEFINE_FINAL_TYPE(SpectraDftSignal, spectra_dft_signal,
                    SPECTRA_TYPE_SAMPLED_SIGNAL)

static void spectra_dft_signal_init(SpectraDftSignal *sigp) {
    // these fields are intiialized by spectra_dft_ctor
    sigp->source_sig = NULL;

    sigp->inverse = FALSE;
}

static void spectra_dft_signal_get_prop(GObject *gobj, guint propno,
                                        GValue *valp, GParamSpec *spec) {
    SpectraDftSignal *sigp = SPECTRA_DFT_SIGNAL(gobj);
    switch (propno) {
    case SPECTRA_DFT_SIGNAL_PROP_INVERSE:
        g_value_set_boolean(valp, sigp->inverse);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, spec);
    }
}

static void spectra_dft_signal_set_prop(GObject *gobj, guint propno,
                                        GValue const *valp, GParamSpec *spec) {
    SpectraDftSignal *sigp = SPECTRA_DFT_SIGNAL(gobj);
    switch (propno) {
    case SPECTRA_DFT_SIGNAL_PROP_INVERSE:
        sigp->inverse = g_value_get_boolean(valp);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, spec);
    }
}

static void spectra_dft_signal_class_init(SpectraDftSignalClass *class) {
    GObjectClass *gobj_class = G_OBJECT_CLASS(class);
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(class);

    sig_class->to_str = spectra_dft_signal_to_str;
    sig_class->accumulate_inputs = spectra_dft_signal_accumulate_inputs;

    gobj_class->get_property = spectra_dft_signal_get_prop;
    gobj_class->set_property = spectra_dft_signal_set_prop;
    gobj_class->finalize = spectra_dft_signal_finalize;

    GParamSpec *is_inverse_spec =
        g_param_spec_boolean("inverse", "is inverse", "if true then this is the "
                             "inverse DFT that converts frequency-domain "
                             "representations of signals to the time-domain.  "
                             "If false then this is the \"forward\" (for lack of "
                             "a better term) DFT that converts a time-domain "
                             "representation to the frequency domain.",
                             FALSE,
                             G_PARAM_READWRITE |
                             G_PARAM_CONSTRUCT_ONLY |
                             G_PARAM_STATIC_STRINGS);

    g_object_class_install_property(gobj_class, SPECTRA_DFT_SIGNAL_PROP_INVERSE,
                                    is_inverse_spec);
}

static void spectra_dft_signal_finalize(GObject *gobj) {
    SpectraDftSignal *sigp = SPECTRA_DFT_SIGNAL(gobj);

    if (sigp->source_sig)
        g_object_unref(sigp->source_sig);

    G_OBJECT_CLASS(spectra_dft_signal_parent_class)->finalize(gobj);
}

static GString *spectra_dft_signal_to_str(SpectraSignal *sigp) {
    SpectraDftSignal *dftsigp = SPECTRA_DFT_SIGNAL(sigp);

    GString *ret = g_string_new("dft(");
    GString *source_sig_str = spectra_signal_to_string(dftsigp->source_sig);

    g_string_append_printf(ret, "%s)",
                           source_sig_str->str);

    g_string_free(source_sig_str, TRUE);

    return ret;
}

static void spectra_dft_signal_accumulate_inputs(SpectraSignal *sigp,
                                                 GPtrArray *inputs) {
    SPECTRA_SIGNAL_CLASS(spectra_dft_signal_parent_class)->
        accumulate_inputs(sigp, inputs);
    SpectraDftSignal *dft_sig_p = SPECTRA_DFT_SIGNAL(sigp);
    if (dft_sig_p->inverse)
        spectra_signal_add_input_to_set(inputs, "t");
    else
        spectra_signal_add_input_to_set(inputs, "w");
}

#define USE_FFT
#ifdef USE_FFT

#define DFT_ALGO_NAME "FFT Decimation-In-Frequency"

static void fft_inner_inner(SpectraVector **outp, SpectraVector **inp,
                            unsigned sz, spectra_scalar scale_factor,
                            spectra_scalar twiddle_sign) {
    if (sz == 1) {
        /*******************************************************************
         **
         ** < exp(-j*w*n*t) | exp(-j*w*n*t) > == 2*pi/w == P
         ** therefore exp(-j*w*n*t) needs to be evenly divided by P to make
         ** it orthonormal.
         **
         ** of course, F=1/P, so multiplying by F is equivalent to
         ** dividing by P.
         **
         ** since this is discrete-time, we also need to divide by the
         ** number of harmonics/samples.  So scale_factor is the
         ** product of fundamental frequency and samples/harmonics.
         **
         ******************************************************************/
        *outp = spectra_vector_scale(*inp, scale_factor);
        return;
    } else {
        SpectraVector **g1 = g_malloc0_n(sz / 2, sizeof(SpectraVector*));
        SpectraVector **g2 = g_malloc0_n(sz / 2, sizeof(SpectraVector*));

        unsigned index;
        for (index = 0; index < sz / 2; index++) {
            SpectraVector *lhs = inp[index];
            SpectraVector *rhs = inp[index + sz / 2];
            SpectraVector *diff = spectra_vector_sub(lhs, rhs);
            SpectraVector *twiddle =
                spectra_complex_exponential_new(1.0, twiddle_sign * 2.0 * M_PI * index / sz);

            g1[index] = spectra_vector_add(lhs, rhs);
            g2[index] = spectra_vector_mult(diff, twiddle);

            g_object_unref(diff);
            g_object_unref(twiddle);
        }

        /*
         * TODO: since this isn't an in-place algorithm, there ought
         * not to be any reason for bit_rev_scramble_complex to be
         * needed to unscramble the transform.
         *
         * the transform is scrambled because we pass outp + sz / 2
         * for the destination parameter.  Should we instead be
         * passing outp + 1?  but then does fft_inner_inner need to
         * know its place in the root-level transform????
         */
        fft_inner_inner(outp, g1, sz / 2, scale_factor, twiddle_sign);
        fft_inner_inner(outp + sz / 2, g2, sz / 2, scale_factor, twiddle_sign);

        for (index = 0; index < sz / 2; index++) {
            g_object_unref(g1[index]);
            g_object_unref(g2[index]);
        }
        g_free(g1);
        g_free(g2);
    }
}

GPtrArray* fft_inner(GPtrArray *inp, spectra_scalar scale_factor,
                     spectra_scalar harmonic_sign) {
    SpectraVector **harmonics = g_malloc0_n(inp->len, sizeof(SpectraVector*));
    fft_inner_inner(harmonics, (SpectraVector**)inp->pdata, inp->len,
                    scale_factor, harmonic_sign);
    return g_ptr_array_new_take((gpointer*)harmonics, inp->len, g_object_unref);
}

#else

#define DFT_ALGO_NAME "Slow Fourier Transform"

static void sft_inner_inner(SpectraVector **outp, SpectraVector **inp,
                            unsigned sz, spectra_scalar scale_factor,
                            spectra_scalar harmonic_sign) {
    unsigned harmonic_index, sample_index;

    for (harmonic_index = 0; harmonic_index < sz; harmonic_index++) {
        SpectraVector *sum = spectra_real_new(0.0);
        for (sample_index = 0; sample_index < sz; sample_index++) {
            spectra_scalar log_scalar = harmonic_sign *
                2.0 * M_PI *
                (double)sample_index *
                (double)harmonic_index / (double)sz;
            SpectraVector *axis = spectra_complex_exponential_new(1.0, log_scalar);
            SpectraVector *proj =
                spectra_vector_mult(inp[sample_index], axis);
            {
                SpectraVector *old_sum = sum;
                sum = spectra_vector_add(old_sum, proj);
                g_object_unref(old_sum);
            }

            g_object_unref(proj);
            g_object_unref(axis);
        }
        /*******************************************************************
         **
         ** < exp(-j*w*n*t) | exp(-j*w*n*t) > == 2*pi/w == P
         ** therefore exp(-j*w*n*t) needs to be evenly divided by P to make
         ** it orthonormal.
         **
         ** of course, F=1/P, so multiplying by F is equivalent to
         ** dividing by P.
         **
         ** since this is discrete-time, we also need to divide by the
         ** number of harmonics/samples.  So scale_factor is the
         ** product of fundamental frequency and samples/harmonics.
         **
         ******************************************************************/
        *outp++ = spectra_vector_scale(sum, scale_factor);
        g_object_unref(sum);
    }
}

/*******************************************************************************
 **
 ** "Slow Fourier Transform"
 **
 ** the O(N*N) algorithm implied by the definition of the DFT, as opposed to the
 ** "Fast Fourier Transform" algorithms which reduce complexity to O(N*log(N))
 ** by recursively factoring it into pairs of N/2-sized discrete fourier transforms.
 **
 ******************************************************************************/
// sft - "Slow Fourier Transform"
static GPtrArray *sft_inner(GPtrArray *inp,
                            spectra_scalar scale_factor,
                            spectra_scalar harmonic_sign) {
    SpectraVector **harmonics = g_malloc0_n(inp->len, sizeof(SpectraVector*));
    sft_inner_inner(harmonics, (SpectraVector**)inp->pdata, inp->len,
                    scale_factor, harmonic_sign);
    return g_ptr_array_new_take((gpointer*)harmonics, inp->len, g_object_unref);
}
#endif

/*******************************************************************************
 **
 ** PREREQUISITES:
 **     * signal to create DFT of
 **       (must be SpectraSampledSignal, with power-of-two length)
 **
 ******************************************************************************/
SpectraSignal *spectra_dft_ctor(SpectraConstantBuffer *cbuf,
                                gchar const *ident,
                                GPtrArray *prereq) {
    gboolean inverse;
    if (strcmp(ident, "inv_dft") == 0)
        inverse = TRUE;
    else if (strcmp(ident, "dft") == 0)
        inverse = FALSE;
    else
        errx(1, "%s was called using unrecognized identifier \"%s\"\n", G_STRFUNC, ident);

    gpointer prereq0 = g_ptr_array_index(prereq, 0);
    if (prereq->len == 2 &&
        SPECTRA_IS_SAMPLED_SIGNAL(prereq0)) {
        SpectraSampledSignal *input =
            SPECTRA_SAMPLED_SIGNAL(prereq0);
        GPtrArray *time_domain = spectra_sampled_signal_data(input);
        guint sz = time_domain->len;
        if (sz & (sz - 1)) {
            errx(1, "%s function must only ever be called with power-of-two \n"
                 "length input (your input signal had a length of %u).\n"
                 "The resample function can be used to create a power-of-two \n"
                 "length signal from a signal of arbitrary length\n",
                 G_STRFUNC);
        }
        guint sz_log2 = my_log2(sz);
        spectra_scalar input_sample_period =
            spectra_signal_sample_period(SPECTRA_SIGNAL(input));

        if (sz < 512)
            print_complex_data(time_domain, "input data");

        GPtrArray *freq_domain;
        time_t start_timestamp = time(NULL), end_timestamp;

        printf("%s - beginning \"%s%s\" algorithm, N=%d\n",
               G_STRFUNC, inverse ? "inverse " : "", DFT_ALGO_NAME, sz);
#ifdef USE_FFT
        if (inverse) {
            freq_domain = fft_inner(time_domain, 1.0 / sz, -1.0);
        } else {
            freq_domain = fft_inner(time_domain, 1.0, 1.0);
        }
#else
        if (inverse) {
            freq_domain = sft_inner(time_domain, 1.0 / sz, -1.0);
        } else {
            freq_domain = sft_inner(time_domain, 1.0, 1.0);
        }
#endif

        end_timestamp = time(NULL);
        double n_seconds = difftime(end_timestamp, start_timestamp);
        printf("%s - \"%s\" algorithm completed in %f seconds\n",
               G_STRFUNC, DFT_ALGO_NAME, n_seconds);

        if (freq_domain->len < 512)
            print_complex_data(freq_domain,
#ifdef USE_FFT
                           "output data (scrambled)"
#else
                           "output data"
#endif
                           );

#ifdef USE_FFT
        bit_rev_scramble_complex(freq_domain, sz_log2);
        if (freq_domain->len < 512)
            print_complex_data(freq_domain, "output data (unscrambled)");
#endif
        fflush(stdout);

        GPtrArray *prereq_actual = g_ptr_array_new_full(1, g_object_unref);
        g_ptr_array_add(prereq_actual, g_object_ref(g_ptr_array_index(prereq, 1)));

        SpectraDftSignal *sigp = g_object_new(SPECTRA_TYPE_DFT_SIGNAL,
                                              "vecs", freq_domain,
                                              "cbuf", cbuf,
                                              "prereq", prereq_actual,
                                              "sampleperiod", 1.0,
                                              "inverse", inverse,
                                              NULL);
        printf("DFT sample period: %f\n", spectra_signal_sample_period(SPECTRA_SIGNAL(sigp)));

        sigp->source_sig = SPECTRA_SIGNAL(g_object_ref(input));

        g_ptr_array_unref(time_domain);
        g_ptr_array_unref(freq_domain);
        g_ptr_array_unref(prereq_actual);

        return SPECTRA_SIGNAL(sigp);
    } else {
        errx(1, "%s function only accepts as its input a single sampled "
             "signal, with power-of-two length.  If you would like to perform "
             "the dft of an arbitrary non-sampled signal you must use the "
             "resample function to convert it to a sampled signal.", ident);
    }
}
