/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef AUDIO_H_
#define AUDIO_H_

#include <stdbool.h>

#include <glib.h>
#include <SDL2/SDL.h>

#include "vector.h"

struct audio_output {
    SDL_AudioDeviceID dev;
    unsigned n_chans;
    spectra_scalar sample_rate;

    bool is_playing;

    GPtrArray *samples;
    guint pos;
};

// user must independently call SDL_INIT(SDL_INIT_AUDIO) first
int audio_init(struct audio_output *ao, unsigned n_chans, int sample_rate,
               GPtrArray *samples);
void audio_cleanup(struct audio_output *ao);

void audio_set_samples(struct audio_output *ao, GPtrArray *samples);

void audio_start(struct audio_output *ao);
void audio_stop(struct audio_output *ao);

#endif
