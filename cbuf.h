/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef CBUF_H_
#define CBUF_H_

#include <glib.h>
#include <glib-object.h>

#include "vector.h"

G_BEGIN_DECLS

typedef enum {
    SPECTRA_CONSTANT_BUFFER_WRONG_TYPE,
    SPECTRA_CONSTANT_BUFFER_INVALID_INDEX
} SpectraConstantBufferError;

enum spectra_constant_type {
    SPECTRA_CONSTANT_TYPE_FUNCTION,
    SPECTRA_CONSTANT_TYPE_VECTOR
};

#define SPECTRA_ADDR_INVALID (~0)

#define SPECTRA_CONSTANT_MAX 64

#define SPECTRA_TYPE_CONSTANT_BUFFER (spectra_constant_buffer_get_type())
G_DECLARE_FINAL_TYPE(SpectraConstantBuffer, spectra_constant_buffer,
                     SPECTRA, CONSTANT_BUFFER, GObject)

SpectraConstantBuffer *spectra_constant_buffer_new(void);

void spectra_constant_buffer_define(SpectraConstantBuffer *cbuf, ...);
guint spectra_constant_buffer_get_index(SpectraConstantBuffer *cbuf,
                                        gchar const *sym);

gchar const *spectra_constant_buffer_reverse_query(SpectraConstantBuffer *cbuf,
                                                   guint index);

gpointer spectra_constant_buffer_function(SpectraConstantBuffer *cbuf,
                                          guint index, GError **errp);
SpectraVector *spectra_constant_buffer_vector(SpectraConstantBuffer *cbuf,
                                              guint index, GError **errp);

G_END_DECLS

#endif
