/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <err.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>

#include "tokens.h"
#include "functions.h"
#include "tokens.h"
#include "parser.h"
#include "program.h"
#include "symbol.h"
#include "cbuf.h"
#include "dft.h"

#include "signal.h"

static void
spectra_signal_default_accumulate_inputs_implementation(SpectraSignal *sigp,
                                                        GPtrArray *inputs);

/*******************************************************************************
 **
 ** base address for string indices
 **
 ******************************************************************************/
#define SPECTRA_SIGNAL_STRING_FIRST 0xb16b00b5
enum spectra_signal_addr_type {
    SPECTRA_SIGNAL_ADDR_INVALID,
    SPECTRA_SIGNAL_ADDR_SCALAR,
    SPECTRA_SIGNAL_ADDR_STRING
};
static enum spectra_signal_addr_type spectra_signal_classify_addr(guint addr) {
    if (addr < SPECTRA_SIGNAL_STRING_FIRST)
        return SPECTRA_SIGNAL_ADDR_SCALAR;
    else
        return SPECTRA_SIGNAL_ADDR_STRING;
}

static guint spectra_signal_compile_node(SpectraSignal *sigp,
                                         SpectraProgram *prog,
                                         GArray *prereq_outputs);
enum spectra_signal_prop_id {
    SPECTRA_SIGNAL_PROP_ID_SAMPLE_PERIOD = 1,
    SPECTRA_SIGNAL_PROP_ID_PREREQ,
    SPECTRA_SIGNAL_PROP_ID_CBUF
};

typedef struct {
    spectra_scalar sample_period;
    GPtrArray *prereq;

    /*
     * cache of compiled program. not instantiated until the first call to
     * spectra_signal_compile.
     */
    SpectraProgram *prog;

    SpectraConstantBuffer *cbuf;
} SpectraSignalPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(SpectraSignal, spectra_signal, G_TYPE_OBJECT)

static void spectra_signal_finalize(GObject *gobj);
static GString *
spectra_signal_default_summary_implementation(SpectraSignal *sigp);

static void spectra_signal_get_prop(GObject *gobj, guint propno,
                                    GValue *valp, GParamSpec *specp) {
    SpectraSignal *sigp = SPECTRA_SIGNAL(gobj);
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    switch (propno) {
    case SPECTRA_SIGNAL_PROP_ID_SAMPLE_PERIOD:
        g_value_set_double(valp, priv->sample_period);
        break;
    case SPECTRA_SIGNAL_PROP_ID_PREREQ:
        g_return_if_fail(priv->prereq);
        g_value_set_boxed(valp, priv->prereq);
        break;
    case SPECTRA_SIGNAL_PROP_ID_CBUF:
        g_warn_if_fail(priv->cbuf);
        g_value_set_object(valp, priv->cbuf);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, specp);
    }
}

static void
spectra_signal_copy_prereq_single(gpointer prereq, gpointer ptr_array) {
    g_ptr_array_add(ptr_array, g_object_ref(prereq));
}

static void spectra_signal_set_prop(GObject *gobj, guint propno,
                                    GValue const *valp, GParamSpec *specp) {
    SpectraSignal *sigp = SPECTRA_SIGNAL(gobj);
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    switch (propno) {
    case SPECTRA_SIGNAL_PROP_ID_SAMPLE_PERIOD:
        priv->sample_period = g_value_get_double(valp);
        break;
    case SPECTRA_SIGNAL_PROP_ID_PREREQ:
        {
            g_warn_if_fail(priv->prereq && !priv->prereq->len);
            GPtrArray *prereq_in = g_value_get_boxed(valp);
            if (prereq_in) {
                g_ptr_array_foreach(prereq_in,
                                    spectra_signal_copy_prereq_single,
                                    priv->prereq);
            }
        }
        break;
    case SPECTRA_SIGNAL_PROP_ID_CBUF:
        g_return_if_fail(!priv->cbuf);
        priv->cbuf = g_object_ref(g_value_get_object(valp));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, specp);
    }
}

SpectraConstantBuffer *spectra_signal_cbuf(SpectraSignal *sigp) {
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    g_warn_if_fail(priv->cbuf);
    return priv->cbuf;
}

spectra_scalar spectra_signal_sample_period(SpectraSignal *sigp) {
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    return priv->sample_period;
}

static void spectra_signal_init(SpectraSignal *sigp) {
    g_print("%s\n", G_STRFUNC);
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    priv->prereq = g_ptr_array_new_with_free_func(g_object_unref);
    priv->prog = NULL;
}

static void spectra_signal_finalize(GObject *gobj) {
    SpectraSignal *sigp = SPECTRA_SIGNAL(gobj);
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    g_return_if_fail(priv->prereq);
    g_ptr_array_unref(priv->prereq);
    if (priv->prog)
        g_object_unref(priv->prog);
    G_OBJECT_CLASS(spectra_signal_parent_class)->finalize(gobj);
}

static void spectra_signal_class_init(SpectraSignalClass *classp) {
    GObjectClass *gobj_classp = G_OBJECT_CLASS(classp);

    gobj_classp->set_property = spectra_signal_set_prop;
    gobj_classp->get_property = spectra_signal_get_prop;

    classp->summary = spectra_signal_default_summary_implementation;
    classp->accumulate_inputs =
        spectra_signal_default_accumulate_inputs_implementation;

    GParamSpec *samp_period_spec = g_param_spec_double("sampleperiod",
                                                       "sampleperiod",
                                                       "sample period",
                                                       0.0, DBL_MAX,
                                                       0.0,
                                                       G_PARAM_READWRITE |
                                                       G_PARAM_STATIC_STRINGS |
                                                       G_PARAM_CONSTRUCT_ONLY);
    GParamSpec *prereq_spec = g_param_spec_boxed("prereq", "prerequisites",
                                                 "GPtrArray of pointers to "
                                                 "SpectraSignal instances "
                                                 "which are prerequisites of "
                                                 "this node",
                                                 G_TYPE_PTR_ARRAY,
                                                 G_PARAM_READWRITE |
                                                 G_PARAM_STATIC_STRINGS |
                                                 G_PARAM_CONSTRUCT_ONLY);

    GParamSpec *cbuf_spec =
        g_param_spec_object("cbuf", "constant buffer",
                            "SpectraConstantBuffer listing all defined "
                            "constant values",
                            SPECTRA_TYPE_CONSTANT_BUFFER,
                            G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS);

    g_object_class_install_property(gobj_classp,
                                    SPECTRA_SIGNAL_PROP_ID_SAMPLE_PERIOD,
                                    samp_period_spec);
    g_object_class_install_property(gobj_classp,
                                    SPECTRA_SIGNAL_PROP_ID_PREREQ,
                                    prereq_spec);
    g_object_class_install_property(gobj_classp,
                                    SPECTRA_SIGNAL_PROP_ID_CBUF,
                                    cbuf_spec);
}

static guint
spectra_signal_compile_subtree(SpectraSignal *root,
                               gpointer prog_void) {
    SpectraProgram *prog = SPECTRA_PROGRAM(prog_void);
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(root);
    GArray *prereq_outputs = g_array_new(FALSE, FALSE, sizeof(guint));

    guint prereq_index;
    for (prereq_index = 0; prereq_index < priv->prereq->len; prereq_index++) {
        SpectraSignal *prereq = g_ptr_array_index(priv->prereq, prereq_index);
        guint output_index = spectra_signal_compile_subtree(prereq, prog);

        if (output_index == PROGRAM_ADDR_INVALID) {
            g_array_unref(prereq_outputs);
            return PROGRAM_ADDR_INVALID;
        }

        g_array_append_val(prereq_outputs, output_index);
    }
    guint output = spectra_signal_compile_node(root, prog, prereq_outputs);

    g_array_unref(prereq_outputs);

    g_warn_if_fail(output != PROGRAM_ADDR_INVALID);
    return output;
}

static guint
spectra_signal_compile_node(SpectraSignal *sigp,
                            SpectraProgram *prog,
                            GArray *prereq_outputs) {
    g_return_val_if_fail(SPECTRA_IS_SIGNAL(sigp), PROGRAM_ADDR_INVALID);
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_GET_CLASS(sigp);
    g_return_val_if_fail(sig_class->compile_node, PROGRAM_ADDR_INVALID);
    return sig_class->compile_node(sigp, prog, prereq_outputs);
}

GString *spectra_signal_to_string(SpectraSignal *sigp) {
    SpectraSignalClass *class = SPECTRA_SIGNAL_GET_CLASS(sigp);
    g_return_val_if_fail(class->to_str, g_string_new(NULL));
    return class->to_str(sigp);
}

GString *spectra_signal_summary(SpectraSignal *sigp) {
    SpectraSignalClass *class = SPECTRA_SIGNAL_GET_CLASS(sigp);
    g_return_val_if_fail(class->summary, g_string_new(NULL));
    return class->summary(sigp);
}

static GString *spectra_signal_default_summary_implementation(SpectraSignal *sigp) {
    return spectra_signal_to_string(sigp);
}

static void spectra_signal_accumulate_inputs(SpectraSignal *sigp, GPtrArray *inputs) {
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_GET_CLASS(sigp);
    g_return_if_fail(sig_class->accumulate_inputs);
    sig_class->accumulate_inputs(sigp, inputs);
}

GPtrArray* spectra_signal_inputs(SpectraSignal *sigp) {
    GPtrArray *inputs = g_ptr_array_new_with_free_func(g_free);
    spectra_signal_accumulate_inputs(sigp, inputs);
    return inputs;
}

void
spectra_signal_add_input_to_set(GPtrArray *set, gchar const *input) {
    if (!g_ptr_array_find_with_equal_func(set, input, g_str_equal, NULL))
        g_ptr_array_add(set, g_strdup(input));
}

static void
spectra_signal_default_accumulate_inputs_implementation(SpectraSignal *sigp,
                                                        GPtrArray *inputs) {
    guint prereq_index;
    guint n_prereq = spectra_signal_prereq_count(sigp);
    for (prereq_index = 0; prereq_index < n_prereq; prereq_index++) {
        SpectraSignal *prereq = spectra_signal_prereq(sigp, prereq_index);
        spectra_signal_accumulate_inputs(prereq, inputs);
    }
}

/*******************************************************************************
 **
 ** SpectraLiteralSignal
 **
 ******************************************************************************/

struct _SpectraLiteralSignal {
    SpectraSignal parent;
    SpectraVector *litp;
};

G_DEFINE_FINAL_TYPE(SpectraLiteralSignal, spectra_literal_signal,
                    SPECTRA_TYPE_SIGNAL)

static guint
spectra_literal_signal_compile(SpectraSignal *sigp,
                               SpectraProgram *prog, GArray *prereq_outputs) {
    // TODO: UPDATE THIS FOR VECTORS

    g_warn_if_fail(prereq_outputs->len == 0);
    return spectra_program_literal(prog, SPECTRA_LITERAL_SIGNAL(sigp)->litp);
}

static void spectra_literal_signal_init(SpectraLiteralSignal *sigp) {
    sigp->litp = NULL;
}

static GString *spectra_literal_signal_to_str(SpectraSignal *sigp) {
    SpectraLiteralSignal *lit = SPECTRA_LITERAL_SIGNAL(sigp);
    return spectra_vector_gstring(lit->litp);
}

static void spectra_literal_signal_finalize(GObject *gobj) {
    g_object_unref(SPECTRA_LITERAL_SIGNAL(gobj)->litp);
    G_OBJECT_CLASS(spectra_literal_signal_parent_class)->finalize(gobj);
}

static void spectra_literal_signal_class_init(SpectraLiteralSignalClass *sigp) {
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(sigp);
    sig_class->compile_node = spectra_literal_signal_compile;
    sig_class->to_str = spectra_literal_signal_to_str;
    G_OBJECT_CLASS(sigp)->finalize = spectra_literal_signal_finalize;
}

SpectraSignal *spectra_literal_signal_new(SpectraConstantBuffer *cbuf, SpectraVector *litp) {
    SpectraLiteralSignal *sigp =
        g_object_new(SPECTRA_TYPE_LITERAL_SIGNAL, "cbuf", cbuf, NULL);
    sigp->litp = g_object_ref(litp);
    return SPECTRA_SIGNAL(sigp);
}

SpectraVector* spectra_literal_signal_value(SpectraSignal *sigp) {
    g_return_val_if_fail(SPECTRA_IS_LITERAL_SIGNAL(sigp), NULL);
    return SPECTRA_LITERAL_SIGNAL(sigp)->litp;
}

/*******************************************************************************
 **
 ** SpectraSumSignal
 **
 ******************************************************************************/

#define SPECTRA_TYPE_SUM_SIGNAL (spectra_sum_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraSumSignal, spectra_sum_signal, SPECTRA,
                     SUM_SIGNAL, SpectraSignal)

struct _SpectraSumSignal {
    SpectraSignal parent;
};

G_DEFINE_FINAL_TYPE(SpectraSumSignal, spectra_sum_signal, SPECTRA_TYPE_SIGNAL)

static GString *spectra_sum_signal_to_str(SpectraSignal *sigp) {
    guint n_prereq = spectra_signal_prereq_count(sigp);
    if (n_prereq) {
        gchar const *prefix = "";
        GString *ret = g_string_new("");
        guint index;
        for (index = 0; index < n_prereq; index++) {
            SpectraSignal *prereq = spectra_signal_prereq(sigp, index);
            GString *sub = spectra_signal_to_string(prereq);
            g_string_append_printf(ret, "%s(%s)", prefix, sub->str);
            g_string_free(sub, TRUE);
            prefix = " + ";
        }
        return ret;
    } else {
        return g_string_new("0");
    }
}

static guint
spectra_sum_signal_compile(SpectraSignal *sigp,
                           SpectraProgram *prog,
                           GArray *prereq_outputs) {
    g_return_val_if_fail(prereq_outputs->len >= 2, PROGRAM_ADDR_INVALID);
    guint prereq_index;
    guint accum_addr = g_array_index(prereq_outputs, guint, 0);
    for (prereq_index = 1; prereq_index < prereq_outputs->len; prereq_index++) {
        guint prereq_addr = g_array_index(prereq_outputs, guint, prereq_index);
        accum_addr = spectra_program_add(prog, accum_addr, prereq_addr);
    }
    return accum_addr;
}

static void spectra_sum_signal_init(SpectraSumSignal *sigp) {
}

static void spectra_sum_signal_class_init(SpectraSumSignalClass *sum_class) {
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(sum_class);
    sig_class->compile_node = spectra_sum_signal_compile;
    sig_class->to_str = spectra_sum_signal_to_str;
}

SpectraSignal *spectra_sum_signal_new_full(SpectraConstantBuffer *cbuf,
                                           GPtrArray *prereq) {
    return SPECTRA_SIGNAL(g_object_new(SPECTRA_TYPE_SUM_SIGNAL,
                                       "prereq", prereq,
                                       "cbuf", cbuf,
                                       NULL));
}

SpectraSignal *spectra_sum_signal_new(SpectraConstantBuffer *cbuf,
                                      guint n_operands,
                                      ...) {
    GPtrArray *prereq = g_ptr_array_new_full(n_operands, g_object_unref);

    va_list arg;
    va_start(arg, n_operands);

    while (n_operands--) {
        SpectraSignal *sigp = va_arg(arg, SpectraSignal*);
        g_ptr_array_add(prereq, g_object_ref(sigp));
    }
    va_end(arg);

    SpectraSignal *sum_sig = spectra_sum_signal_new_full(cbuf, prereq);
    g_ptr_array_unref(prereq);
    return sum_sig;
}


/*******************************************************************************
 **
 ** SpectraMultSignal
 **
 ******************************************************************************/

#define SPECTRA_TYPE_MULT_SIGNAL (spectra_mult_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraMultSignal, spectra_mult_signal, SPECTRA,
                     MULT_SIGNAL, SpectraSignal)

struct _SpectraMultSignal {
    SpectraSignal parent;
};

G_DEFINE_FINAL_TYPE(SpectraMultSignal, spectra_mult_signal, SPECTRA_TYPE_SIGNAL)

static GString *spectra_mult_signal_to_str(SpectraSignal *sigp) {
    guint n_prereq = spectra_signal_prereq_count(sigp);
    if (n_prereq) {
        gchar const *prefix = "";
        GString *ret = g_string_new("");
        guint index;
        for (index = 0; index < n_prereq; index++) {
            SpectraSignal *prereq = spectra_signal_prereq(sigp, index);
            GString *sub = spectra_signal_to_string(prereq);
            g_string_append_printf(ret, "%s(%s)", prefix, sub->str);
            g_string_free(sub, TRUE);
            prefix = " * ";
        }
        return ret;
    } else {
        return g_string_new("0");
    }
}

static guint spectra_mult_signal_compile(SpectraSignal *sigp,
                                         SpectraProgram *prog,
                                         GArray *prereq_outputs) {
    g_return_val_if_fail(prereq_outputs->len >= 2, PROGRAM_ADDR_INVALID);
    guint prereq_index;
    guint accum_addr = g_array_index(prereq_outputs, guint, 0);
    for (prereq_index = 1; prereq_index < prereq_outputs->len; prereq_index++) {
        guint prereq_addr = g_array_index(prereq_outputs, guint, prereq_index);
        accum_addr = spectra_program_mult(prog, accum_addr, prereq_addr);
    }
    return accum_addr;
}

static void spectra_mult_signal_init(SpectraMultSignal *sigp) {
}

static void spectra_mult_signal_class_init(SpectraMultSignalClass *mult_class) {
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(mult_class);
    sig_class->compile_node = spectra_mult_signal_compile;
    sig_class->to_str = spectra_mult_signal_to_str;
}

SpectraSignal *spectra_mult_signal_new_full(SpectraConstantBuffer *cbuf,
                                            GPtrArray *prereq) {
    return g_object_new(SPECTRA_TYPE_MULT_SIGNAL,
                        "cbuf", cbuf,
                        "prereq", prereq,
                        NULL);
}

SpectraSignal *spectra_mult_signal_new(SpectraConstantBuffer *cbuf,
                                       guint n_operands,
                                       ...) {
    GPtrArray *prereq = g_ptr_array_new_full(n_operands, g_object_unref);

    va_list arg;
    va_start(arg, n_operands);

    while (n_operands--) {
        SpectraSignal *sigp = va_arg(arg, SpectraSignal*);
        g_ptr_array_add(prereq, g_object_ref(sigp));
    }
    va_end(arg);

    SpectraSignal *mult_sig = spectra_mult_signal_new_full(cbuf, prereq);
    g_ptr_array_unref(prereq);
    return mult_sig;
}


/*******************************************************************************
 **
 ** SpectraDivSignal
 **
 ******************************************************************************/

#define SPECTRA_TYPE_DIV_SIGNAL (spectra_div_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraDivSignal, spectra_div_signal, SPECTRA,
                     DIV_SIGNAL, SpectraSignal)

struct _SpectraDivSignal {
    SpectraSignal parent;
};

G_DEFINE_FINAL_TYPE(SpectraDivSignal, spectra_div_signal, SPECTRA_TYPE_SIGNAL)

static GString *spectra_div_signal_to_str(SpectraSignal *sigp) {
    g_return_val_if_fail(spectra_signal_prereq_count(sigp) == 2, g_string_new(""));
    GString *sub[2] = {
        spectra_signal_to_string(spectra_signal_prereq(sigp, 0)),
        spectra_signal_to_string(spectra_signal_prereq(sigp, 1))
    };

    GString *ret = g_string_new(NULL);

    g_string_printf(ret, "(%s) / (%s)", sub[0]->str, sub[1]->str);

    g_string_free(sub[0], TRUE);
    g_string_free(sub[1], TRUE);

    return ret;
}

static guint spectra_div_signal_compile(SpectraSignal *sigp,
                                        SpectraProgram *prog, GArray *prereq) {
    g_return_val_if_fail(prereq->len == 2, PROGRAM_ADDR_INVALID);
    guint dividend = g_array_index(prereq, guint, 0);
    guint divisor = g_array_index(prereq, guint, 1);
    return spectra_program_div(prog, dividend, divisor);
}

static void spectra_div_signal_init(SpectraDivSignal *sigp) {
}

static void spectra_div_signal_class_init(SpectraDivSignalClass *div_class) {
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(div_class);
    sig_class->compile_node = spectra_div_signal_compile;
    sig_class->to_str = spectra_div_signal_to_str;
}

SpectraSignal *spectra_div_signal_new(SpectraConstantBuffer *cbuf,
                                      SpectraSignal *dividend,
                                      SpectraSignal *divisor) {
    GPtrArray *prereq = g_ptr_array_new_with_free_func(g_object_unref);
    g_ptr_array_add(prereq, g_object_ref(dividend));
    g_ptr_array_add(prereq, g_object_ref(divisor));
    SpectraSignal *ret = g_object_new(SPECTRA_TYPE_DIV_SIGNAL,
                                      "cbuf", cbuf,
                                      "prereq", prereq,
                                      NULL);
    g_ptr_array_unref(prereq);
    return ret;
}

/*******************************************************************************
 **
 ** Symbol
 **
 ******************************************************************************/
#define SPECTRA_TYPE_SYMBOL_SIGNAL (spectra_symbol_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraSymbolSignal, spectra_symbol_signal, SPECTRA,
                     SYMBOL_SIGNAL, SpectraSignal)

struct _SpectraSymbolSignal {
    SpectraSignal parent;
    gchar *sym;
};

static GString *spectra_symbol_signal_to_str(SpectraSignal *sigp) {
    return g_string_new(SPECTRA_SYMBOL_SIGNAL(sigp)->sym);
}

static guint spectra_symbol_compile(SpectraSignal *sigp,
                                    SpectraProgram *prog,
                                    GArray *prereq_outputs) {
    g_warn_if_fail(prereq_outputs->len == 0);
    return spectra_program_symbol(prog, SPECTRA_SYMBOL_SIGNAL(sigp)->sym);
}

void spectra_symbol_signal_accumulate_inputs(SpectraSignal *sigp,
                                             GPtrArray *inputs) {
    SpectraConstantBuffer *cbuf = spectra_signal_cbuf(sigp);
    SpectraSymbolSignal *symsig = SPECTRA_SYMBOL_SIGNAL(sigp);
    if (spectra_constant_buffer_get_index(cbuf, symsig->sym) ==
        SPECTRA_ADDR_INVALID) {
        spectra_signal_add_input_to_set(inputs, symsig->sym);
    }
}

static void spectra_symbol_signal_init(SpectraSymbolSignal *symp) {
}

static void spectra_symbol_signal_finalize(GObject *gobj);

static void spectra_symbol_signal_class_init(SpectraSymbolSignalClass *class) {
    GObjectClass *gobj_class = G_OBJECT_CLASS(class);
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(class);

    gobj_class->finalize = spectra_symbol_signal_finalize;
    sig_class->compile_node = spectra_symbol_compile;
    sig_class->to_str = spectra_symbol_signal_to_str;
    sig_class->accumulate_inputs = spectra_symbol_signal_accumulate_inputs;
}

G_DEFINE_FINAL_TYPE(SpectraSymbolSignal, spectra_symbol_signal,
                    SPECTRA_TYPE_SIGNAL)

static void spectra_symbol_signal_finalize(GObject *gobj) {
    SpectraSymbolSignal *symp = SPECTRA_SYMBOL_SIGNAL(gobj);
    g_free(symp->sym);

    G_OBJECT_CLASS(spectra_symbol_signal_parent_class)->finalize(gobj);
}

SpectraSignal *spectra_symbol_signal_new(SpectraConstantBuffer *cbuf,
                                         gchar const *symbol) {
    guint cbuf_addr = spectra_constant_buffer_get_index(cbuf, symbol);
    gchar const *inputs = (cbuf_addr == SPECTRA_ADDR_INVALID) ? symbol : "";

    SpectraSymbolSignal *symp = g_object_new(SPECTRA_TYPE_SYMBOL_SIGNAL,
                                             "sampleperiod", 0.0,
                                             "cbuf", cbuf,
                                             NULL);
    symp->sym = g_strdup(symbol);

    return SPECTRA_SIGNAL(symp);
}

/*******************************************************************************
 **
 ** SpectraInvokeSignal
 **
 ******************************************************************************/

#define SPECTRA_TYPE_INVOKE_SIGNAL (spectra_invoke_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraInvokeSignal, spectra_invoke_signal,
                     SPECTRA, INVOKE_SIGNAL, SpectraSignal)

struct _SpectraInvokeSignal {
    SpectraSignal parent;
    gchar *ident;
};

G_DEFINE_FINAL_TYPE(SpectraInvokeSignal, spectra_invoke_signal, SPECTRA_TYPE_SIGNAL)

static GString *spectra_invoke_signal_to_str(SpectraSignal *sigp) {
    GString *ret = g_string_new("");
    g_string_printf(ret, "%s(", SPECTRA_INVOKE_SIGNAL(sigp)->ident);
    guint n_prereq = spectra_signal_prereq_count(sigp);
    guint index;
    gchar const *prefix = "";
    for (index = 0; index < n_prereq; index++) {
        SpectraSignal *prereq = spectra_signal_prereq(sigp, index);
        GString *sub = spectra_signal_to_string(prereq);
        g_string_append_printf(ret, "%s%s", prefix, sub->str);
        g_string_free(sub, TRUE);
        prefix = ", ";
    }
    g_string_append(ret, ")");
    return ret;
}

static guint spectra_invoke_signal_compile(SpectraSignal *sigp_parent,
                                           SpectraProgram *prog,
                                           GArray *prereq_outputs) {
    SpectraInvokeSignal *sigp = SPECTRA_INVOKE_SIGNAL(sigp_parent);
    GArray *arguments = g_array_sized_new(FALSE, FALSE,
                                          sizeof(struct spectra_program_argument),
                                          prereq_outputs->len);
    guint arg_index;
    for (arg_index = 0; arg_index < prereq_outputs->len; arg_index++) {
        struct spectra_program_argument arg;
        guint address = g_array_index(prereq_outputs, guint, arg_index);
        switch (spectra_signal_classify_addr(address)) {
        case SPECTRA_SIGNAL_ADDR_SCALAR:
            arg.tp = SPECTRA_PROGRAM_MEMORY;
            arg.index = address;
            break;
        case SPECTRA_SIGNAL_ADDR_STRING:
            arg.tp = SPECTRA_PROGRAM_STRING;
            arg.index = address - SPECTRA_SIGNAL_STRING_FIRST;
            break;
        default:
            errx(1, "unimplemented address range %08x", address);
        }
        g_array_append_val(arguments, arg);
    }

    guint output = spectra_program_invoke(prog, sigp->ident, arguments);
    g_array_unref(arguments);
    return output;
}

static void spectra_invoke_signal_init(SpectraInvokeSignal *sigp) {
    sigp->ident = NULL;
}

static void spectra_invoke_signal_finalize(GObject *gobj) {
    SpectraInvokeSignal *sigp = SPECTRA_INVOKE_SIGNAL(gobj);

    g_free(sigp->ident);

    G_OBJECT_CLASS(spectra_invoke_signal_parent_class)->finalize(gobj);
}

static void spectra_invoke_signal_class_init(SpectraInvokeSignalClass *class) {
    G_OBJECT_CLASS(class)->finalize = spectra_invoke_signal_finalize;
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(class);
    sig_class->compile_node = spectra_invoke_signal_compile;
    sig_class->to_str = spectra_invoke_signal_to_str;
}

SpectraSignal *spectra_invoke_signal_new_full(SpectraConstantBuffer *cbuf,
                                              gchar const *symbol,
                                              GPtrArray *arguments) {
    SpectraInvokeSignal *sigp = g_object_new(SPECTRA_TYPE_INVOKE_SIGNAL,
                                             "cbuf", cbuf,
                                             "prereq", arguments,
                                             NULL);
    sigp->ident = g_strdup(symbol);
    return SPECTRA_SIGNAL(sigp);
}

/*******************************************************************************
 **
 ** StringSignal
 **
 ******************************************************************************/

#define SPECTRA_TYPE_STRING_SIGNAL (spectra_string_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraStringSignal, spectra_string_signal, SPECTRA,
                     STRING_SIGNAL, SpectraSignal)

struct _SpectraStringSignal {
    SpectraSignal parent;
    gchar *text;
};

G_DEFINE_FINAL_TYPE(SpectraStringSignal, spectra_string_signal,
                    SPECTRA_TYPE_SIGNAL)

static GString *spectra_string_signal_summary(SpectraSignal *sigp) {
    return g_string_new(SPECTRA_STRING_SIGNAL(sigp)->text);
}

static GString *spectra_string_signal_to_string(SpectraSignal *sigp) {
    GString *ret = spectra_string_signal_summary(sigp);
    g_string_prepend(ret, "\"");
    g_string_append(ret, "\"");
    return ret;
}

static guint spectra_string_signal_compile(SpectraSignal *sigp,
                                           SpectraProgram *prog,
                                           GArray *prereq_outputs) {
    SpectraStringSignal *strsig = SPECTRA_STRING_SIGNAL(sigp);
    return spectra_program_string_literal(prog, strsig->text) +
        SPECTRA_SIGNAL_STRING_FIRST;
}

static void spectra_string_signal_finalize(GObject *gobj) {
    SpectraStringSignal *sigp = SPECTRA_STRING_SIGNAL(gobj);
    g_free(sigp->text);
    G_OBJECT_CLASS(spectra_string_signal_parent_class)->finalize(gobj);
}

static void spectra_string_signal_init(SpectraStringSignal *sigp) {
    sigp->text = NULL;
}

static void spectra_string_signal_class_init(SpectraStringSignalClass *class) {
    G_OBJECT_CLASS(class)->finalize = spectra_string_signal_finalize;
    SPECTRA_SIGNAL_CLASS(class)->compile_node = spectra_string_signal_compile;
    SPECTRA_SIGNAL_CLASS(class)->to_str = spectra_string_signal_to_string;
    SPECTRA_SIGNAL_CLASS(class)->summary = spectra_string_signal_summary;
}

SpectraSignal *
spectra_string_signal_new_full(SpectraConstantBuffer *cbuf,
                               gchar const *text) {
    SpectraStringSignal *sigp = g_object_new(SPECTRA_TYPE_STRING_SIGNAL,
                                             "cbuf", cbuf, NULL);
    sigp->text = g_strdup(text);
    return SPECTRA_SIGNAL(sigp);
}

SpectraSignal *spectra_string_signal_new(SpectraConstantBuffer *cbuf,
                                         gchar const *text) {
    return spectra_string_signal_new_full(cbuf, text);
}

static void spectra_signal_do_check_single_input(gpointer data_raw,
                                                 gpointer sig_raw) {
    gchar const *input = data_raw;
    SpectraSignal *sigp = sig_raw;

    g_warn_if_fail(spectra_signal_check_input(sigp, input));
}

SpectraProgram *spectra_signal_compile(SpectraSignal *sigp) {
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    if (priv->prog)
        return g_object_ref(priv->prog);

    GPtrArray *inputs = spectra_signal_inputs(sigp);

    g_ptr_array_foreach(inputs, spectra_signal_do_check_single_input, sigp);

    SpectraProgram *prog = spectra_program_new(priv->cbuf);
    spectra_program_state_transition(prog,
                                     SPECTRA_PROGRAM_STATE_COMPILE_IN_PROGRESS);
    guint output_index = spectra_signal_compile_subtree(sigp, prog);
    if (output_index == PROGRAM_ADDR_INVALID) {
        g_object_unref(prog);
        g_ptr_array_unref(inputs);
        return NULL;
    } else {
        spectra_program_terminate(prog, output_index);
        spectra_program_state_transition(prog, SPECTRA_PROGRAM_STATE_COMPILED);
        g_ptr_array_unref(inputs);
        priv->prog = g_object_ref(prog);
        return prog;
    }
}

// returns true if the given input is on sigp's inputs list
gboolean spectra_signal_check_input(SpectraSignal *sigp,
                                    gchar const *input_name) {
    GPtrArray *inp = spectra_signal_inputs(sigp);
    gboolean foundit = g_ptr_array_find_with_equal_func(inp, input_name,
                                                        g_str_equal, NULL);
    g_ptr_array_unref(inp);
    return foundit;
}

guint spectra_signal_prereq_count(SpectraSignal *sigp) {
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    return priv->prereq->len;
}

SpectraSignal *spectra_signal_prereq(SpectraSignal *sigp, guint index) {
    SpectraSignalPrivate *priv = spectra_signal_get_instance_private(sigp);
    g_return_val_if_fail(index < priv->prereq->len, NULL);
    return g_ptr_array_index(priv->prereq, index);
}

/*******************************************************************************
 **
 ** SampledSignal
 **
 ******************************************************************************/
typedef struct {
    GPtrArray *vecs;
} SpectraSampledSignalPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(SpectraSampledSignal,
                           spectra_sampled_signal,
                           SPECTRA_TYPE_SIGNAL)

guint spectra_sampled_signal_len(SpectraSampledSignal *sigp) {
    SpectraSampledSignalPrivate *priv =
        spectra_sampled_signal_get_instance_private(sigp);
    return priv->vecs->len;
}

SpectraVector *spectra_sampled_signal_index(SpectraSampledSignal *sigp,
                                            guint index) {
    SpectraSampledSignalPrivate *priv =
        spectra_sampled_signal_get_instance_private(sigp);
    if (index < priv->vecs->len)
        return g_object_ref(g_ptr_array_index(priv->vecs, index));
    else
        return spectra_real_new(0.0);
}

GPtrArray *spectra_sampled_signal_data(SpectraSampledSignal *sigp) {
    SpectraSampledSignalPrivate *priv =
        spectra_sampled_signal_get_instance_private(sigp);
    return g_ptr_array_ref(priv->vecs);
}

static guint spectra_sampled_signal_compile(SpectraSignal *sigp,
                                            SpectraProgram *prog,
                                            GArray *prereq_outputs) {
    g_return_val_if_fail(prereq_outputs->len == 1, PROGRAM_ADDR_INVALID);
    SpectraSampledSignal *sig_smp = SPECTRA_SAMPLED_SIGNAL(sigp);
    SpectraSampledSignalPrivate *priv =
        spectra_sampled_signal_get_instance_private(sig_smp);
    guint table_index =
        spectra_program_add_vector_table(prog, priv->vecs,
                                         1.0 / spectra_signal_sample_period(sigp));
    guint time_input = g_array_index(prereq_outputs, guint, 0);
    return spectra_program_sample_vector_table(prog, table_index, time_input);
}

static void spectra_sampled_signal_init(SpectraSampledSignal *sigp) {
    SpectraSampledSignal *sig_smp = SPECTRA_SAMPLED_SIGNAL(sigp);
    SpectraSampledSignalPrivate *priv =
        spectra_sampled_signal_get_instance_private(sig_smp);
    priv->vecs = NULL;
}

static void spectra_sampled_signal_finalize(GObject *gobj) {
    SpectraSampledSignal *sig_smp = SPECTRA_SAMPLED_SIGNAL(gobj);
    SpectraSampledSignalPrivate *priv =
        spectra_sampled_signal_get_instance_private(sig_smp);
    g_ptr_array_unref(priv->vecs);
    G_OBJECT_CLASS(spectra_sampled_signal_parent_class)->finalize(gobj);
}

static void spectra_sampled_signal_get_prop(GObject *gobj, guint propno,
                                            GValue *valp, GParamSpec *spec) {
    SpectraSampledSignal *sigp = SPECTRA_SAMPLED_SIGNAL(gobj);
    SpectraSampledSignalPrivate *priv =
        spectra_sampled_signal_get_instance_private(sigp);
    switch (propno) {
    case SPECTRA_SAMPLED_SIGNAL_PROP_VECS:
        g_value_set_boxed(valp, priv->vecs);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, spec);
    }
}

static void spectra_sampled_signal_set_prop(GObject *gobj, guint propno,
                                            GValue const *valp, GParamSpec *spec) {
    SpectraSampledSignal *sigp = SPECTRA_SAMPLED_SIGNAL(gobj);
    SpectraSampledSignalPrivate *priv =
        spectra_sampled_signal_get_instance_private(sigp);
    switch (propno) {
    case SPECTRA_SAMPLED_SIGNAL_PROP_VECS:
        priv->vecs = g_value_dup_boxed(valp);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, spec);
    }
}

static void
spectra_sampled_signal_class_init(SpectraSampledSignalClass *smp_sig_class) {
    GObjectClass *gobj_class = G_OBJECT_CLASS(smp_sig_class);
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(smp_sig_class);

    gobj_class->finalize = spectra_sampled_signal_finalize;
    gobj_class->set_property = spectra_sampled_signal_set_prop;
    gobj_class->get_property = spectra_sampled_signal_get_prop;

    sig_class->compile_node = spectra_sampled_signal_compile;

    GParamSpec *vecs_spec =
        g_param_spec_boxed("vecs", "sample data",
                           "samples that represent the given discrete tmie signal.  "
                           "each sample is real or complex.",
                           G_TYPE_PTR_ARRAY,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);
    g_object_class_install_property(gobj_class,
                                    SPECTRA_SAMPLED_SIGNAL_PROP_VECS,
                                    vecs_spec);
}

/*******************************************************************************
 **
 ** SpectraFileSignal - subclass of SpectraSampledSignal for signals loaded
 ** from files
 **
 ******************************************************************************/
struct _SpectraFileSignal {
    SpectraSampledSignal parent;
    gchar *path;
};

#define SPECTRA_TYPE_FILE_SIGNAL (spectra_file_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraFileSignal, spectra_file_signal, SPECTRA,
                     FILE_SIGNAL, SpectraSampledSignal)
G_DEFINE_FINAL_TYPE(SpectraFileSignal, spectra_file_signal,
                    SPECTRA_TYPE_SAMPLED_SIGNAL)

static void spectra_file_signal_init(SpectraFileSignal *sigp) {
    sigp->path = NULL;
}

static GString *spectra_file_signal_to_str(SpectraSignal *sigp) {
    g_return_val_if_fail(spectra_signal_prereq_count(sigp) == 1, NULL);
    SpectraFileSignal *file_sig = SPECTRA_FILE_SIGNAL(sigp);
    GString *prereq_str = spectra_signal_to_string(spectra_signal_prereq(sigp, 0));
    GString *str = g_string_new(NULL);
    g_string_printf(str, "sample(\"%s\", %s)", file_sig->path, prereq_str->str);
    g_string_free(prereq_str, TRUE);
    return str;
}

static void spectra_file_signal_finalize(GObject *gobj) {
    g_free(SPECTRA_FILE_SIGNAL(gobj)->path);
    G_OBJECT_CLASS(spectra_file_signal_parent_class)->finalize(gobj);
}

static void spectra_file_signal_class_init(SpectraFileSignalClass *class) {
    SPECTRA_SIGNAL_CLASS(class)->to_str = spectra_file_signal_to_str;
    G_OBJECT_CLASS(class)->finalize = spectra_file_signal_finalize;
}

/*******************************************************************************
 **
 ** SpectraResampledSignal - subclass of SpectraSampledSignal for signals
 **                          sampled from other signals.
 **
 ******************************************************************************/

#define SPECTRA_TYPE_RESAMPLED_SIGNAL (spectra_resampled_signal_get_type())

struct _SpectraResampledSignal {
    SpectraSampledSignal parent;

    double start, stop;
    gint n_samples;

    /*
     * only kept around for the sake of to_str.  not included in prerequisites
     * because if we did then it would get compiled and its input variable would
     * get added to the source.
     */
    SpectraSignal *source;
};

G_DECLARE_FINAL_TYPE(SpectraResampledSignal, spectra_resampled_signal,
                     SPECTRA, RESAMPLED_SIGNAL, SpectraSampledSignal)
G_DEFINE_FINAL_TYPE(SpectraResampledSignal, spectra_resampled_signal,
                    SPECTRA_TYPE_SAMPLED_SIGNAL)

static GString *spectra_resampled_signal_to_str(SpectraSignal *sigp) {
    g_return_val_if_fail(spectra_signal_prereq_count(sigp) == 1, NULL);
    SpectraResampledSignal *resamp = SPECTRA_RESAMPLED_SIGNAL(sigp);
    GString *sampled_str = spectra_signal_to_string(resamp->source);
    GString *time_index_str = spectra_signal_to_string(spectra_signal_prereq(sigp, 0));
    GString *str = g_string_new(NULL);
    g_string_printf(str, "resample(%s, %s, %f, %f, %d)",
                    sampled_str->str, time_index_str->str,
                    resamp->start, resamp->stop, resamp->n_samples);
    g_string_free(sampled_str, TRUE);
    g_string_free(time_index_str, TRUE);
    return str;
}

static void spectra_resampled_signal_finalize(GObject *gobj) {
    SpectraResampledSignal *sigp = SPECTRA_RESAMPLED_SIGNAL(gobj);
    g_object_unref(sigp->source);
    G_OBJECT_CLASS(spectra_resampled_signal_parent_class)->finalize(gobj);
}

static void spectra_resampled_signal_init(SpectraResampledSignal *sigp) {
}

static void spectra_resampled_signal_class_init(SpectraResampledSignalClass *class) {
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(class);
    sig_class->to_str = spectra_resampled_signal_to_str;
    G_OBJECT_CLASS(sig_class)->finalize = spectra_resampled_signal_finalize;
}

/*******************************************************************************
 **
 ** function constructors
 **
 ******************************************************************************/

typedef SpectraSignal*(*spectra_signal_constructor)(SpectraConstantBuffer *cbuf,
                                                    gchar const *ident,
                                                    GPtrArray *prereq);

static SpectraSignal* spectra_invoke_ctor(SpectraConstantBuffer *cbuf,
                                          gchar const *ident,
                                          GPtrArray *prereq) {
    return spectra_invoke_signal_new_full(cbuf, ident, prereq);
}

static SpectraSignal *spectra_sample_ctor(SpectraConstantBuffer *cbuf,
                                          gchar const *ident,
                                          GPtrArray *prereq) {
    if (prereq->len == 2) {
        SpectraSignal *input_sig =
            SPECTRA_SIGNAL(g_ptr_array_index(prereq, 0));
        if (SPECTRA_IS_STRING_SIGNAL(input_sig)) {
            gchar *path =
                g_string_free_and_steal(spectra_signal_summary(input_sig));

            GBytes *data = NULL;
            double sample_frequency = NAN;

            {
                /* sorta hackish.  instantiate a temporary instance of
                   SpectraFileCache, load an entire file and then destroy the
                   FileCache (thus defeating the entire purpose of having a cache).
                */
                SpectraFileCache *cache = spectra_file_cache_new();
                GError *err = NULL;
                SpectraFile *file = spectra_file_cache_open(cache, path, &err);
                if (!file)
                    errx(1, "%s: %s", g_quark_to_string(err->domain), err->message);
                data = g_bytes_ref(spectra_file_get_channel(file, 0));
                sample_frequency = spectra_file_sample_frequency(file);

                g_object_unref(cache);
            }

            GPtrArray *prereq_actual = g_ptr_array_new_with_free_func(g_object_unref);
            g_ptr_array_add(prereq_actual, g_object_ref(g_ptr_array_index(prereq, 1)));

            gsize data_len;
            gconstpointer data_ptr = g_bytes_get_data(data, &data_len);;
            data_len /= sizeof(spectra_scalar);
            GPtrArray *vecs = g_ptr_array_new_full(data_len, g_object_unref);

            guint sample_index;
            for (sample_index = 0; sample_index < data_len; sample_index++) {
                spectra_scalar scalar;
                memcpy(&scalar, (spectra_scalar const*)data_ptr + sample_index,
                       sizeof(scalar));
                g_ptr_array_add(vecs, spectra_real_new(scalar));
            }

            SpectraFileSignal *sigp =
                g_object_new(SPECTRA_TYPE_FILE_SIGNAL,
                             "cbuf", cbuf,
                             "sampleperiod", 1.0 / sample_frequency,
                             "prereq", prereq_actual,
                             "vecs", vecs,
                             NULL);
            g_ptr_array_unref(prereq_actual);
            g_bytes_unref(data);
            g_ptr_array_unref(vecs);
            sigp->path = path;

            return SPECTRA_SIGNAL(sigp);
        } else {
            errx(1, "unimplemented");
        }
    } else {
        errx(1, "invalid parameters for %s", G_STRFUNC);
    }
    g_warn_if_reached(); // should be impossible
}

/*******************************************************************************
 **
 ** resample signal - takes as input one signal, returns a sampled version of
 ** that signal
 **
 ** prereqs:
 **     the signal to re-sample
 **     time to sample from
 **     start-time        (must be literal)
 **     end-time          (must be literal)
 **     number of samples (must be literal)
 **
 ******************************************************************************/
static SpectraSignal *spectra_resample_ctor(SpectraConstantBuffer *cbuf,
                                            gchar const *ident,
                                            GPtrArray *prereq) {
    SpectraSignal *input_sig, *start_time_sig, *end_time_sig, *n_samples_sig;
    if (prereq->len == 5 &&
        SPECTRA_IS_LITERAL_SIGNAL(start_time_sig = g_ptr_array_index(prereq, 2)) &&
        SPECTRA_IS_LITERAL_SIGNAL(end_time_sig = g_ptr_array_index(prereq, 3)) &&
        SPECTRA_IS_LITERAL_SIGNAL(n_samples_sig = g_ptr_array_index(prereq, 4))) {

        SpectraVector *start_time_vec = spectra_literal_signal_value(start_time_sig);
        SpectraVector *end_time_vec = spectra_literal_signal_value(end_time_sig);
        SpectraVector *n_samples_vec = spectra_literal_signal_value(n_samples_sig);

        SpectraVector *start_time_real = spectra_vector_as_real(start_time_vec);
        SpectraVector *end_time_real = spectra_vector_as_real(end_time_vec);
        SpectraVector *n_samples_real = spectra_vector_as_real(n_samples_vec);

        if (!(start_time_real && end_time_real && n_samples_real)) {
            if (start_time_real)
                g_object_unref(start_time_real);
            if (end_time_real)
                g_object_unref(end_time_real);
            if (n_samples_real)
                g_object_unref(n_samples_real);
            errx(1, "expected real number arguments for start_time, "
                 "end_time and n_samples!");
        }

        double start_time = spectra_vector_index(start_time_real, 0);
        double end_time = spectra_vector_index(end_time_real, 0);
        int n_samples = spectra_vector_index(n_samples_real, 0);

        if (n_samples <= 0 || start_time < 0.0 || end_time < 0.0 || start_time >= end_time) {
            if (start_time_real)
                g_object_unref(start_time_real);
            if (end_time_real)
                g_object_unref(end_time_real);
            if (n_samples_real)
                g_object_unref(n_samples_real);
            errx(1, "invalid");
        }

        spectra_scalar sample_period = (end_time - start_time) / n_samples;
        GPtrArray *vecs = g_ptr_array_new_full(n_samples, g_object_unref);

        input_sig = g_ptr_array_index(prereq, 0);
        {
            SpectraProgram *prog = spectra_signal_compile(input_sig);
            SpectraProgramContext *ctxt = spectra_program_context_new(prog);

            if (spectra_program_input_count(prog) > 1) {
                if (start_time_real)
                    g_object_unref(start_time_real);
                if (end_time_real)
                    g_object_unref(end_time_real);
                if (n_samples_real)
                    g_object_unref(n_samples_real);
                errx(1, "input signal has too many unresolved "
                     "input variables!");
            }

            GPtrArray *inputs_arr = g_ptr_array_new_full(1, g_object_unref);
            g_ptr_array_add(inputs_arr, spectra_real_new(start_time));

            int sample_index;
            for (sample_index = 0; sample_index < n_samples; sample_index++) {
                GError *err = NULL;
                SpectraVector *old_input = g_ptr_array_index(inputs_arr, 0);
                g_ptr_array_index(inputs_arr, 0) =
                    spectra_real_new(start_time + sample_period * sample_index);
                g_object_unref(old_input);
                SpectraVector *vecp = spectra_program_execute(prog, ctxt,
                                                              inputs_arr, &err);
                if (err)
                    errx(1, "failure to execute");
                else
                    g_ptr_array_add(vecs, vecp);
            }
            g_ptr_array_unref(inputs_arr);
            g_object_unref(ctxt);
            g_object_unref(prog);
        }

        /***********************************************************************
         **
         ** The only prereq that actually matters is the one that tells us when
         ** to sample from (second parameter to resample().  The other one,
         ** which is the signal that we are sampling from, doesn't really
         ** matter anymore because we already have all the samples we need.  We
         ** only keep it around for the sake of the to_str implementation.
         **
         **********************************************************************/

        GPtrArray *prereq_actual = g_ptr_array_new_with_free_func(g_object_unref);
        g_ptr_array_add(prereq_actual, g_object_ref(g_ptr_array_index(prereq, 1)));
        SpectraResampledSignal *sigp = g_object_new(SPECTRA_TYPE_RESAMPLED_SIGNAL,
                                                    "cbuf", cbuf,
                                                    "sampleperiod", sample_period,
                                                    "prereq", prereq_actual,
                                                    "vecs", vecs, NULL);
        g_ptr_array_unref(prereq_actual);
        g_ptr_array_unref(vecs);

        sigp->start = start_time;
        sigp->stop = end_time;
        sigp->n_samples = n_samples;
        sigp->source = g_object_ref(g_ptr_array_index(prereq, 0));


        g_object_unref(start_time_real);
        g_object_unref(end_time_real);
        g_object_unref(n_samples_real);

        return SPECTRA_SIGNAL(sigp);
    } else {
        errx(1, "usage: resample(<signal>, <current time to sample from>, "
             "<start-time> (must be literal), "
             "<end-time> (must be literal), <sample-count> (must be literal)");
    }
}


/*******************************************************************************
 **
 ** bind signal - takes as input a string representing a variable name, a signal
 **               which should fold down into a constant value or symbol, and a signal
 **               that is to be bound.
 **
 **               the purpose is to construct a new signal which has the input
 **               variable mapped to a constant value.
 **
 **               For now you can only bind one variable at a time but that is
 **               going to change eventually.
 **
 **               A BOUND SIGNAL IS NOT THE SAME THING AS A FUNCTION
 **               CALL!!!!!!!!!!
 **               a function calls identifies its parameters positionally.  A
 **               bound signal identifies its parameters by name.
 **
 **               for example, if t is an input variable into the outer (root)
 **               signal, then we might use a binding such as this:
 **                       bind("freq", 400, sin(2 * pi * freq * t))
 **               to generatee a 400Hz tone by binding the value "400" to
 **               "freq".  the bind function knows to replace "freq" with 400
 **               because we explicitly told it to; it has nothing to do with
 **               freq's position or relationship to other terms in the wrapped
 **               signal.  So, in a manner of speaking, bind is like a way to
 **               declare temporary local variables.
 **
 ** LINGO:
 **     "wrapped signal": another signal which this signal passes data to,
 **                       usually (but not necessarily) with one of the
 **                       arguments being replaced by a literal value.
 **     "inner signal":   see "wrapped signal".
 **     "bound symbol":   the name of the input variable which is being bound to
 **                       a literal value
 **     "bound value":    the value that the bound_symbol will be set to.  As of
 **                       this writing it must be a literal value, but in
 **                       general it should be possible to pass something that
 **                       can get folded into a literal value, or the output of
 **                       any arbitrarily complicated signal
 **     "wrapped inputs": the input arguments of the wrapped signal, in contrast to
 **                       the input arguments of the SpectraBindSignal itself.
 **
 ** example: bind("a", 3, a * t * t) will be equivalent to 3 * t * t.
 **
 ******************************************************************************/

struct _SpectraBindSignal {
    SpectraSignal parent;

    /***************************************************************************
     **
     ** the wrapped signal isn't actually a prerequisite because the wrapped
     ** signal represents what this node in the tree is doing.  if we made it a
     ** prerequisite then it would get automatically compiled into programs
     ** before the SpectraBindSignal is compiled.
     **
     **************************************************************************/
    SpectraSignal *wrapped;
};

enum spectra_bind_signal_prop_id {
    SPECTRA_BIND_SIGNAL_PROP_ID_WRAPPED = 1
};

#define SPECTRA_TYPE_BIND_SIGNAL (spectra_bind_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraBindSignal, spectra_bind_signal,
                     SPECTRA, BIND_SIGNAL, SpectraSignal)
G_DEFINE_FINAL_TYPE(SpectraBindSignal, spectra_bind_signal, SPECTRA_TYPE_SIGNAL)

static gchar *spectra_bind_signal_get_bound_symbol(SpectraBindSignal *sigp);

/*******************************************************************************
 **
 ** TODO: bind signals should work the way function calls work in C, just push
 **       all the arguments onto the inputs stack.
 **
 ******************************************************************************/
static guint
spectra_bind_signal_compile(SpectraSignal *sigp,
                            SpectraProgram *prog, GArray *prereq_outputs) {
    SpectraBindSignal *bindsig = SPECTRA_BIND_SIGNAL(sigp);
    g_return_val_if_fail(bindsig->wrapped, PROGRAM_ADDR_INVALID);

    // step 1 - extract name of the bound symbol
    gchar *bound_symbol = spectra_bind_signal_get_bound_symbol(bindsig);
    g_return_val_if_fail(bound_symbol, PROGRAM_ADDR_INVALID);

    // step 2 - extract location of the bound symbol's value
    guint bound_value_addr = g_array_index(prereq_outputs, guint, 1);
    if (bound_value_addr == PROGRAM_ADDR_INVALID)
        errx(1, "input location is PROGRAM_ADDR_INVALID");

    // step 3 - remap inputs and compile the wrapped signal (ie the hard part)
    GArray *remapped_inputs = g_array_new(FALSE, FALSE, sizeof(guint));
    GPtrArray *wrapped_inputs = spectra_signal_inputs(bindsig->wrapped);
    GPtrArray *input_names = g_ptr_array_new_with_free_func(g_free);

    // for each input in the wrapped signal
    guint wrapped_input_index;
    for (wrapped_input_index = 0;
         wrapped_input_index < wrapped_inputs->len;
         wrapped_input_index++) {
        gchar *sym_name = g_ptr_array_index(wrapped_inputs,
                                            wrapped_input_index);
        if (strcmp(sym_name, bound_symbol) == 0) {
            // symbol matches the name of our bound symbol
            g_array_append_val(remapped_inputs, bound_value_addr);
        } else {
            // look through the current node's inputs for a matching symbol
            guint input_loc = spectra_program_load_input(prog, sym_name);
            if (input_loc == PROGRAM_ADDR_INVALID)
                errx(1, "%s failed to load input \"%s\"", G_STRFUNC, sym_name);
            g_array_append_val(remapped_inputs, input_loc);
        }
        g_ptr_array_add(input_names, g_strdup(sym_name));
    }

    g_ptr_array_unref(wrapped_inputs);
    wrapped_inputs = NULL;

    // now go through all the inputs in reverse and push them into the inputs
    // array
    for (wrapped_input_index = remapped_inputs->len-1;
         wrapped_input_index != PROGRAM_ADDR_INVALID; wrapped_input_index--) {
        gchar *name = g_ptr_array_index(input_names, wrapped_input_index);
        spectra_program_push_input(prog, name,
                                   g_array_index(remapped_inputs, guint,
                                                 wrapped_input_index));
    }

    g_ptr_array_unref(input_names);
    input_names = NULL;

    // compile wrapped signal
    guint output_index = spectra_signal_compile_subtree(bindsig->wrapped, prog);

    // now pop all the inputs
    for (wrapped_input_index = 0;
         wrapped_input_index < remapped_inputs->len;
         wrapped_input_index++) {
        spectra_program_pop_input(prog);
    }

    g_array_unref(remapped_inputs);
    g_free(bound_symbol);

    // return the address returned by the wrapped signal
    return output_index;
}

static GString *spectra_bind_signal_to_string(SpectraSignal *sigp) {
    SpectraBindSignal *bindsig = SPECTRA_BIND_SIGNAL(sigp);
    g_return_val_if_fail(bindsig->wrapped, NULL);
    g_return_val_if_fail(spectra_signal_prereq_count(sigp) == 2, NULL);

    GString *wrapped = spectra_signal_to_string(bindsig->wrapped);
    GString *sym = spectra_signal_to_string(spectra_signal_prereq(sigp, 0));
    GString *val = spectra_signal_to_string(spectra_signal_prereq(sigp, 1));

    GString *gstr = g_string_new(NULL);
    g_string_printf(gstr, "bind(%s, %s, %s)",
                    sym->str, val->str, wrapped->str);

    g_string_free(wrapped, TRUE);
    g_string_free(sym, TRUE);
    g_string_free(val, TRUE);

    return gstr;
}

/*******************************************************************************
 **
 ** returns a NULL-terminated string representing the name of the variable which
 ** is bound (must be g_free'd by the caller)
 **
 ******************************************************************************/
static gchar *spectra_bind_signal_get_bound_symbol(SpectraBindSignal *sigp) {
    g_return_val_if_fail(spectra_signal_prereq_count(SPECTRA_SIGNAL(sigp)) == 2,
                         NULL);
    SpectraSignal *strsig = spectra_signal_prereq(SPECTRA_SIGNAL(sigp), 0);
    g_return_val_if_fail(SPECTRA_IS_STRING_SIGNAL(strsig), NULL);
    return g_string_free_and_steal(spectra_signal_summary(strsig));
}

void spectra_bind_signal_constructed(GObject *gobj) {
    // chain up to parent class
    G_OBJECT_CLASS(spectra_bind_signal_parent_class)->constructed(gobj);

    SpectraBindSignal *sigp = SPECTRA_BIND_SIGNAL(gobj);

    guint n_arg = spectra_signal_prereq_count(SPECTRA_SIGNAL(gobj));
    if (n_arg != 2)
        errx(1, "there must be 2 arguments, not %u arguments!", n_arg);
}

static void spectra_bind_signal_set_prop(GObject *gobj, guint propno,
                                         GValue const *valp, GParamSpec *spec) {
    SpectraBindSignal *sigp = SPECTRA_BIND_SIGNAL(gobj);
    switch (propno) {
    case SPECTRA_BIND_SIGNAL_PROP_ID_WRAPPED:
        sigp->wrapped = g_object_ref(g_value_get_object(valp));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, spec);
    }
}

static void spectra_bind_signal_get_prop(GObject *gobj, guint propno,
                                         GValue *valp, GParamSpec *spec) {
    SpectraBindSignal *sigp = SPECTRA_BIND_SIGNAL(gobj);
    switch (propno) {
    case SPECTRA_BIND_SIGNAL_PROP_ID_WRAPPED:
        g_value_set_object(valp, sigp->wrapped);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, spec);
    }
}

static void spectra_bind_signal_init(SpectraBindSignal *sigp) {
}

static void spectra_bind_signal_accumulate_inputs(SpectraSignal *sigp, GPtrArray *inputs) {
    SpectraBindSignal *bindsig = SPECTRA_BIND_SIGNAL(sigp);
    GString *bound = spectra_signal_summary(spectra_signal_prereq(sigp, 0));

    // chain-up to default implementation
    SPECTRA_SIGNAL_CLASS(spectra_bind_signal_parent_class)->accumulate_inputs(sigp, inputs);

    GPtrArray *fuckit = g_ptr_array_new_with_free_func(g_free);
    spectra_signal_accumulate_inputs(bindsig->wrapped, fuckit);
    guint input_index;
    for (input_index = 0; input_index < fuckit->len; input_index++) {
        gchar *cunt = g_ptr_array_index(fuckit, input_index);
        if (strcmp(cunt, bound->str) != 0)
            spectra_signal_add_input_to_set(inputs, cunt);
    }
    g_ptr_array_unref(fuckit);
    g_string_free(bound, TRUE);
}

static void spectra_bind_signal_finalize(GObject *gobj) {
    g_object_unref(SPECTRA_BIND_SIGNAL(gobj)->wrapped);

    G_OBJECT_CLASS(spectra_bind_signal_parent_class)->finalize(gobj);
}

static void spectra_bind_signal_class_init(SpectraBindSignalClass *class) {
    GObjectClass *gobj_class = G_OBJECT_CLASS(class);
    gobj_class->constructed = spectra_bind_signal_constructed;
    gobj_class->finalize = spectra_bind_signal_finalize;
    gobj_class->get_property = spectra_bind_signal_get_prop;
    gobj_class->set_property = spectra_bind_signal_set_prop;

    SpectraSignalClass *sigclass = SPECTRA_SIGNAL_CLASS(class);
    sigclass->to_str = spectra_bind_signal_to_string;
    sigclass->compile_node = spectra_bind_signal_compile;
    sigclass->accumulate_inputs = spectra_bind_signal_accumulate_inputs;

    GParamSpec *wrapped_spec =
        g_param_spec_object("wrapped", "wrapped signal",
                            "signal which will evaluated with a given input "
                            "variable bound to a given value",
                            SPECTRA_TYPE_SIGNAL,
                            G_PARAM_READWRITE |
                            G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS);
    g_object_class_install_property(gobj_class,
                                    SPECTRA_BIND_SIGNAL_PROP_ID_WRAPPED,
                                    wrapped_spec);
}

static SpectraSignal *spectra_bind_ctor(SpectraConstantBuffer *cbuf,
                                        gchar const *ident,
                                        GPtrArray *prereq) {
    g_return_val_if_fail(prereq->len == 3, NULL);
    SpectraSignal *wrapped_signal = g_ptr_array_index(prereq, 2);
    GPtrArray *actual_prereqs = g_ptr_array_new_with_free_func(g_object_unref);

    g_ptr_array_add(actual_prereqs, g_object_ref(g_ptr_array_index(prereq, 0)));
    g_ptr_array_add(actual_prereqs, g_object_ref(g_ptr_array_index(prereq, 1)));

    SpectraSampledSignal *sigp =
        g_object_new(SPECTRA_TYPE_BIND_SIGNAL,
                     "cbuf", cbuf,
                     "sampleperiod", 1.0 / 44100.0,
                     "prereq", actual_prereqs,
                     "wrapped", wrapped_signal,
                     NULL);
    g_ptr_array_unref(actual_prereqs);
    return SPECTRA_SIGNAL(sigp);
}

/*******************************************************************************
 **
 ** exp - a natural exponential
 **
 ******************************************************************************/
#define SPECTRA_TYPE_EXP_SIGNAL (spectra_exp_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraExpSignal, spectra_exp_signal, SPECTRA, EXP_SIGNAL, SpectraSignal)

struct _SpectraExpSignal {
    SpectraSignal parent;
};

G_DEFINE_FINAL_TYPE(SpectraExpSignal, spectra_exp_signal, SPECTRA_TYPE_SIGNAL)

static SpectraSignal *spectra_exp_signal_new_full(SpectraConstantBuffer *cbuf, GPtrArray *prereq) {
    return SPECTRA_SIGNAL(g_object_new(SPECTRA_TYPE_EXP_SIGNAL,
                                       "prereq", prereq, "cbuf", cbuf, NULL));
}

static SpectraSignal *spectra_exp_signal_new(SpectraConstantBuffer *cbuf, SpectraSignal *sigp) {
    GPtrArray *prereq = g_ptr_array_new_full(1, g_object_unref);
    g_ptr_array_add(prereq, g_object_ref(sigp));

    SpectraSignal *retp = spectra_exp_signal_new_full(cbuf, prereq);

    g_ptr_array_unref(prereq);
    return retp;
}

static GString *spectra_exp_signal_to_str(SpectraSignal *sigp) {
    g_return_val_if_fail(spectra_signal_prereq_count(sigp) == 1, NULL);
    GString *ret = g_string_new("exp(");
    GString *inner = spectra_signal_to_string(spectra_signal_prereq(sigp, 0));
    g_string_append_printf(ret, "%s)", inner->str);
    g_string_free(inner, true);
    return ret;
}

static guint spectra_exp_signal_compile(SpectraSignal *sigp,
                                        SpectraProgram *prog,
                                        GArray *prereq_outputs) {
    g_return_val_if_fail(prereq_outputs->len == 1, PROGRAM_ADDR_INVALID);
    guint prereq_addr = g_array_index(prereq_outputs, guint, 0);
    return spectra_program_exp(prog, prereq_addr);
}


static void spectra_exp_signal_init(SpectraExpSignal *sigp) {
}

static void spectra_exp_signal_class_init(SpectraExpSignalClass *classp) {
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(classp);
    sig_class->to_str = spectra_exp_signal_to_str;
    sig_class->compile_node = spectra_exp_signal_compile;
}

static SpectraSignal *spectra_exp_ctor(SpectraConstantBuffer *cbuf,
                                        gchar const *ident,
                                        GPtrArray *prereq) {
    return spectra_exp_signal_new_full(cbuf, prereq);
}

/*******************************************************************************
 **
 ** natural logarithm
 **
 ******************************************************************************/
#define SPECTRA_TYPE_LN_SIGNAL (spectra_ln_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraLnSignal, spectra_ln_signal, SPECTRA, LN_SIGNAL, SpectraSignal)

struct _SpectraLnSignal {
    SpectraSignal parent;
};

G_DEFINE_FINAL_TYPE(SpectraLnSignal, spectra_ln_signal, SPECTRA_TYPE_SIGNAL)

static SpectraSignal *
spectra_ln_signal_new_full(SpectraConstantBuffer *cbuf, GPtrArray *prereq) {
    return SPECTRA_SIGNAL(g_object_new(SPECTRA_TYPE_LN_SIGNAL,
                                       "prereq", prereq, "cbuf", cbuf, NULL));
}

static SpectraSignal *
spectra_ln_signal_new(SpectraConstantBuffer *cbuf, SpectraSignal *sigp) {
    GPtrArray *prereq = g_ptr_array_new_full(1, g_object_unref);
    g_ptr_array_add(prereq, g_object_ref(sigp));

    SpectraSignal *retp = spectra_ln_signal_new_full(cbuf, prereq);

    g_ptr_array_unref(prereq);
    return retp;
}

static GString *spectra_ln_signal_to_str(SpectraSignal *sigp) {
    g_return_val_if_fail(spectra_signal_prereq_count(sigp) == 1, NULL);
    GString *ret = g_string_new("ln(");
    GString *inner = spectra_signal_to_string(spectra_signal_prereq(sigp, 0));
    g_string_append_printf(ret, "%s)", inner->str);
    g_string_free(inner, true);
    return ret;
}

static guint spectra_ln_signal_compile(SpectraSignal *sigp,
                                       SpectraProgram *prog,
                                       GArray *prereq_outputs) {
    g_return_val_if_fail(prereq_outputs->len == 1, PROGRAM_ADDR_INVALID);
    guint prereq_addr = g_array_index(prereq_outputs, guint, 0);
    return spectra_program_ln(prog, prereq_addr);
}

static void spectra_ln_signal_init(SpectraLnSignal *sigp) {
}

static void spectra_ln_signal_class_init(SpectraLnSignalClass *classp) {
    SpectraSignalClass *sig_class = SPECTRA_SIGNAL_CLASS(classp);
    sig_class->to_str = spectra_ln_signal_to_str;
    sig_class->compile_node = spectra_ln_signal_compile;
}

static SpectraSignal *spectra_ln_ctor(SpectraConstantBuffer *cbuf,
                                      gchar const *ident,
                                      GPtrArray *prereq) {
    return spectra_ln_signal_new_full(cbuf, prereq);
}

/*******************************************************************************
 **
 ** sin/cos functions - implemented on top of exp using euler's formula
 **
 ******************************************************************************/

static SpectraSignal *spectra_sin_ctor(SpectraConstantBuffer *cbuf,
                                       gchar const *ident,
                                       GPtrArray *prereq) {
    g_return_val_if_fail(prereq->len == 1, NULL);
    SpectraSignal *imag_unit_lit, *neg_imag_unit_lit, *half_and_phase_shift_lit, *neg_one_lit;
    {
        SpectraVector *imag_unit = spectra_complex_new(0.0, 1.0);
        SpectraVector *neg_imag_unit = spectra_complex_new(0.0, -1.0);
        SpectraVector *half_and_phase_shift = spectra_complex_new(0.0, -0.5);
        SpectraVector *neg_one = spectra_real_new(-1.0);
        imag_unit_lit = spectra_literal_signal_new(cbuf, imag_unit);
        neg_imag_unit_lit = spectra_literal_signal_new(cbuf, neg_imag_unit);
        half_and_phase_shift_lit =
            spectra_literal_signal_new(cbuf, half_and_phase_shift);
        neg_one_lit = spectra_literal_signal_new(cbuf, neg_one);

        g_object_unref(neg_one);
        g_object_unref(half_and_phase_shift);
        g_object_unref(imag_unit);
        g_object_unref(neg_imag_unit);
    }
    SpectraSignal *mult_sigs[2] = {
        spectra_mult_signal_new(cbuf, 2, imag_unit_lit, g_ptr_array_index(prereq, 0)),
        spectra_mult_signal_new(cbuf, 2, neg_imag_unit_lit, g_ptr_array_index(prereq, 0))
    };
    SpectraSignal *exp_sigs[2] = {
        spectra_exp_signal_new(cbuf, mult_sigs[0]),
        spectra_exp_signal_new(cbuf, mult_sigs[1])
    };
    SpectraSignal *neg_sig = spectra_mult_signal_new(cbuf, 2, neg_one_lit, exp_sigs[1]);
    SpectraSignal *sum_sig = spectra_sum_signal_new(cbuf, 2, exp_sigs[0], neg_sig);
    SpectraSignal *sin_sig = spectra_mult_signal_new(cbuf, 2, half_and_phase_shift_lit, sum_sig);

    g_object_unref(sum_sig);
    g_object_unref(neg_sig);
    g_object_unref(exp_sigs[0]);
    g_object_unref(exp_sigs[1]);
    g_object_unref(mult_sigs[0]);
    g_object_unref(mult_sigs[1]);
    g_object_unref(neg_one_lit);
    g_object_unref(half_and_phase_shift_lit);
    g_object_unref(neg_imag_unit_lit);
    g_object_unref(imag_unit_lit);

    return sin_sig;
}


static SpectraSignal *spectra_cos_ctor(SpectraConstantBuffer *cbuf,
                                       gchar const *ident,
                                       GPtrArray *prereq) {
    g_return_val_if_fail(prereq->len == 1, NULL);
    SpectraSignal *imag_unit_lit, *neg_imag_unit_lit, *half_lit;
    {
        SpectraVector *imag_unit = spectra_complex_new(0.0, 1.0);
        SpectraVector *neg_imag_unit = spectra_complex_new(0.0, -1.0);
        SpectraVector *half = spectra_real_new(0.5);
        imag_unit_lit = spectra_literal_signal_new(cbuf, imag_unit);
        neg_imag_unit_lit = spectra_literal_signal_new(cbuf, neg_imag_unit);
        half_lit = spectra_literal_signal_new(cbuf, half);

        g_object_unref(half);
        g_object_unref(imag_unit);
        g_object_unref(neg_imag_unit);
    }
    SpectraSignal *mult_sigs[2] = {
        spectra_mult_signal_new(cbuf, 2, imag_unit_lit, g_ptr_array_index(prereq, 0)),
        spectra_mult_signal_new(cbuf, 2, neg_imag_unit_lit, g_ptr_array_index(prereq, 0))
    };
    SpectraSignal *exp_sigs[2] = {
        spectra_exp_signal_new(cbuf, mult_sigs[0]),
        spectra_exp_signal_new(cbuf, mult_sigs[1])
    };
    SpectraSignal *sum_sig = spectra_sum_signal_new(cbuf, 2, exp_sigs[0], exp_sigs[1]);
    SpectraSignal *cos_sig = spectra_mult_signal_new(cbuf, 2, half_lit, sum_sig);

    g_object_unref(sum_sig);
    g_object_unref(exp_sigs[0]);
    g_object_unref(exp_sigs[1]);
    g_object_unref(mult_sigs[0]);
    g_object_unref(mult_sigs[1]);
    g_object_unref(half_lit);
    g_object_unref(neg_imag_unit_lit);
    g_object_unref(imag_unit_lit);

    return cos_sig;
}

static struct spectra_function_constructor {
    gchar const *name;
    spectra_signal_constructor constructor;
} spectra_func_tbl[] = {
    { "sin", spectra_sin_ctor },
    { "cos", spectra_cos_ctor },
    { "exp", spectra_exp_ctor },
    { "ln", spectra_ln_ctor },
    { "floor", spectra_invoke_ctor },
    { "ceil", spectra_invoke_ctor },
    { "sqrt", spectra_invoke_ctor },
    { "mag", spectra_invoke_ctor },
    { "print", spectra_invoke_ctor },
    { "sample", spectra_sample_ctor },
    { "resample", spectra_resample_ctor },
    { "dft", spectra_dft_ctor },
    { "inv_dft", spectra_dft_ctor },
    { "bind", spectra_bind_ctor },

    { NULL }
};

SpectraSignal *spectra_function_signal_new_full(SpectraConstantBuffer *cbuf,
                                                gchar const *ident,
                                                GPtrArray *prereq) {
    struct spectra_function_constructor *curs;
    for (curs = spectra_func_tbl; curs->name; curs++) {
        if (strcmp(curs->name, ident) == 0)
            return curs->constructor(cbuf, ident, prereq);
    }

    g_error("%s - function \"%s\" not defined\n", G_STRFUNC, ident);
    abort(); // g_error should never return but whatevs
}
