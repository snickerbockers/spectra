/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <math.h>
#include <stdarg.h>
#include <err.h>

#include <glib.h>
#include <glib-object.h>

#include "vector.h"

/*******************************************************************************
 **
 ** SPECTRAVECTOR
 **
 ** THE BASE CLASS OF ALL VECTOR TYPES
 **
 ** vector types are defined at run-time for a given set of axes;
 ** these axes are stored in SpectraVectorClass.  This is so that all
 ** vectors of a given class will have the same set of axes.
 **
 ** derived vectors do not have any additional data on top of that
 ** which is already contained in SpectraVector, and neither do
 ** derived dector classes.
 **
 ******************************************************************************/
typedef struct {
    GArray *data;
} SpectraVectorPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(SpectraVector, spectra_vector, G_TYPE_OBJECT);

guint spectra_vector_len(SpectraVector const *vec) {
    return SPECTRA_VECTOR_CLASS(G_OBJECT_GET_CLASS(vec))->axes->len;
}

gchar const *spectra_vector_axis(SpectraVector const *vec, guint index) {
    SpectraVectorClass *class = SPECTRA_VECTOR_CLASS(G_OBJECT_GET_CLASS(vec));
    g_return_val_if_fail(index < class->axes->len, NULL);
    return g_ptr_array_index(class->axes, index);
}

static void spectra_vector_set_prop(GObject *gobj, guint propno,
                                    GValue const *valp,
                                    GParamSpec *param) {
    SpectraVector *vec = SPECTRA_VECTOR(gobj);
    SpectraVectorPrivate *priv = spectra_vector_get_instance_private(vec);
    switch (propno) {
    case SPECTRA_VECTOR_DATA:
        {
            if (priv->data)
                g_array_unref(priv->data);
            priv->data = g_array_copy(g_value_get_boxed(valp));
            g_return_if_fail(priv->data->len == spectra_vector_len(vec));
        }
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, param);
    }
}

static void spectra_vector_get_prop(GObject *gobj, guint propno,
                                    GValue *valp, GParamSpec *param) {
    SpectraVector *spec = SPECTRA_VECTOR(gobj);
    SpectraVectorPrivate *priv = spectra_vector_get_instance_private(spec);
    switch (propno) {
    case SPECTRA_VECTOR_DATA:
        /*
         * TODO: constantly making copies is suboptimal
         *       but it does make things more error-proof by
         *       guaranteeing the immutability of vectors.
         */
        g_value_take_boxed(valp, g_array_copy(priv->data));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(gobj, propno, param);
    }
}

static GString *spectra_vector_to_str_default(SpectraVector *vec) {
    SpectraVectorPrivate *priv = spectra_vector_get_instance_private(vec);
    gchar const *pre = " ";
    GString *ret = g_string_new("(");
    guint len = priv->data->len;
    guint index;
    for (index = 0; index < len; index++) {
        g_string_append_printf(ret, "%s%f * %s",
                               pre,
                               g_array_index(priv->data, spectra_scalar, index),
                               spectra_vector_axis(vec, index));
        pre = ", ";
    }
    g_string_append_printf(ret, " )");
    return ret;
}

static SpectraVector* spectra_vector_add_default(SpectraVector *lhs, SpectraVector *rhs) {
    SpectraVectorClass *lhs_class = SPECTRA_VECTOR_GET_CLASS(lhs);
    SpectraVectorClass *rhs_class = SPECTRA_VECTOR_GET_CLASS(rhs);
    if (lhs_class == rhs_class) {
        guint index, n_elem = lhs_class->axes->len;
        GArray *sums = g_array_sized_new(FALSE, FALSE, sizeof(spectra_scalar), n_elem);
        SpectraVectorPrivate *lhs_priv = spectra_vector_get_instance_private(lhs);
        SpectraVectorPrivate *rhs_priv = spectra_vector_get_instance_private(rhs);
        GArray *lhs_data = lhs_priv->data;
        GArray *rhs_data = rhs_priv->data;
        for (index = 0; index < n_elem;index++) {
            spectra_scalar sum = g_array_index(lhs_data, spectra_scalar, index) +
                g_array_index(rhs_data, spectra_scalar, index);
            g_array_append_val(sums, sum);
        }
        SpectraVector *ret = spectra_vector_new_full(G_OBJECT_CLASS_TYPE(lhs_class), sums);
        g_array_unref(sums);
        return ret;
    } else {
        // this addition function can only handle cases when both vectors are
        // of the same type
        return NULL;
    }
}

spectra_scalar spectra_vector_index(SpectraVector *vec, guint index) {
    SpectraVectorPrivate *priv = spectra_vector_get_instance_private(vec);
    GArray *data = priv->data;

    g_return_val_if_fail(index < data->len, NAN);
    return g_array_index(data, spectra_scalar, index);
}

static void spectra_vector_finalize(GObject *gobj) {
    SpectraVector *vec = SPECTRA_VECTOR(gobj);
    SpectraVectorPrivate *priv = spectra_vector_get_instance_private(vec);
    g_array_unref(priv->data);
}

static void spectra_vector_init(SpectraVector *vec) {
    SpectraVectorPrivate *priv = spectra_vector_get_instance_private(vec);
    priv->data = NULL;
}

static void spectra_vector_class_init(SpectraVectorClass *class) {
    GObjectClass *gobj_class = G_OBJECT_CLASS(class);

    gobj_class->set_property = spectra_vector_set_prop;
    gobj_class->get_property = spectra_vector_get_prop;
    gobj_class->finalize = spectra_vector_finalize;

    memset(&class->vtable, 0, sizeof(class->vtable));

    class->vtable.to_str = spectra_vector_to_str_default;
    class->vtable.add = spectra_vector_add_default;

    GParamSpec *data_spec =
        g_param_spec_boxed("data", "data",
                           "GArray of axis data.  This is expected to be the "
                           "same length as the axes array.",
                           G_TYPE_ARRAY,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property(gobj_class, SPECTRA_VECTOR_DATA, data_spec);
}

/*******************************************************************************
 **
 ** vector_class_params - central database of all existant vector
 ** types, indexed by name.
 **
 ** ideally new vector types would only be allocated for different
 ** sets of axes and we wouldn't care about the name; however this would
 ** make things more complicated than they need to be so for now we
 ** rely upon users not to get the names of different vector classes
 ** mixed up.
 **
 ******************************************************************************/
struct vector_class_data {
    gchar *name;
    GPtrArray *axes;
    GType tp;
    struct spectra_vector_vtable vtable;
};
// GDestroyNotify implementation for struct vector_class_data
static void free_vector_class_data_single(gpointer loc) {
    if (loc) {
        struct vector_class_data *datp = loc;
        g_free(datp->name);
        g_ptr_array_unref(datp->axes);
        g_free(datp);
    }
}

static gpointer copy_axis_single(gconstpointer src, gpointer) {
    return g_strdup(src);
}

static GPtrArray *vector_class_params;

/*
 * forward-declarations for the init and class_init implementations of
 * SpectraVectorDerived
 */
static void spectra_vector_derived_class_init(gpointer classp, gpointer datp);
static void spectra_vector_derived_init(GTypeInstance *instance, gpointer fuckit);
static void spectra_vector_derived_class_finalize(gpointer g_class,
                                                  gpointer class_data);

static gboolean vector_class_data_eq(gconstpointer cl_data_void, gconstpointer name) {
    struct vector_class_data const *cl_data = cl_data_void;
    return g_str_equal(cl_data->name, name);
}

GType spectra_vector_find_type_by_name(gchar const *name) {
    guint index;
    if (vector_class_params &&
        g_ptr_array_find_with_equal_func(vector_class_params, name,
                                         vector_class_data_eq,
                                         &index)) {
        struct vector_class_data *class_data =
            g_ptr_array_index(vector_class_params, index);
        return class_data->tp;
    } else {
        return G_TYPE_INVALID;
    }
}

GType spectra_vector_type_full(gchar const *name,
                               struct spectra_vector_vtable vtable,
                               GPtrArray *axes) {
    /***************************************************************************
     **
     ** if there's already a derived vector type with the given name,
     ** then return it instead of making a new one.  It falls upon the
     ** caller to not accidentally create two different sets of axes with
     ** the same name.
     **
     **************************************************************************/
    GType tp_exist = spectra_vector_find_type_by_name(name);
    if (tp_exist != G_TYPE_INVALID)
        return tp_exist;

    if (!vector_class_params) {
        vector_class_params =
            g_ptr_array_new_with_free_func(free_vector_class_data_single);
    }

    struct vector_class_data *class_data = g_malloc0(sizeof(struct vector_class_data));
    class_data->name = g_strdup(name);
    class_data->axes = g_ptr_array_copy(axes, copy_axis_single, NULL);
    class_data->vtable = vtable;

    GTypeInfo spec = {
        .class_size = sizeof(SpectraVectorClass),
        .class_init = spectra_vector_derived_class_init,
        //        .class_finalize = spectra_vector_derived_class_finalize,
        // TODO: make it dynamic, implement class_finalize
        .class_data = class_data,
        .instance_size = sizeof(SpectraVector),
        .instance_init = spectra_vector_derived_init
    };

    class_data->tp = g_type_register_static(SPECTRA_TYPE_VECTOR,
                                            name, &spec, G_TYPE_FLAG_FINAL);

    g_ptr_array_add(vector_class_params, class_data);

    g_print("new vector class type \"%s\": %p\n",
            name, (gconstpointer)class_data->tp);

    return class_data->tp;
}

SpectraVector *spectra_vector_new_full(GType type, GArray *data) {
    return g_object_new(type, "data", data, NULL);
}

guint spectra_vector_type_len(GType type) {
    guint index;
    for (index = 0; index < vector_class_params->len; index++) {
        struct vector_class_data const *param =
            g_ptr_array_index(vector_class_params, index);
        if (param->tp == type)
            return param->axes->len;
    }
    g_error("type not a vector type");
}

SpectraVector *spectra_vector_new(GType type, ...) {
    guint len = spectra_vector_type_len(type);
    guint index;

    GArray *params = g_array_sized_new(FALSE, FALSE,
                                       sizeof(spectra_scalar), len);

    va_list args;
    va_start(args, type);
    for (index = 0; index < len; index++) {
        spectra_scalar val = va_arg(args, spectra_scalar);
        g_array_append_val(params, val);
    }
    va_end(args);

    SpectraVector *vecp = spectra_vector_new_full(type, params);

    g_array_unref(params);

    return vecp;
}

GType spectra_vector_type(gchar const *name, struct spectra_vector_vtable vtable, ...) {
    va_list args;
    GPtrArray *axes = g_ptr_array_new_with_free_func(g_free);

    va_start(args, vtable);
    gchar const *axis_name;
    while ((axis_name = va_arg(args, gchar const*)))
        g_ptr_array_add(axes, g_strdup(axis_name));
    va_end(args);

    GType tp = spectra_vector_type_full(name, vtable, axes);
    g_ptr_array_unref(axes);
    return tp;
}

/*******************************************************************************
 **
 ** SPECTRAVECTORDERIVED
 **
 ** all derived vectors are effectively the same type even though in
 ** reality they all have separate GTypes.
 **
 ** it doesn't have any additional data on top of the base vector
 ** class because the only real purpose of SpectraVectorDerived is to
 ** have a way for all vectors of a given class to share a common set
 ** of axis-names for their orthogonal bases.
 **
 ******************************************************************************/
static void spectra_vector_derived_class_init(gpointer classp, gpointer datp) {
    struct vector_class_data *params = datp;
    SpectraVectorClass *class = SPECTRA_VECTOR_CLASS(classp);
    class->axes = g_ptr_array_ref(params->axes);
    class->name = g_strdup(params->name);

    if (params->vtable.to_str)
        class->vtable.to_str = params->vtable.to_str;
    if (params->vtable.add)
        class->vtable.add = params->vtable.add;
    if (params->vtable.sub)
        class->vtable.sub = params->vtable.sub;
    if (params->vtable.mult)
        class->vtable.mult = params->vtable.mult;
    if (params->vtable.div)
        class->vtable.div = params->vtable.div;
    if (params->vtable.scale)
        class->vtable.scale = params->vtable.scale;
    if (params->vtable.as_real)
        class->vtable.as_real = params->vtable.as_real;
    if (params->vtable.exp)
        class->vtable.exp = params->vtable.exp;
    if (params->vtable.ln)
        class->vtable.ln = params->vtable.ln;
}

static void spectra_vector_derived_init(GTypeInstance *instance, gpointer fuckit) {
}

static void spectra_vector_derived_class_finalize(gpointer g_class,
                                                  gpointer class_data) {
    SpectraVectorClass *class = SPECTRA_VECTOR_CLASS(g_class);
    g_ptr_array_unref(class->axes);
    g_free(class->name);
}

SpectraVector *spectra_vector_as_real(SpectraVector *vecp) {
    SpectraVectorClass *class = (SpectraVectorClass*)G_OBJECT_GET_CLASS(vecp);
    if (class->vtable.as_real)
        return class->vtable.as_real(vecp);
    else
        return NULL;
}

SpectraVector *spectra_vector_add(SpectraVector *lhs, SpectraVector *rhs) {
    SpectraVector *ret;
    SpectraVectorClass *lhs_class = (SpectraVectorClass*)G_OBJECT_GET_CLASS(lhs);
    if (lhs_class->vtable.add && (ret = lhs_class->vtable.add(lhs, rhs)))
        return ret;
    g_error("do not know how to add %s and %s\n",
            G_OBJECT_TYPE_NAME(lhs), G_OBJECT_TYPE_NAME(rhs));
}

SpectraVector *spectra_vector_sub(SpectraVector *lhs, SpectraVector *rhs) {
    SpectraVector *ret;
    SpectraVectorClass *lhs_class = (SpectraVectorClass*)G_OBJECT_GET_CLASS(lhs);
    if (lhs_class->vtable.sub && (ret = lhs_class->vtable.sub(lhs, rhs)))
        return ret;
    g_error("do not know how to subtract %s and %s\n",
            G_OBJECT_TYPE_NAME(lhs), G_OBJECT_TYPE_NAME(rhs));
}

SpectraVector *spectra_vector_mult(SpectraVector *lhs, SpectraVector *rhs) {
    SpectraVector *ret;
    SpectraVectorClass *lhs_class = (SpectraVectorClass*)G_OBJECT_GET_CLASS(lhs);
    if (lhs_class->vtable.mult && (ret = lhs_class->vtable.mult(lhs, rhs)))
        return ret;
    g_error("do not know how to multiply %s and %s\n",
            G_OBJECT_TYPE_NAME(lhs), G_OBJECT_TYPE_NAME(rhs));
}

SpectraVector *spectra_vector_div(SpectraVector *lhs, SpectraVector *rhs) {
    SpectraVector *ret;
    SpectraVectorClass *lhs_class = (SpectraVectorClass*)G_OBJECT_GET_CLASS(lhs);
    if (lhs_class->vtable.div && (ret = lhs_class->vtable.div(lhs, rhs)))
        return ret;
    g_error("do not know how to divide %s and %s\n",
            G_OBJECT_TYPE_NAME(lhs), G_OBJECT_TYPE_NAME(rhs));
}

SpectraVector* spectra_vector_scale(SpectraVector *lhs, spectra_scalar rhs) {
    SpectraVector *ret;
    SpectraVectorClass *lhs_class = (SpectraVectorClass*)G_OBJECT_GET_CLASS(lhs);
    if (lhs_class->vtable.scale && (ret = lhs_class->vtable.scale(lhs, rhs)))
        return ret;
    g_error("do not know how to scale %s\n", G_OBJECT_TYPE_NAME(lhs));
}

SpectraVector *spectra_vector_exp(SpectraVector *logarithm) {
    SpectraVector *ret;
    SpectraVectorClass *logarithm_class =
        (SpectraVectorClass*)G_OBJECT_GET_CLASS(logarithm);
    if (logarithm_class->vtable.exp &&
        (ret = logarithm_class->vtable.exp(logarithm))) {
        return ret;
    }
    g_error("do not know how to calculate natural exponent of %s\n", G_OBJECT_TYPE_NAME(logarithm));
}

SpectraVector *spectra_vector_ln(SpectraVector *val) {
    SpectraVector *ret;
    SpectraVectorClass *val_class =
        (SpectraVectorClass*)G_OBJECT_GET_CLASS(val);
    if (val_class->vtable.ln &&
        (ret = val_class->vtable.ln(val))) {
        return ret;
    }
    g_error("do not know how to calculate natural "
            "logarithm of %s\n", G_OBJECT_TYPE_NAME(val));
}

spectra_scalar spectra_vector_mag(SpectraVector *vecp) {
    spectra_scalar sum = 0.0;
    guint len = spectra_vector_len(vecp);
    guint index;
    for (index = 0; index < len; index++) {
        spectra_scalar comp = spectra_vector_index(vecp, index);
        sum += comp * comp;
    }
    return sqrt(sum);
}

GString *spectra_vector_gstring(SpectraVector *vecp) {
    GString *ret;
    SpectraVectorClass *vecp_class = (SpectraVectorClass*)G_OBJECT_GET_CLASS(vecp);
    if (vecp_class->vtable.to_str && (ret = vecp_class->vtable.to_str(vecp)))
        return ret;
    g_error("do not now how to convert %s to a string\n", G_OBJECT_TYPE_NAME(vecp));
}

gchar *spectra_vector_str(SpectraVector *vecp) {
    return g_string_free_and_steal(spectra_vector_gstring(vecp));
}

SpectraVector *spectra_real_new(spectra_scalar value) {
    return spectra_vector_new(SPECTRA_TYPE_REAL, value);
}

SpectraVector *spectra_complex_new(spectra_scalar real,
                                   spectra_scalar imaginary) {
    return spectra_vector_new(SPECTRA_TYPE_COMPLEX, real, imaginary);
}

SpectraVector *spectra_complex_exponential_new(spectra_scalar magnitude,
                                               spectra_scalar phase_angle) {
    return spectra_complex_new(magnitude * cos(phase_angle),
                               magnitude * sin(phase_angle));
}

#define SPECTRA_EPSILON 0.001

spectra_scalar spectra_vector_phase(SpectraVector *vecp) {
    // complex angle of 0-magnitude vector is undefined, just
    // return 0
    if (fabs(spectra_vector_mag(vecp)) < SPECTRA_EPSILON)
        return 0.0;

    if (SPECTRA_VECTOR_IS_REAL(vecp)) {
        return atan2(0.0, spectra_vector_index(vecp, 0));
    } else if (SPECTRA_VECTOR_IS_COMPLEX(vecp))
        return atan2(spectra_vector_index(vecp, 1), spectra_vector_index(vecp, 0));
    else
        errx(1, "need to implement %s for this unknown vector type!",
             G_STRFUNC);
}

spectra_scalar spectra_vector_real(SpectraVector *vecp) {
    if (SPECTRA_VECTOR_IS_REAL(vecp) || SPECTRA_VECTOR_IS_COMPLEX(vecp)) {
        return spectra_vector_index(vecp, 0);
    } else {
        errx(1, "need to implement %s for new unknown vector type", G_STRFUNC);
    }
}

spectra_scalar spectra_vector_imaginary(SpectraVector *vecp) {
    if (SPECTRA_VECTOR_IS_REAL(vecp))
        return 0.0;
    else if (SPECTRA_VECTOR_IS_COMPLEX(vecp))
        return spectra_vector_index(vecp, 1);
    else
        errx(1, "need to implement %s for new unknown vector type", G_STRFUNC);
}
