/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <math.h>
#include <err.h>
#include <glib.h>
#include <stdio.h>
#include <sndfile.h>

#include "tokens.h"
#include "parser.h"
#include "functions.h"
#include "filecache.h"

#define SPECTRA_FUNC_ERROR (spectra_func_error_quark())
G_DEFINE_QUARK(spectra-func-error-quark, spectra_func_error)

#define WRAP_UNARY(fn)                                                  \
    SpectraVector*                                                      \
    UNARY_WRAPPER(fn)(SpectraProgram* prog,                             \
                      SpectraProgramContext *ctxt,                      \
                      guint n_arg,                                      \
                      struct spectra_program_argument const *args,      \
                      GError **error) {                                 \
        if (n_arg != 1 || args->tp != SPECTRA_PROGRAM_MEMORY) {         \
            g_set_error(error, SPECTRA_FUNC_ERROR,                      \
                        SPECTRA_FUNC_INVALID_ARG,                       \
                        "the %s function accepts exactly one argument, " \
                        "and it must be real.", #fn);                   \
            return NULL;                                                \
        }                                                               \
                                                                        \
        SpectraVector *vecp =                                           \
            spectra_program_context_read_real(ctxt,                     \
                                             args[0].index);            \
        if (vecp) {                                                     \
            SpectraVector *ret =                                        \
                spectra_real_new(fn(spectra_vector_index(vecp, 0)));    \
            g_object_unref(vecp);                                       \
            return ret;                                                 \
        }                                                               \
                                                                        \
        g_set_error(error, SPECTRA_FUNC_ERROR,                          \
                    SPECTRA_FUNC_INVALID_ARG,                           \
                    "the %s function accepts exactly one argument, "    \
                    "and it must be real.", #fn);                       \
        return NULL;                                                    \
    }

WRAP_UNARY(floor)
WRAP_UNARY(ceil)

SpectraVector*
spectra_unary_sqrt(SpectraProgram* prog, SpectraProgramContext *ctxt,
                   guint n_arg, struct spectra_program_argument const *args,
                   GError **error) {
        if (n_arg != 1 || args->tp != SPECTRA_PROGRAM_MEMORY) {
            g_set_error(error, SPECTRA_FUNC_ERROR,
                        SPECTRA_FUNC_INVALID_ARG,
                        "the %s function accepts exactly one "
                        "argument. ", G_STRFUNC);
            return NULL;
        }
        SpectraVector *vecp =
            spectra_program_context_read_vec(ctxt,
                                             args[0].index);
        if (SPECTRA_VECTOR_IS_REAL(vecp)) {
            spectra_scalar real = spectra_vector_index(vecp, 0);
            if (real >= 0.0)
                return spectra_real_new(sqrt(real));
            else
                return spectra_complex_new(0.0, sqrt(fabs(real)));
        } else if (SPECTRA_VECTOR_IS_COMPLEX(vecp)) {
            spectra_scalar mag = spectra_vector_mag(vecp);
            spectra_scalar phase = spectra_vector_phase(vecp);
            spectra_scalar result_mag = sqrt(mag);
            spectra_scalar result_phase = phase / 2.0;
            return spectra_complex_new(result_mag * cos(result_phase),
                                       result_mag * sin(result_phase));
        } else {
            g_set_error(error, SPECTRA_FUNC_ERROR,
                        SPECTRA_FUNC_INVALID_ARG,
                        "the %s function accepts exactly one argument, "
                        "and it must be real or complex.", G_STRFUNC);
            return NULL;
        }
}

SpectraVector*
spectra_mag(SpectraProgram* prog, SpectraProgramContext *ctxt,
            guint n_arg, struct spectra_program_argument const *args,
            GError **error) {
    spectra_scalar val = 0.0;

    if (!n_arg) {
        g_set_error(error, SPECTRA_FUNC_ERROR, SPECTRA_FUNC_UNDEFINED_RESULT,
                    "The empty-set does not have a defined magnitude!");
        return NULL;
    }

    if (n_arg != 1) {
        g_set_error(error, SPECTRA_FUNC_ERROR, SPECTRA_FUNC_UNDEFINED_RESULT,
                    "only one argument allowed to mag");
        return NULL;
    }

    if (args->tp != SPECTRA_PROGRAM_MEMORY) {
        g_set_error(error, SPECTRA_FUNC_ERROR, SPECTRA_FUNC_INVALID_ARG,
                    "you cannot take the magnitude of a string!");
        return NULL;
    }

    SpectraVector *vecp = spectra_program_context_read_vec(ctxt, args->index);
    guint n_dims = spectra_vector_len(vecp);
    spectra_scalar square_mag = 0.0;
    guint dim;
    for (dim = 0; dim < n_dims; dim++) {
        spectra_scalar elem = spectra_vector_index(vecp, dim);
        square_mag += elem * elem;
    }

    return spectra_real_new(sqrt(square_mag));
}

SpectraVector*
spectra_print(SpectraProgram* prog, SpectraProgramContext *ctxt,
              guint n_arg, struct spectra_program_argument const *args,
              GError **error) {
    char const *pre = "";
    while (n_arg-- >= 1) {
        switch (args->tp) {
        case SPECTRA_PROGRAM_MEMORY: {
            SpectraVector *vecp =
                spectra_program_context_read_vec(ctxt, args++->index);
            gchar *as_string = spectra_vector_str(vecp);
            printf("%s%s", pre, as_string);
        }
            break;
        case SPECTRA_PROGRAM_STRING:
            printf("%s%s", pre,
                   spectra_program_string_literal_index(prog, args++->index));
            break;
        default:
            g_set_error(error, SPECTRA_FUNC_ERROR, SPECTRA_FUNC_INVALID_ARG,
                        "unknown argument type %02x", args++->tp);
            return NULL;
        }
        pre = " ";
    }
    putchar('\n');
    return spectra_real_new(0.0);
}

SpectraVector*
spectra_sample(SpectraProgram *prog, SpectraProgramContext *ctxt,
               guint n_arg, struct spectra_program_argument const *args,
               GError **error) {
    if ((n_arg == 2 || n_arg == 3) &&
        args[0].tp == SPECTRA_PROGRAM_STRING &&
        args[1].tp == SPECTRA_PROGRAM_MEMORY) {

        guint ch;
        if (n_arg == 3) {
            SpectraVector *ch_as_vec;
            if (args[2].tp == SPECTRA_PROGRAM_MEMORY &&
                (ch_as_vec = spectra_program_context_read_real(ctxt, args[2].index))) {
                ch = spectra_vector_index(ch_as_vec, 0);
                g_object_unref(ch_as_vec);
            } else {
                g_set_error(error, SPECTRA_FUNC_ERROR,
                            SPECTRA_FUNC_INVALID_ARG, "third parameter to "
                            "sample must be a scalar value indicating "
                            "channel number");
                return NULL;
            }
        } else {
            ch = 0;
        }

        gchar const *path = spectra_program_string_literal_index(prog, args[0].index);
        SpectraVector *time_as_vec;
        if (!(time_as_vec = spectra_program_context_read_real(ctxt, args[1].index))) {
            g_set_error(error, SPECTRA_FUNC_ERROR, SPECTRA_FUNC_INVALID_ARG,
                        "time has to be a real number.  It\'s not.  Womp womp.");
            return NULL;
        }
        spectra_scalar time = spectra_vector_index(time_as_vec, 0);
        g_object_unref(time_as_vec);
        time_as_vec = NULL;

        SpectraFile *file = spectra_program_context_load_file(ctxt, path, error);
        if (file)
            return spectra_file_sample(file, time, ch);
        else
            return NULL;
    } else {
        g_set_error(error, SPECTRA_FUNC_ERROR, SPECTRA_FUNC_INVALID_ARG,
                    "sample only accepts two arguments; first must be a "
                    "string literal, second must be a scalar time index; "
                    "you tried to send %u arguments.", n_arg);
        return NULL;
    }
}
