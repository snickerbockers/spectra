/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <glib.h>

#include "parser.h"
#include "program.h"

G_BEGIN_DECLS

typedef enum {
    SPECTRA_FUNC_INVALID_ARG,
    SPECTRA_FUNC_UNDEFINED_RESULT,
    SPECTRA_FUNC_ARG_OUT_OF_RANGE
} SpectraFuncError;

#define UNARY_WRAPPER(fn) spectra_unary_##fn

/*******************************************************************************
 **
 ** these function handlers must all return references which belong to the
 ** caller
 **
 ******************************************************************************/
#define UNARY_WRAPPER_DECL(fn)                                          \
    SpectraVector* UNARY_WRAPPER(fn)(SpectraProgram *prog,              \
                                     SpectraProgramContext *ctxt,       \
                                     guint n_arg,                       \
                                     struct spectra_program_argument const *args, \
                                     GError **err)

UNARY_WRAPPER_DECL(floor);
UNARY_WRAPPER_DECL(ceil);
UNARY_WRAPPER_DECL(sqrt);

// magnitude
SpectraVector*
spectra_mag(SpectraProgram *prog, SpectraProgramContext *ctxt,
            guint n_arg, struct spectra_program_argument const *args,
            GError **err);

/*******************************************************************************
 **
 ** prints a message to stdout and returns 0
 ** this is merely for debugging/testing purposes and will be removed.
 **
 ******************************************************************************/
SpectraVector*
spectra_print(SpectraProgram *prog, SpectraProgramContext *ctxt,
              guint n_arg, struct spectra_program_argument const *args,
              GError **err);

SpectraVector*
spectra_sample(SpectraProgram *prog, SpectraProgramContext *ctxt,
               guint n_arg, struct spectra_program_argument const *args,
               GError **err);

G_END_DECLS

#endif
