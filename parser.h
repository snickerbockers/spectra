/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef PARSER_H_
#define PARSER_H_

#include <stdbool.h>

#include <glib.h>

#include "program.h"
#include "signal.h"

typedef enum {
    SPECTRA_PARSER_ERROR_TREE,
    SPECTRA_PARSER_ERROR_UNIMPLEMENTED,
    SPECTRA_PARSER_ERROR_INVALID_LITERAL,
    SPECTRA_PARSER_ERROR_SURPRISING_TOKEN
} SpectraParserError;

struct token; // see tokens.h

struct parser {
    GSList *expr;
    GSList *unparsed;
};

void parser_init(struct parser *parser);
void parser_cleanup(struct parser *parser);

bool parser_complete(struct parser const *parser);

void parser_print_stack(struct parser const *parser);

void parser_print_tree(struct parser const *parser);

int parser_parse(struct parser *parser,
                 struct tokenizer *tokp, GError **error);

SpectraSignal *parser_get_signal(struct parser const *parser,
                                 SpectraConstantBuffer *cbuf,
                                 gchar const *inputs, GError **error);

#endif
