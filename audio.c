/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <err.h>
#include <string.h>
#include <limits.h>

#include "vector.h"

#include "audio.h"

static void
output_ready_callback(void *argp, Uint8 *stream, int n_bytes);

#define SAMP_BUF_LEN 1024

int audio_init(struct audio_output *ao, unsigned n_chans, int sample_rate,
               GPtrArray *samples) {
    memset(ao, 0, sizeof(*ao));

    SDL_AudioSpec expectation = {
        .freq = sample_rate,
        .format = AUDIO_S16,
        .channels = n_chans,
        .samples = SAMP_BUF_LEN,
        .callback = output_ready_callback,
        .userdata = ao
    };
    SDL_AudioSpec reality;

    SDL_AudioDeviceID dev =
        SDL_OpenAudioDevice(NULL, 0, &expectation, &reality, 0);

    if (dev < 0) {
        warnx("failure to open default audio device");
        return -1;
    }

    ao->dev = dev;
    ao->sample_rate = reality.freq;
    ao->n_chans = reality.channels;
    ao->is_playing = false;

    ao->samples = g_ptr_array_ref(samples);
    ao->pos = 0;

    ao->is_playing = true;

    printf("audio will be played on %s\n", SDL_GetAudioDriver(0));

    return 0;
}

void audio_cleanup(struct audio_output *ao) {
    g_ptr_array_unref(ao->samples);
    SDL_CloseAudioDevice(ao->dev);
    memset(ao, 0, sizeof(*ao));
}

void audio_set_samples(struct audio_output *ao, GPtrArray *samples) {
    if (ao->samples)
        g_ptr_array_unref(ao->samples);
    ao->samples = g_ptr_array_ref(samples);
}

static void
output_ready_callback(void *argp, Uint8 *stream, int n_bytes) {
    struct audio_output *ao = argp;
    Uint16 *outp = (Uint16*)stream;
    unsigned n_samp = n_bytes / (2 * ao->n_chans);
    while (n_samp--) {
        unsigned chan;
        SpectraVector *vecp = g_ptr_array_index(ao->samples, ao->pos);
        SpectraVector *real_vecp = spectra_vector_as_real(vecp);

        spectra_scalar sample_in;
        if (real_vecp) {
            sample_in = spectra_vector_index(real_vecp, 0);
            g_object_unref(real_vecp);
            real_vecp = NULL;
        } else {
            g_error("%s - result index %u is non-real", G_STRFUNC, ao->pos);
        }
        sample_in *= INT16_MAX;
        Uint16 sample;
        if (sample_in <= INT16_MIN)
            sample = INT16_MIN;
        else if (sample_in >= INT16_MAX)
            sample = INT16_MAX;
        else
            sample = sample_in;

        for (chan = 0; chan < ao->n_chans; chan++)
            *outp++ = sample;
        ao->pos = (ao->pos + 1) % ao->samples->len;
    }
}

void audio_start(struct audio_output *ao) {
    SDL_PauseAudioDevice(ao->dev, 0);
    ao->is_playing = true;
}

void audio_stop(struct audio_output *ao) {
    SDL_PauseAudioDevice(ao->dev, 1);
    ao->is_playing = false;
}
