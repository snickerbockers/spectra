/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef SYMBOL_H_
#define SYMBOL_H_

#include <glib.h>

enum prog_sym_tp {
    PROG_SYM_TP_INVALID,

    // signal parameter
    PROG_SYM_TP_INPUT
};

#define PROG_SYM_MAX 64
struct prog_sym {
    grefcount cnt;

    gchar name[PROG_SYM_MAX];
    enum prog_sym_tp tp;
    unsigned addr;
};

struct symbol_table {
    grefcount cnt;
    GPtrArray *syms;
};

struct prog_sym *prog_sym_new(void);
struct prog_sym *prog_sym_ref(struct prog_sym *sym);
void prog_sym_unref(struct prog_sym *sym);

struct symbol_table *symbol_table_new(void);
struct symbol_table *symbol_table_ref(struct symbol_table *tbl);
void symbol_table_unref(struct symbol_table *tbl);

// returns NULL if the given symbol could not be found
struct prog_sym *symbol_table_query(struct symbol_table *tbl,
                                    char const *symbol_name);
struct prog_sym *symbol_table_reverse_query(struct symbol_table *tbl,
                                            enum prog_sym_tp tp,
                                            unsigned index);

struct prog_sym *symbol_table_query_one_type(struct symbol_table *tbl,
                                             enum prog_sym_tp tp,
                                             char const *symbol_name);

void symbol_table_add_symbol(struct symbol_table *table,
                             struct prog_sym *symbol);

void symbol_table_foreach(struct symbol_table *table,
                          GFunc function, gpointer user_data);
unsigned symbol_table_len(struct symbol_table const *table);
struct prog_sym *symbol_table_index_const(struct symbol_table const *table, unsigned index);
struct prog_sym *symbol_table_index(struct symbol_table *table, unsigned index);

struct prog_sym_init {
    enum prog_sym_tp tp;
    gchar name[PROG_SYM_MAX];
    gpointer impl;
};

#endif
