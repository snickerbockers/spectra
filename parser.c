/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <err.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <assert.h>

#include <glib.h>

#include "tokens.h"
#include "parser.h"
#include "signal.h"
#include "symbol.h"

#define SPECTRA_PARSER_ERROR (spectra_parser_error_quark())
G_DEFINE_QUARK(spectra-parser-error-quark, spectra_parser_error)

enum expr_node_op {
    OP_INVALID,

    OP_LITERAL,
    OP_VARIABLE,
    OP_STRING,

    OP_ADD,
    OP_SUB,
    OP_MUL,
    OP_DIV,

    OP_FUNCTION_INVOCATION,
    OP_ARGUMENT_LIST
};

enum expr_node_class {
    EXPR_NODE_CLASS_INVALID,

    EXPR_NODE_CLASS_IDENTIFIER,
    EXPR_NODE_CLASS_SUBEXPRESSION,
    EXPR_NODE_CLASS_PRODUCT,
    EXPR_NODE_CLASS_SUM,
    EXPR_NODE_CLASS_EXPR,

    EXPR_NODE_CLASS_STRING,

    /*
     * ARGUMENT LISTS:
     * prereq[0] - EXPR_NODE_CLASS_SUM of left-most argument
     * prereq[1] - EXPR_NODE_CLASS_ARGUMENT_LIST of all other arguments
     *             (NULL if this is the final argument)
     *
     * so it's like a linked-list with the top-most node (top:=closer to root)
     * being to the left of all subsequent nodes
     */
    EXPR_NODE_CLASS_ARGUMENT_LIST,

    // this is an argument list which has been fully parsed (ie both
    // parenthesis have been processed) and therefore can no longer
    // accept new arguments
    EXPR_NODE_CLASS_COMPLETE_ARGUMENT_LIST,

    EXPR_NODE_CLASS_PLUS = '+',
    EXPR_NODE_CLASS_MINUS = '-',
    EXPR_NODE_CLASS_AST = '*',
    EXPR_NODE_CLASS_SLASH = '/',
    EXPR_NODE_CLASS_OPEN_PAREN = '(',
    EXPR_NODE_CLASS_CLOSE_PAREN = ')',
    EXPR_NODE_CLASS_OPEN_BRACE = '{',
    EXPR_NODE_CLASS_CLOSE_BRACE = '}',
    EXPR_NODE_CLASS_COMMA = ','
};

struct expr_node {
    enum expr_node_class class;
    enum expr_node_op op;

    union {
        char *ident; // for variables only
        char *text;
        SpectraVector *value; // for literals only
    };
    // for unary and binary operators
    // (unary will only use prereq[0]
    struct expr_node *prereq[2];
};

static void expr_destroy_notify(void *ptr);
static void parser_push(struct parser *parser, struct expr_node *nodep);
static void print_single_inst(gpointer data, gpointer user_ptr);

void parser_init(struct parser *parser) {
    memset(parser, 0, sizeof(*parser));
}

void parser_cleanup(struct parser *parser) {
    g_slist_free_full(parser->expr, expr_destroy_notify);
    g_slist_free_full(parser->unparsed, expr_destroy_notify);
}

static void expr_destroy_notify(void *ptr) {
    struct expr_node *root = ptr;

    switch (root->op) {
    case OP_LITERAL:
        if (root->value) {
            g_object_unref(root->value);
            root->value = NULL;
        }
        break;
    case OP_FUNCTION_INVOCATION:
        if (root->prereq[0])
            expr_destroy_notify(root->prereq[0]);
        // INTENTIONAL FALL-THROUGH
    case OP_VARIABLE:
        g_free(root->ident);
        break;
    case OP_STRING:
        g_free(root->text);
        break;
    case OP_ADD:
    case OP_SUB:
    case OP_MUL:
    case OP_DIV:
    case OP_ARGUMENT_LIST:
        if (root->prereq[1])
            expr_destroy_notify(root->prereq[1]);
        if (root->prereq[0])
            expr_destroy_notify(root->prereq[0]);
    }
    g_free(root);
}

static guint node_count(struct parser const *parser) {
    return g_slist_length(parser->expr);
}

bool parser_complete(struct parser const *parser) {
    return node_count(parser) == 1 &&
        ((struct expr_node*)parser->expr->data)->class == EXPR_NODE_CLASS_SUM;
}

static enum expr_node_class parser_lookahead(struct parser *parser) {
    if (parser->unparsed)
        return ((struct expr_node*)parser->unparsed->data)->class;
    else
        return EXPR_NODE_CLASS_INVALID;
}

static struct expr_node* parser_ingest(struct parser *parser) {
    GSList *ingested_link = parser->unparsed;
    if (ingested_link) {
        struct expr_node *ingested_node = ingested_link->data;

        parser->unparsed = g_slist_remove_link(parser->unparsed, ingested_link);
        g_slist_free(ingested_link);
        parser_push(parser, ingested_node);

        return ingested_node;
    } else {
        return NULL;
    }
}

static struct expr_node *parser_pop(struct parser *parser) {
    if (parser->expr) {
        GSList *linkp = parser->expr;
        struct expr_node *nodep = linkp->data;
        parser->expr = g_slist_remove_link(parser->expr, linkp);
        g_slist_free(linkp);
        return nodep;
    } else {
        g_warning("no data to pop");
        return NULL;
    }
}

static void parser_push(struct parser *parser, struct expr_node *nodep) {
    parser->expr = g_slist_prepend(parser->expr, nodep);
}

static void print_single_expr_node(gpointer datp, gpointer user_data) {
    struct expr_node *nodep = datp;
    char const *prefix = "";
    switch (nodep->class) {
    case EXPR_NODE_CLASS_INVALID:
        g_info("[ INVALID ]\n");
        break;
    case EXPR_NODE_CLASS_SUM:
        g_info("[ SUM ]\n");
        break;
    case EXPR_NODE_CLASS_EXPR:
        g_info("[ EXPR ]\n");
        break;
    case EXPR_NODE_CLASS_STRING:
        g_info("[ STRING ]\n");
    default:
        g_info("%c\n", nodep->class);
    }
}

void parser_print_stack(struct parser const *parser) {
    g_slist_foreach(parser->expr, print_single_expr_node, NULL);
}

static int parser_push_token(gpointer datp, gpointer user_data, GError **error) {
    struct parser *parser = user_data;
    struct token *tok = datp;
    struct expr_node *new_node = g_malloc0(sizeof(struct expr_node));

    switch (tok->class) {
    case TOKEN_CLASS_IDENTIFIER:
        printf("INGEST IDENTIFIER %s\n", tok->text);
        new_node->class = EXPR_NODE_CLASS_IDENTIFIER;
        new_node->op = OP_VARIABLE;
        new_node->ident = g_strdup(tok->text);
        break;
    case TOKEN_CLASS_LITERAL:
        new_node->class = EXPR_NODE_CLASS_PRODUCT;
        new_node->op = OP_LITERAL;
        errno = 0;
        new_node->value = spectra_real_new(strtod(tok->text, NULL));
        printf("INGEST LITERAL %f\n", new_node->value);
        if (errno) {
            g_set_error(error, SPECTRA_PARSER_ERROR,
                        SPECTRA_PARSER_ERROR_INVALID_LITERAL,
                        "failure to parse literal \"%s\"", tok->text);
            return -1;
        }
        break;
    case TOKEN_CLASS_PLUS:
        printf("INGEST +\n");
        new_node->class = EXPR_NODE_CLASS_PLUS;
        new_node->op = OP_ADD;
        break;
    case TOKEN_CLASS_MINUS:
        printf("INGEST -\n");
        new_node->class = EXPR_NODE_CLASS_MINUS;
        new_node->op = OP_INVALID;
        break;
    case TOKEN_CLASS_AST:
        printf("INGEST *\n");
        new_node->class = EXPR_NODE_CLASS_AST;
        new_node->op = OP_MUL;
        break;
    case TOKEN_CLASS_SLASH:
        printf("INGEST /\n");
        new_node->class = EXPR_NODE_CLASS_SLASH;
        new_node->op = OP_DIV;
        break;
    case TOKEN_CLASS_OPEN_PAREN:
        printf("INGEST (\n");
        new_node->class = EXPR_NODE_CLASS_OPEN_PAREN;
        new_node->op = OP_INVALID;
        break;
    case TOKEN_CLASS_CLOSE_PAREN:
        printf("INGEST )\n");
        new_node->class = EXPR_NODE_CLASS_CLOSE_PAREN;
        new_node->op = OP_INVALID;
        break;
    case TOKEN_CLASS_OPEN_BRACE:
        printf("INGEST {\n");
        new_node->class = EXPR_NODE_CLASS_OPEN_BRACE;
        new_node->op = OP_INVALID;
        break;
    case TOKEN_CLASS_CLOSE_BRACE:
        printf("INGEST }\n");
        new_node->class = EXPR_NODE_CLASS_CLOSE_BRACE;
        new_node->op = OP_INVALID;
        break;
    case TOKEN_CLASS_COMMA:
        printf("INGEST ,\n");
        new_node->class = EXPR_NODE_CLASS_COMMA;
        new_node->op = OP_INVALID;
        break;
    case TOKEN_CLASS_STRING:
        new_node->class = EXPR_NODE_CLASS_STRING;
        new_node->op = OP_STRING;
        {
            size_t n_char = strlen(tok->text);
            if (n_char > 2)
                new_node->text = g_strndup(tok->text + 1, n_char - 2);
            else
                new_node->text = g_strdup("");
            printf("INGEST STRING (%s)\n", new_node->text);
        }
        break;
    case TOKEN_CLASS_INVALID:
    case TOKEN_CLASS_WHITESPACE:
    default:
        g_set_error(error, SPECTRA_PARSER_ERROR,
                    SPECTRA_PARSER_ERROR_SURPRISING_TOKEN,
                    "invalid token %d passed to parser", (int)tok->class);
        return -1;
    }

    parser->unparsed = g_slist_append(parser->unparsed, new_node);
    return 0;
}

int parser_parse(struct parser *parser,
                 struct tokenizer *tokp, GError **error) {
    /*
     * product : literal
     * product : identifier
     * product : product '*' product
     * product : product '/' product
     * product : '-' product
     *
     * sum : product
     * sum : product '+' product
     * sum : product '-' product
     *
     * expr : sum
     */

    GSList *cursor;
    for (cursor = tokp->tokens; cursor; cursor = g_slist_next(cursor))
        if (parser_push_token(cursor->data, parser, error) != 0)
            return -1;

    for (;;) {
        struct expr_node *top[3] = { NULL, NULL, NULL };
        GSList *top_links[3] = { NULL, NULL, NULL };
        enum expr_node_class top_class[3] = {
            EXPR_NODE_CLASS_INVALID,
            EXPR_NODE_CLASS_INVALID,
            EXPR_NODE_CLASS_INVALID
        };
        enum expr_node_class lookahead = parser_lookahead(parser);

        if (parser->expr) {
            top_links[0] = parser->expr;
            top[0] = top_links[0]->data;
            top_class[0] = top[0]->class;
            if (top_links[0]->next) {
                top_links[1] = top_links[0]->next;
                top[1] = top_links[1]->data;
                top_class[1] = top[1]->class;
                if (top_links[1]->next) {
                    top_links[2] = top_links[1]->next;
                    top[2] = top_links[2]->data;
                    top_class[2] = top[2]->class;
                }
            }
        }

        if (top_class[0] == EXPR_NODE_CLASS_PRODUCT &&
            top_class[1] == EXPR_NODE_CLASS_AST &&
            top_class[2] == EXPR_NODE_CLASS_PRODUCT) {
            struct expr_node *rhs = parser_pop(parser);
            struct expr_node *ast = parser_pop(parser);
            struct expr_node *lhs = parser_pop(parser);

            ast->op = OP_MUL;
            ast->class = EXPR_NODE_CLASS_PRODUCT;
            ast->prereq[0] = lhs;
            ast->prereq[1] = rhs;

            parser_push(parser, ast);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_PRODUCT &&
                   top_class[1] == EXPR_NODE_CLASS_SLASH &&
                   top_class[2] == EXPR_NODE_CLASS_PRODUCT) {
            struct expr_node *rhs = parser_pop(parser);
            struct expr_node *slash = parser_pop(parser);
            struct expr_node *lhs = parser_pop(parser);

            slash->op = OP_DIV;
            slash->class = EXPR_NODE_CLASS_PRODUCT;
            slash->prereq[0] = lhs;
            slash->prereq[1] = rhs;

            parser_push(parser, slash);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_SUM &&
                   top_class[1] == EXPR_NODE_CLASS_PLUS &&
                   top_class[2] == EXPR_NODE_CLASS_SUM &&
                   lookahead != TOKEN_CLASS_SLASH && lookahead != TOKEN_CLASS_AST) {
            struct expr_node *rhs = parser_pop(parser);
            struct expr_node *plus = parser_pop(parser);
            struct expr_node *lhs = parser_pop(parser);
            plus->op = OP_ADD;
            plus->class = EXPR_NODE_CLASS_SUM;
            plus->prereq[0] = lhs;
            plus->prereq[1] = rhs;

            parser_push(parser, plus);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_SUM &&
                   top_class[1] == EXPR_NODE_CLASS_MINUS &&
                   top_class[2] == EXPR_NODE_CLASS_SUM &&
                   lookahead != TOKEN_CLASS_SLASH && lookahead != TOKEN_CLASS_AST) {
            struct expr_node *rhs = parser_pop(parser);
            struct expr_node *minus = parser_pop(parser);
            struct expr_node *lhs = parser_pop(parser);
            minus->op = OP_SUB;
            minus->class = EXPR_NODE_CLASS_SUM;
            minus->prereq[0] = lhs;
            minus->prereq[1] = rhs;

            parser_push(parser, minus);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_PRODUCT &&
                   top_class[1] == EXPR_NODE_CLASS_MINUS &&
                   top_class[2] != EXPR_NODE_CLASS_SUM) {
            struct expr_node *operand = parser_pop(parser);
            struct expr_node *neg = parser_pop(parser);
            struct expr_node *minus_one = g_malloc0(sizeof(struct expr_node));

            minus_one->op = OP_LITERAL;
            minus_one->class = EXPR_NODE_CLASS_SUM;
            minus_one->value = spectra_real_new(-1.0);

            neg->op = OP_MUL;
            neg->class = EXPR_NODE_CLASS_PRODUCT;
            neg->prereq[0] = minus_one;
            neg->prereq[1] = operand;

            parser_push(parser, neg);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_PRODUCT &&
                   lookahead != TOKEN_CLASS_SLASH &&
                   lookahead != TOKEN_CLASS_AST) {
            // promote products to sums
            top[0]->class = EXPR_NODE_CLASS_SUM;
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_CLOSE_PAREN &&
                   top_class[1] == EXPR_NODE_CLASS_SUM &&
                   top_class[2] == EXPR_NODE_CLASS_OPEN_PAREN) {
            // parenthesized subexpression
            // TODO: check that top_class[3] != IDENTIFIER (if it does
            //       then this must be a function invocation)
            struct expr_node *close_paren = parser_pop(parser);
            struct expr_node *subexp = parser_pop(parser);
            struct expr_node *open_paren = parser_pop(parser);
            expr_destroy_notify(open_paren);
            expr_destroy_notify(close_paren);
            subexp->class = EXPR_NODE_CLASS_SUBEXPRESSION;
            parser_push(parser, subexp);
            continue;

            /*******************************************************************
             **
             ** subexpression: '(' SUM ')'
             ** argument_list: ',' SUM ')' OR
             **                ',' SUM argument_list OR
             **                '(' SUM argument_list
             ** function_invocation: identifier subexpression OR
             **                      identifier argument_list
             **
             ******************************************************************/
        } else if (top_class[0] == EXPR_NODE_CLASS_CLOSE_PAREN &&
                   top_class[1] == EXPR_NODE_CLASS_STRING &&
                   top_class[2] == EXPR_NODE_CLASS_COMMA) {
            struct expr_node *close_paren = parser_pop(parser);
            struct expr_node *string = parser_pop(parser);
            struct expr_node *comma = parser_pop(parser);

            expr_destroy_notify(comma);

            close_paren->class = EXPR_NODE_CLASS_ARGUMENT_LIST;
            close_paren->op = OP_ARGUMENT_LIST;
            close_paren->prereq[0] = string;
            close_paren->prereq[1] = NULL;

            parser_push(parser, close_paren);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_CLOSE_PAREN &&
                   top_class[1] == EXPR_NODE_CLASS_SUM &&
                   top_class[2] == EXPR_NODE_CLASS_COMMA) {
            struct expr_node *close_paren = parser_pop(parser);
            struct expr_node *arg = parser_pop(parser);
            struct expr_node *comma = parser_pop(parser);

            comma->class = EXPR_NODE_CLASS_ARGUMENT_LIST;
            comma->op = OP_ARGUMENT_LIST;
            comma->prereq[0] = arg;
            comma->prereq[1] = NULL;

            parser_push(parser, comma);
            expr_destroy_notify(close_paren);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_ARGUMENT_LIST &&
                   (top_class[1] == EXPR_NODE_CLASS_SUM ||
                    top_class[1] == EXPR_NODE_CLASS_STRING) &&
                   top_class[2] == EXPR_NODE_CLASS_COMMA) {
            /*
             * NOTE: the comma was already folded into the previous
             * EXPR_NODE_CLASS_ARGUMENT_LIST
             */
            struct expr_node *other_args = parser_pop(parser);
            struct expr_node *new_arg  = parser_pop(parser);
            struct expr_node *comma = parser_pop(parser);

            comma->class = EXPR_NODE_CLASS_ARGUMENT_LIST;
            comma->op = OP_ARGUMENT_LIST;
            comma->prereq[0] = new_arg;
            comma->prereq[1] = other_args;

            parser_push(parser, comma);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_ARGUMENT_LIST &&
                   (top_class[1] == EXPR_NODE_CLASS_SUM ||
                    top_class[1] == EXPR_NODE_CLASS_STRING) &&
                   top_class[2] == EXPR_NODE_CLASS_OPEN_PAREN) {
            struct expr_node *other_args = parser_pop(parser);
            struct expr_node *new_arg  = parser_pop(parser);
            struct expr_node *open_paren = parser_pop(parser);
            open_paren->class = EXPR_NODE_CLASS_COMPLETE_ARGUMENT_LIST;
            open_paren->op = OP_ARGUMENT_LIST;
            open_paren->prereq[0] = new_arg;
            open_paren->prereq[1] = other_args;

            parser_push(parser, open_paren);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_SUBEXPRESSION &&
                   top_class[1] == EXPR_NODE_CLASS_IDENTIFIER) {
            struct expr_node *params = parser_pop(parser);
            struct expr_node *func_ident = parser_pop(parser);

            struct expr_node *arg_node = g_malloc0(sizeof(struct expr_node));
            arg_node->class = EXPR_NODE_CLASS_COMPLETE_ARGUMENT_LIST;
            arg_node->op = OP_ARGUMENT_LIST;
            arg_node->prereq[0] = params;
            arg_node->prereq[1] = NULL; // unary function

            func_ident->class = EXPR_NODE_CLASS_PRODUCT;
            func_ident->op = OP_FUNCTION_INVOCATION;
            func_ident->prereq[0] = arg_node;
            parser_push(parser, func_ident);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_CLOSE_PAREN &&
                   top_class[1] == EXPR_NODE_CLASS_STRING &&
                   top_class[2] == EXPR_NODE_CLASS_OPEN_PAREN) {
            struct expr_node *close_paren = parser_pop(parser);
            struct expr_node *string = parser_pop(parser);
            struct expr_node *open_paren = parser_pop(parser);

            expr_destroy_notify(open_paren);

            close_paren->class = EXPR_NODE_CLASS_COMPLETE_ARGUMENT_LIST;
            close_paren->op = OP_ARGUMENT_LIST;
            close_paren->prereq[0] = string;
            close_paren->prereq[1] = NULL;

            parser_push(parser, close_paren);
            continue;
        } else if (top_class[0] == EXPR_NODE_CLASS_COMPLETE_ARGUMENT_LIST &&
                   top_class[1] == EXPR_NODE_CLASS_IDENTIFIER) {
            struct expr_node *args = parser_pop(parser);
            struct expr_node *ident = parser_pop(parser);

            ident->class = EXPR_NODE_CLASS_PRODUCT;
            ident->op = OP_FUNCTION_INVOCATION;
            ident->prereq[0] = args;
            parser_push(parser, ident);
            continue;
        } else if ((top_class[0] == EXPR_NODE_CLASS_IDENTIFIER &&
                    lookahead != TOKEN_CLASS_OPEN_PAREN) ||
                   top_class[0] == EXPR_NODE_CLASS_SUBEXPRESSION) {
            top[0]->class = EXPR_NODE_CLASS_PRODUCT;
            continue;
        } else if (parser_ingest(parser)) {
            continue;
        } else {
            // nothing to do, exit so we dont get stuck in a loop
            return 0;
        }
    }
    return 0;
}

static void print_expr_node(struct expr_node const *nodep) {
    g_info("NODE %p\n", nodep);

    switch (nodep->op) {
    case OP_LITERAL:
        g_info("LOAD LITERAL %f\n", nodep->value);
        break;
    case OP_VARIABLE:
        g_info("LOAD IDENTIFIER %s\n", nodep->ident);
        break;
    case OP_ADD:
        g_info("\tADD NODE %p AND %p\n", nodep->prereq[0], nodep->prereq[1]);
        if (nodep->prereq[0])
            print_expr_node(nodep->prereq[0]);
        if (nodep->prereq[1])
            print_expr_node(nodep->prereq[1]);
        break;
    case OP_SUB:
        g_info("\tSUBTRACT NODE %p FROM %p\n", nodep->prereq[1], nodep->prereq[0]);
        if (nodep->prereq[0])
            print_expr_node(nodep->prereq[0]);
        if (nodep->prereq[1])
            print_expr_node(nodep->prereq[1]);
        break;
    case OP_MUL:
        g_info("\tMULTIPLY NODE %p AND %p\n", nodep->prereq[1], nodep->prereq[0]);
        if (nodep->prereq[0])
            print_expr_node(nodep->prereq[0]);
        if (nodep->prereq[1])
            print_expr_node(nodep->prereq[1]);
        break;
    case OP_DIV:        
        g_info("\tDIVIDE NODE %p BU %p\n", nodep->prereq[1], nodep->prereq[0]);
        if (nodep->prereq[0])
            print_expr_node(nodep->prereq[0]);
        if (nodep->prereq[1])
            print_expr_node(nodep->prereq[1]);
        break;
    case OP_FUNCTION_INVOCATION:
        g_info("\tINVOKE FUNC %s(%p)\n", nodep->ident, nodep->prereq[0]);
        break;
    case OP_ARGUMENT_LIST:
        g_info("\tOP_ARGUMENT_LIST %p FOLLOWED BY %p\n", nodep->prereq[0], nodep->prereq[1]);
        break;
    case OP_STRING:
        g_info("\tOP_STRING \"%s\"\n", nodep->text);
        break;
    default:
        g_warning("invalid node %p\n", nodep);
    }
}

void parser_print_tree(struct parser const *parser) {
    if (parser->expr && parser->expr->data && !parser->expr->next)
        print_expr_node(parser->expr->data);
}

static SpectraSignal *
parser_convert_expr_node_to_signal(struct expr_node const *root,
                                   SpectraConstantBuffer *cbuf,
                                   gchar const *inputs, GError **error) {
    g_return_val_if_fail(root, NULL);
    switch (root->op) {
    case OP_LITERAL:
        return spectra_literal_signal_new(cbuf, root->value);
    case OP_VARIABLE:
        return spectra_symbol_signal_new(cbuf, root->ident);
    case OP_ADD:
        {
            GPtrArray *prereq = g_ptr_array_new_with_free_func(g_object_unref);
            g_ptr_array_add(prereq,
                            parser_convert_expr_node_to_signal(root->prereq[0],
                                                               cbuf, inputs, error));
            g_ptr_array_add(prereq,
                            parser_convert_expr_node_to_signal(root->prereq[1],
                                                               cbuf, inputs, error));
            SpectraSignal *sigp = spectra_sum_signal_new_full(cbuf, prereq);
            g_ptr_array_unref(prereq);
            return sigp;
        }
    case OP_SUB:
        {
            GPtrArray *sum_prereq = g_ptr_array_new_with_free_func(g_object_unref);
            g_ptr_array_add(sum_prereq,
                            parser_convert_expr_node_to_signal(root->prereq[0],
                                                               cbuf, inputs, error));

            SpectraSignal *rhs_abs =
                parser_convert_expr_node_to_signal(root->prereq[1],
                                                   cbuf, inputs, error);
            SpectraSignal *minus_one;
            {
                SpectraVector *minus_one_val = spectra_real_new(-1.0);
                minus_one = spectra_literal_signal_new(cbuf, minus_one_val);
                g_object_unref(minus_one_val);
            }
            GPtrArray *mult_prereq =
                g_ptr_array_new_with_free_func(g_object_unref);
            g_ptr_array_add(mult_prereq, minus_one);
            g_ptr_array_add(mult_prereq, rhs_abs);

            SpectraSignal *mult = spectra_mult_signal_new_full(cbuf,
                                                               mult_prereq);
            g_ptr_array_add(sum_prereq, mult);

            SpectraSignal *sum = spectra_sum_signal_new_full(cbuf,
                                                             sum_prereq);

            g_ptr_array_unref(sum_prereq);
            g_ptr_array_unref(mult_prereq);
            return sum;
        }
    case OP_MUL:
        {
            GPtrArray *prereq = g_ptr_array_new_with_free_func(g_object_unref);
            g_ptr_array_add(prereq,
                            parser_convert_expr_node_to_signal(root->prereq[0],
                                                               cbuf, inputs, error));
            g_ptr_array_add(prereq,
                            parser_convert_expr_node_to_signal(root->prereq[1],
                                                               cbuf, inputs, error));
            SpectraSignal *sigp = spectra_mult_signal_new_full(cbuf,
                                                               prereq);
            g_ptr_array_unref(prereq);
            return sigp;
        }
    case OP_DIV:
        {
            SpectraSignal *dividend =
                parser_convert_expr_node_to_signal(root->prereq[0],
                                                   cbuf, inputs, error);
            if (!dividend)
                return NULL;
            SpectraSignal *divisor =
                parser_convert_expr_node_to_signal(root->prereq[1],
                                                   cbuf, inputs, error);
            if (!divisor) {
                g_object_unref(dividend);
                return NULL;
            }
            SpectraSignal *sigp = spectra_div_signal_new(cbuf,
                                                         dividend, divisor);
            g_object_unref(dividend);
            g_object_unref(divisor);
            return sigp;
        }
    case OP_STRING:
        return spectra_string_signal_new(cbuf, root->text);
    case OP_FUNCTION_INVOCATION:
        {
            GPtrArray *prereq = g_ptr_array_new_with_free_func(g_object_unref);

            /*******************************************************************
             **
             ** loop down the parser tree and accumulate inputs from
             ** EXPR_NODE_CLASS_ARGUMENT_LIST and
             ** EXPR_NODE_CLASS_COMPLETE_ARGUMENT_LIST nodes into prereq array
             **
             ******************************************************************/
            struct expr_node *arg_node = root->prereq[0];
            if (arg_node->class != EXPR_NODE_CLASS_COMPLETE_ARGUMENT_LIST) {
                errx(1, "corrupt or incomplete parser tree; argument of "
                     "function %s is incomplete", root->ident);
            }

            while (arg_node) {
                struct expr_node *argp = arg_node->prereq[0];
                struct expr_node *next_arg_node = arg_node->prereq[1];

                if (arg_node->op != OP_ARGUMENT_LIST ||
                    (arg_node->class != EXPR_NODE_CLASS_COMPLETE_ARGUMENT_LIST &&
                     arg_node->class != EXPR_NODE_CLASS_ARGUMENT_LIST)) {
                    errx(1, "non-argument encountered in argument list");
                }

                g_ptr_array_add(prereq,
                                parser_convert_expr_node_to_signal(argp,
                                                                   cbuf,
                                                                   inputs,
                                                                   error));
                arg_node = next_arg_node;
            }
            SpectraSignal *sigp =
                spectra_function_signal_new_full(cbuf, root->ident, prereq);
            g_ptr_array_unref(prereq);
            return sigp;
        }
    default:
        errx(1, "OPERATION %02x NOT YET SUPPORTED", (unsigned)root->op);
    }
}

SpectraSignal *parser_get_signal(struct parser const *parser,
                                 SpectraConstantBuffer *cbuf,
                                 gchar const *inputs, GError **error) {
    g_return_val_if_fail(parser_complete(parser), NULL);
    return parser_convert_expr_node_to_signal(parser->expr->data,
                                              cbuf, inputs, error);
}
