/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#include <err.h>
#include <stdio.h>
#include <sndfile.h>
#include <math.h>

#include "signal.h"

#include "filecache.h"

#define SPECTRA_FILE_CACHE_ERROR (spectra_file_cache_error_quark())

G_DEFINE_QUARK (spectra-file-cache-error-quark, spectra_file_cache_error)

struct _SpectraFileCache {
    GObject parent;
    GData *cache;
};

SpectraFileCache *spectra_file_cache_new(void) {
    return g_object_new(SPECTRA_TYPE_FILE_CACHE, NULL);
}

G_DEFINE_FINAL_TYPE(SpectraFileCache, spectra_file_cache, G_TYPE_OBJECT)

static void spectra_file_cache_init(SpectraFileCache *cachep) {
    cachep->cache = NULL;
}

static void spectra_file_cache_finalize(GObject *gobj) {
    SpectraFileCache *cache = SPECTRA_FILE_CACHE(gobj);
    if (cache->cache)
        g_datalist_clear(&cache->cache);

    G_OBJECT_CLASS(spectra_file_cache_parent_class)->finalize(gobj);
}

static void spectra_file_cache_class_init(SpectraFileCacheClass *classp) {
    G_OBJECT_CLASS(classp)->finalize = spectra_file_cache_finalize;
}

static void spectra_file_cache_unref_channel(gpointer ptr) {
    g_bytes_unref(ptr);
}

struct _SpectraFile {
    GObject parent;
    GPtrArray *channels;
    spectra_scalar sample_frequency;
};

G_DEFINE_FINAL_TYPE(SpectraFile, spectra_file, G_TYPE_OBJECT)

static void spectra_file_finalize(GObject *gobj) {
    SpectraFile *file = SPECTRA_FILE(gobj);
    if (file->channels)
        g_ptr_array_unref(file->channels);

    G_OBJECT_CLASS(spectra_file_parent_class)->finalize(gobj);
}

static void spectra_file_init(SpectraFile *file) {
    file->channels = NULL;
    file->sample_frequency = NAN;
}

static void spectra_file_class_init(SpectraFileClass *class) {
    G_OBJECT_CLASS(class)->finalize = spectra_file_finalize;
}

static SpectraFile *spectra_file_new(gchar const *path, GError **err) {
    SF_INFO info;
    SNDFILE *infile = sf_open(path, SFM_READ, &info);

    if (!infile) {
        g_set_error(err, SPECTRA_FILE_CACHE_ERROR,
                    SPECTRA_FILE_CACHE_READ_FAILURE,
                    "failure to open \"%s\": %s", path, sf_strerror(NULL));
        return NULL;
    }

    //    sf_command(infile, SFC_SET_NORM_DOUBLE, NULL, SF_TRUE);

    double *interlaced = g_malloc_n(info.frames, info.channels * sizeof(double));
    sf_readf_double(infile, interlaced, info.frames);

    GPtrArray *channels_raw =
        g_ptr_array_new_with_free_func(spectra_file_cache_unref_channel);
    unsigned chan_index;
    for (chan_index = 0; chan_index < info.channels; chan_index++)
        g_ptr_array_add(channels_raw, g_malloc0_n(info.frames, sizeof(spectra_scalar)));

    unsigned frame;
    for (frame = 0; frame < info.frames; frame++) {
        double *in_frame = interlaced + frame * info.channels;

        // de-interlace channels
        for (chan_index = 0; chan_index < info.channels; chan_index++) {
            ((spectra_scalar*)g_ptr_array_index(channels_raw, chan_index))[frame] =
                in_frame[chan_index];
        }
    }

    // convert from raw array to GBytes
    for (chan_index = 0; chan_index < info.channels; chan_index++) {
        g_ptr_array_index(channels_raw, chan_index) =
            g_bytes_new_take(g_ptr_array_index(channels_raw, chan_index),
                             sizeof(spectra_scalar) * info.frames);
    }

    sf_close(infile);
    g_free(interlaced);

    SpectraFile *file = g_object_new(SPECTRA_TYPE_FILE, NULL);
    file->channels = channels_raw;
    file->sample_frequency = info.samplerate;
    return file;
}

static void spectra_file_cache_destroy_notify(gpointer ptr) {
    g_object_unref(ptr);
}

spectra_scalar spectra_file_sample_frequency(SpectraFile const *file) {
    return file->sample_frequency;
}

unsigned spectra_file_n_channels(SpectraFile const *file) {
    return file->channels->len;
}

GBytes *spectra_file_get_channel(SpectraFile const *file, unsigned ch) {
    g_return_val_if_fail(ch < file->channels->len, NULL);
    return g_ptr_array_index(file->channels, ch);
}

SpectraFile *spectra_file_cache_open(SpectraFileCache *cache,
                                     gchar const *path, GError **err) {
    SpectraFile *ret = g_datalist_get_data(&cache->cache, path);
    if (ret)
        return ret;

    ret = spectra_file_new(path, err);
    if (ret) {
        g_datalist_set_data_full(&cache->cache, path, ret,
                                 spectra_file_cache_destroy_notify);
        return ret;
    } else {
        return NULL;
    }
}

SpectraVector* spectra_file_sample(SpectraFile const *file,
                                   spectra_scalar time, unsigned ch) {
    if (ch >= spectra_file_n_channels(file))
        return spectra_real_new(0.0);
    GBytes *datp = spectra_file_get_channel(file, ch);
    spectra_scalar sfreq = spectra_file_sample_frequency(file);
    gsize sz;
    gconstpointer raw = g_bytes_get_data(datp, &sz);
    gsize n_samples = sz / sizeof(spectra_scalar);
    gint sample_index = time * sfreq;
    if (sample_index >= 0 && sample_index < n_samples) {
        spectra_scalar smp;
        memcpy(&smp, raw + sample_index * sizeof(spectra_scalar), sizeof(smp));
        return spectra_real_new(smp);
    } else {
        return spectra_real_new(0.0);
    }
}
