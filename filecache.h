/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef FILECACHE_H_
#define FILECACHE_H_

#include <glib.h>

#include "vector.h"

G_BEGIN_DECLS

typedef enum {
    SPECTRA_FILE_CACHE_READ_FAILURE
} SpectraFileCacheError;

#define SPECTRA_TYPE_FILE_CACHE (spectra_file_cache_get_type())
G_DECLARE_FINAL_TYPE(SpectraFileCache, spectra_file_cache,
                     SPECTRA, FILE_CACHE, GObject)

SpectraFileCache *spectra_file_cache_new(void);

struct spectra_file {
    GPtrArray *channels;
    spectra_scalar sample_frequency;
};

#define SPECTRA_TYPE_FILE (spectra_file_get_type())
G_DECLARE_FINAL_TYPE(SpectraFile, spectra_file, SPECTRA, FILE, GObject)

spectra_scalar spectra_file_sample_frequency(SpectraFile const *file);
unsigned spectra_file_n_channels(SpectraFile const *file);
GBytes *spectra_file_get_channel(SpectraFile const *file, unsigned ch);


/*******************************************************************************
 **
 ** sample a file from a given sample at a given time.
 **
 ** the SpectraVector returned will be a new reference and the user
 ** will need to call g_object_unref on it.
 **
 ******************************************************************************/
SpectraVector* spectra_file_sample(SpectraFile const *file,
                                   spectra_scalar time, unsigned ch);


SpectraFile *spectra_file_cache_open(SpectraFileCache *cache,
                                     gchar const *path, GError **err);

G_END_DECLS

#endif
