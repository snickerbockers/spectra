/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef TOKENS_H_
#define TOKENS_H_

#include <glib.h>

typedef enum {
    SPECTRA_TOKENIZER_BUFFER_OVERFLOW,
    SPECTRA_TOKENIZER_INVALID_TOKEN,
    SPECTRA_TOKENIZER_UNTERMINATED_ESCAPE
} SpectraTokenizerError;

/*
 * so the idea here is to make a simple "math language" so you can
 * writer whatever arbitrary signal you want and input it into
 * spectra.  This is probably not necessary and represents the
 * phoenomina known as "feature creep" but i'm having fun here.
 *
 * example statements:
 *    harmonic function:
 *        8 * sin(2 * pi * (2 * 3*t + 1/4)) + 9
 *    probably not useful for signal analysis but you can do it:
 *        t * t - 4
 *    gaussian bell curve:
 *        exp(1 - t * t)
 *    numeric integration:
 *        int{ 3 * t, 0, 3 }
 *    differential:
 *        TODO: come up with a syntax for this one, IDK
 *    load a signal from an audio file:
 *        load{ liveandlearn.ogg }
 *
 * high-level overview of syntax:
 *    t is the input value
 *    function call: func_name(arg1, arg2, ...)
 *    meta-operation: op{ stmt } (similar to function call but stmt
 *                    not be evaluated, ie its a function that
 *                    operates on another signal instead of its data)
 *
 * identifier: [^0-9][a-zA-Z0-9_]*
 * literal: [0-9]+.?[0-9]+
 * operator: '+-/*(){}'
 */

enum token_class {
    TOKEN_CLASS_INVALID,
    TOKEN_CLASS_WHITESPACE,

    TOKEN_CLASS_IDENTIFIER,
    TOKEN_CLASS_LITERAL,

    TOKEN_CLASS_STRING,

    TOKEN_CLASS_PLUS = '+',
    TOKEN_CLASS_MINUS = '-',
    TOKEN_CLASS_AST = '*',
    TOKEN_CLASS_SLASH = '/',
    TOKEN_CLASS_OPEN_PAREN = '(',
    TOKEN_CLASS_CLOSE_PAREN = ')',
    TOKEN_CLASS_OPEN_BRACE = '{',
    TOKEN_CLASS_CLOSE_BRACE = '}',
    TOKEN_CLASS_COMMA = ','
};

struct token;
struct tokenizer;

void tokenizer_init(struct tokenizer *tokenizer);
void tokenizer_cleanup(struct tokenizer *tokenizer);

int tokenizer_push(struct tokenizer *tokenizer, gchar ch, GError **error);

int tokenizer_finalize(struct tokenizer *tokenizer, GError **error);

void tokenizer_print_all(struct tokenizer *tokenizer);

struct tokenizer {
    GSList *tokens;
    gsize bufmax, bufnext;
    gchar *buf;
};

struct token {
    enum token_class class;
    gchar *text;
};

#endif
