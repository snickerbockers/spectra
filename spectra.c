/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#define _GNU_SOURCE

#include <err.h>
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <stdarg.h>
#include <stdbool.h>

#include <glib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "signal.h"
#include "audio.h"
#include "tokens.h"
#include "parser.h"
#include "program.h"
#include "functions.h"
#include "symbol.h"
#include "graphics.h"

#include "vector.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

static bool verbose_mode;

static void spectra_error(GError *err) {
    errx(1, "%s: %s", g_quark_to_string(err->domain), err->message);
}

static void log_verbose(char const *fmt, ...) {
    if (verbose_mode) {
        va_list args;
        va_start(args, fmt);
        vprintf(fmt, args);
        va_end(args);
    }
}

static int generate_signal(SpectraVector **outp, SpectraProgram *prog,
                            SpectraProgramContext *ctxt,
                            unsigned n_samples,
                            double sample_period, GError **error_in) {
    printf("evaluating signal over %u samples\n", n_samples);

    guint n_input = spectra_program_input_count(prog);
    if (n_input > 1) {
        guint input_index;
        for (input_index = 0; input_index < n_input; input_index++)
            warnx("input %u: %s", input_index,
                  spectra_program_input_name(prog, input_index));
        errx(1, "program signature has %u inputs (max: 1)", n_input);
    }

    GPtrArray *inputs = g_ptr_array_new_full(1, g_object_unref);
    if (n_input) {
        g_ptr_array_add(inputs, spectra_real_new(0.0));
    }
    GError *error = NULL;

    for (unsigned samp_idx = 0; samp_idx < n_samples; samp_idx++) {
        if (inputs->len) {
            g_ptr_array_remove_index(inputs, 0);
            g_ptr_array_add(inputs, spectra_real_new(samp_idx * sample_period));
        }
        *outp++ = spectra_program_execute(prog, ctxt, inputs, &error);
        if (error) {
            g_propagate_error(error_in, error);
            return -1;
        }
    }
    g_ptr_array_unref(inputs);
    return 0;
}

struct signal_spec {
    unsigned n_samples;
    double s_period;
    double n_harmonics;
};

enum signal_type {
    SPECTRA_SIGNAL_TYPE_REAL,
    SPECTRA_SIGNAL_TYPE_COMPLEX
};

struct signal_analysis {
    double min_phase, max_phase;
    double min_amp, max_amp;
    double min[2], max[2];
    double time;
    double s_freq;
    enum signal_type type;
};

/*******************************************************************************
 **
 ** picks tick-marks for the graph.  is independent of units.
 ** range[0] should be the beginning and range[1] should be the end
 **
 ******************************************************************************/
void pick_tick_interval(double *first,
                        double *increment,
                        double const range[2]) {
    /* double order = log10(fabs(range[1] - range[0])); */
    /* double rounded = exp10(floor(order)); */
    double inc = (range[1] - range[0]) / 10.0;

    if (range[1] > range[0])
        inc = fabs(inc);
    else
        inc = -fabs(inc);

    *first = floor(range[0] / inc) * inc;
    *increment = inc;
}

void analyze_signal(struct signal_analysis *stats,
                    struct signal_spec const *spec_p, SpectraVector **signal) {
    double max[2] = { DBL_MIN, DBL_MIN };
    double min[2] = { DBL_MAX, DBL_MAX };
    double time = 0.0;

    stats->type = SPECTRA_SIGNAL_TYPE_REAL;

    unsigned samp_idx;
    for (samp_idx = 0; samp_idx < spec_p->n_samples; samp_idx++) {
        SpectraVector *smp_real = spectra_vector_as_real(signal[samp_idx]);
        if (smp_real) {
            double smp = spectra_vector_index(smp_real, 0);
            if (!(isnan(smp) || isinf(smp))) {
                max[1] = smp > max[1] ? smp : max[1];
                min[1] = smp < min[1] ? smp : min[1];
                time += spec_p->s_period;
            }
            g_object_unref(smp_real);
        } else if (SPECTRA_VECTOR_IS_COMPLEX(signal[samp_idx])) {
            stats->type = SPECTRA_SIGNAL_TYPE_COMPLEX;
        } else {
            errx(1, "signal index %u is not real", samp_idx);
        }
    }

    min[0] = 0.0;
    max[0] = time;
    memcpy(stats->min, min, sizeof(stats->min));
    memcpy(stats->max, max, sizeof(stats->max));

    printf("MIN: (%f, %f) MAX: (%f, %f)\n", min[0], min[1], max[0], max[1]);

    stats->time = time;
    stats->s_freq = samp_idx / time;
}

/*
 * arrays of struct point_2d are used to render signals as sets of
 * line segments.  Every two consecutive points must form a segment.
 */
struct point_2d {
    spectra_scalar x, y;
};

static GArray *spectra_signal_plot_inner(SpectraVector **signal,
                                         unsigned signal_len,
                                         unsigned plot_len,
                                         spectra_scalar time_start,
                                         spectra_scalar signal_sample_period,
                                         spectra_scalar(*val_fn)(SpectraVector*)) {
    spectra_scalar sig_len_seconds = signal_sample_period * signal_len;
    spectra_scalar plot_sample_period = sig_len_seconds / plot_len;

    GArray *plot = g_array_new(FALSE, FALSE, sizeof(struct point_2d));
    unsigned index;
    for (index = 0; index < plot_len; index++) {
        spectra_scalar time_since_start = index * plot_sample_period;
        spectra_scalar time = time_start + time_since_start;
        struct point_2d pt = { .x = time };
        unsigned sig_idx = time_since_start / signal_sample_period;

        if (sig_idx >= signal_len)
            pt.y = 0.0;
        else
            pt.y = val_fn(signal[sig_idx]);
        g_array_append_val(plot, pt);
    }
    return plot;
}

static void spectra_plot_bounds(GArray *plot,
                                spectra_scalar min[2], spectra_scalar max[2]) {
    if (plot->len >= 2) {
        struct point_2d const *curs = &g_array_index(plot, struct point_2d, 0);
        min[0] = min[1] = DBL_MAX;
        max[0] = max[1] = -DBL_MAX;

        unsigned index;
        for (index = 0; index < plot->len; index++) {
            curs = &g_array_index(plot, struct point_2d, index);
            if (isfinite(curs->x) && isfinite(curs->y)) {
                if (curs->x < min[0])
                    min[0] = curs->x;
                if (curs->x > max[0])
                    max[0] = curs->x;
                if (curs->y < min[1])
                    min[1] = curs->y;
                if (curs->y > max[1])
                    max[1] = curs->y;
            }
        }
    } else if (plot->len == 1) {
        struct point_2d const *curs = &g_array_index(plot, struct point_2d, 0);
        min[0] = max[0] = curs->x;
        min[1] = max[1] = curs->y;
    } else {
        errx(1, "don't ask %s to calculate the range of an empty set, jackass!",
             G_STRFUNC);
    }
}

static GArray* spectra_signal_phase_plot(SpectraVector **signal,
                                         unsigned signal_len,
                                         unsigned plot_len,
                                         spectra_scalar time_start,
                                         spectra_scalar sample_period) {
    return spectra_signal_plot_inner(signal, signal_len, plot_len, time_start,
                                     sample_period, spectra_vector_phase);
}

static GArray* spectra_signal_mag_plot(SpectraVector **signal,
                                       unsigned signal_len,
                                       unsigned plot_len,
                                       spectra_scalar time_start,
                                       spectra_scalar sample_period) {
    return spectra_signal_plot_inner(signal, signal_len, plot_len, time_start,
                                     sample_period, spectra_vector_mag);
}

static GArray* spectra_signal_real_plot(SpectraVector **signal,
                                        unsigned signal_len,
                                        unsigned plot_len,
                                        spectra_scalar time_start,
                                        spectra_scalar sample_period) {
    return spectra_signal_plot_inner(signal, signal_len, plot_len, time_start,
                                     sample_period, spectra_vector_real);
}

static GArray* spectra_signal_imaginary_plot(SpectraVector **signal,
                                             unsigned signal_len,
                                             unsigned plot_len,
                                             spectra_scalar time_start,
                                             spectra_scalar sample_period) {
    return spectra_signal_plot_inner(signal, signal_len, plot_len, time_start,
                                     sample_period, spectra_vector_imaginary);
}

static void render_single_tick_locked(SDL_Surface *target,
                                      TTF_Font *font_p,
                                      Uint32 color,
                                      double const bounds[4],
                                      int const pos[2],
                                      int const dir[2],
                                      double const transform_to_world[9]) {
    // calculate endpoints
    int tick_mark_min_screen[2] = { pos[0] - dir[0], pos[1] - dir[1] };
    int tick_mark_max_screen[2] = { pos[0] + dir[0], pos[1] + dir[1] };

    // clamp endpoints
    if (tick_mark_min_screen[0] < bounds[0])
        tick_mark_min_screen[0] = bounds[0];
    if (tick_mark_min_screen[0] > bounds[1])
        tick_mark_min_screen[0] = bounds[1];
    if (tick_mark_min_screen[1] < bounds[2])
        tick_mark_min_screen[1] = bounds[2];
    if (tick_mark_min_screen[1] > bounds[3])
        tick_mark_min_screen[1] = bounds[3];
    if (tick_mark_max_screen[0] < bounds[0])
        tick_mark_max_screen[0] = bounds[0];
    if (tick_mark_max_screen[0] > bounds[1])
        tick_mark_max_screen[0] = bounds[1];
    if (tick_mark_max_screen[1] < bounds[2])
        tick_mark_max_screen[1] = bounds[2];
    if (tick_mark_max_screen[1] > bounds[3])
        tick_mark_max_screen[1] = bounds[3];

    double pos_dbl[2] = { pos[0], pos[1] };
    double pos_world[2];
    transform_vec_2d(pos_world, transform_to_world, pos_dbl);

    int ibounds[4] = { bounds[0], bounds[1], bounds[2], bounds[3] };
    draw_line_locked(target, tick_mark_min_screen, tick_mark_max_screen, ibounds, color);

    char text_tmp[64];
    int axis;
    if (abs(dir[1]) > abs(dir[0]))
        axis = 0;
    else
        axis = 1;

    /*
     * TODO: need to adjust precision for very small values so that we don't
     *       have adjacent tick-marks with the same printed value.  Consider
     *       exponential notation?
     */
    snprintf(text_tmp, sizeof(text_tmp), "%.02f", pos_world[axis]);
    SDL_Color white = { 0xff, 0xff, 0xff, 0xff };

    if (pos_dbl[0] >= bounds[0] && pos_dbl[0] <= bounds[1] &&
        pos_dbl[1] >= bounds[2] && pos_dbl[1] <= bounds[3]) {
        int shut_up_warnings[2] = { pos[0], pos[1] };
        blit_text_center_justified_locked(target, font_p, text_tmp, shut_up_warnings,
                                          white);
    }

}

/*******************************************************************************
 **
 ** this function accepts arguments in screen coordinates; as a consequence,
 ** lesser valus (in screen coordinates) are actually higher than greater
 ** values.
 **
 ******************************************************************************/
static void
connect_plot_pixels(SDL_Surface *target, int const bounds[4],
                    Uint32 color, int pt_left[2], int pt_right_height) {
    int line_y[2], line_x;

    //int pt_right[2] = { pt_left[0] + 1, pt_right_height };
    if (pt_left[1] < pt_right_height) {
        /*
         * left-most pixel is higher than rightmost pixel, so draw a vertical
         * line beneath the left-most pixel.
         */
        line_x = pt_left[0];
        line_y[0] = pt_left[1];
        line_y[1] = pt_right_height;
    } else if (pt_left[1] > pt_right_height) {
        /*
         * right-most pixel is higher than leftmost pixel, so draw a vertical
         * line beneath the right-most pixel.
         */
        line_x = pt_left[0] + 1;
        line_y[0] = pt_right_height;
        line_y[1] = pt_left[1];
    } else {
        return; // equal heights; no need to connect
    }

    if (line_y[0] < bounds[2])
        line_y[0] = bounds[2];
    if (line_y[0] > bounds[3])
        line_y[0] = bounds[3];
    if (line_y[1] < bounds[2])
        line_y[1] = bounds[2];
    if (line_y[1] > bounds[3])
        line_y[1] = bounds[3];

    if (line_x >= bounds[0] && line_x <= bounds[1]) {
        int pos;
        for (pos = line_y[0]; pos <= line_y[1]; pos++) {
            set_px_locked(target, line_x, pos, color);
        }
    }
}

struct point_2d sample_plot(GArray *plot, spectra_scalar time) {
    guint index;
    for (index = 0; index < plot->len; index++) {
        struct point_2d pt = g_array_index(plot, struct point_2d, index);
        if (index >= 1) {
            struct point_2d prev = g_array_index(plot, struct point_2d, index - 1);
            if (prev.x <= time && pt.x >= time) {
                struct point_2d ret = { .x = time, .y = prev.y };
                return ret;
            }
        }
    }
    struct point_2d nothing = { .x = time, .y = 0.0 };
    return nothing;
}

static void do_render_plot(SDL_Surface *target,
                           TTF_Font *font_p,
                           GArray *plot,
                           double time_start, double sample_period,
                           double const bounds[4],
                           double const plot_bounds[4],
                           double const transform_to_screen[9],
                           double const transform_to_world[9]) {
    int ibounds[4] = { bounds[0], bounds[1], bounds[2], bounds[3] };
    if (!plot->len)
        return;
    if (SDL_LockSurface(target) == 0) {
        double world_origin[2] = { 0.0, 0.0 };
        double world_origin_screen[2];
        transform_vec_2d(world_origin_screen, transform_to_screen, world_origin);
        int zero_point[2] = { world_origin_screen[0], world_origin_screen[1] };

        printf("zero_point: (%d, %d)\n", zero_point[0], zero_point[1]);

        char *dstp = target->pixels;
        unsigned pix_sz = target->format->BytesPerPixel;
        Uint32 sig_px = SDL_MapRGB(target->format, 0xff, 0xff, 0xff);
        unsigned row, col, pt_idx;

        // black backdrop
        //        memset(dstp, 0x0, pix_sz * target->w * target->h);

        /***********************************************************************
         **
         ** render axes
         **
         **********************************************************************/
        int axes[2][2][2] = {
            //x-axis
            {
                { bounds[0], world_origin_screen[1] },
                { bounds[1], world_origin_screen[1] }
            },
            // y-axis
            {
                { world_origin_screen[0], bounds[2] },
                { world_origin_screen[0], bounds[3] }
            }
        };

        if (world_origin_screen[1] >= bounds[2] &&
            world_origin_screen[1] <= bounds[3]) {
            draw_line_locked(target, axes[0][0], axes[0][1], ibounds,
                             SDL_MapRGB(target->format, 0xff, 0x00, 0x00));
        }
        if (world_origin_screen[0] >= bounds[0] &&
            world_origin_screen[0] <= bounds[1]) {
            draw_line_locked(target, axes[1][0], axes[1][1], ibounds,
                             SDL_MapRGB(target->format, 0xff, 0x00, 0x00));
        }

        /***********************************************************************
         **
         ** render tick-marks along the x-axis every 50 pixels
         **
         **********************************************************************/
        int tick_mark_screen;
        for (tick_mark_screen = world_origin_screen[0];
             tick_mark_screen < bounds[1]; tick_mark_screen += 50) {
            int tick_mark_center_screen[2] = {
                tick_mark_screen, world_origin_screen[1]
            };
            int dir[2] = { 0, 8 };
            render_single_tick_locked(target, font_p, SDL_MapRGB(target->format, 0xff, 0x00, 0x00),
                                      bounds, tick_mark_center_screen, dir, transform_to_world);
        }

        for (tick_mark_screen = world_origin_screen[0];
             tick_mark_screen >= bounds[0]; tick_mark_screen -= 50) {
            int tick_mark_center_screen[2] = {
                tick_mark_screen, world_origin_screen[1]
            };
            int dir[2] = { 0, 8 };
            render_single_tick_locked(target, font_p, SDL_MapRGB(target->format, 0xff, 0x00, 0x00),
                                      bounds, tick_mark_center_screen, dir, transform_to_world);
        }

        /***********************************************************************
         **
         ** render tick-marks along the y-axis every 50 pixels
         **
         **********************************************************************/
        for (tick_mark_screen = world_origin_screen[0];
             tick_mark_screen < bounds[1]; tick_mark_screen += 50) {
            int tick_mark_center_screen[2] = {
                world_origin_screen[0], tick_mark_screen
            };
            int dir[2] = { 8, 0 };
            render_single_tick_locked(target, font_p, SDL_MapRGB(target->format, 0xff, 0x00, 0x00),
                                      bounds, tick_mark_center_screen, dir, transform_to_world);
        }

        for (tick_mark_screen = world_origin_screen[0];
             tick_mark_screen >= bounds[0]; tick_mark_screen -= 50) {
            int tick_mark_center_screen[2] = {
                world_origin_screen[0], tick_mark_screen
            };
            int dir[2] = { 8, 0 };
            render_single_tick_locked(target, font_p, SDL_MapRGB(target->format, 0xff, 0x00, 0x00),
                                      bounds, tick_mark_center_screen, dir, transform_to_world);
        }

        if (plot->len >= 1) {
            int last_px_screen[2] = { -1, -1 };
            int i_coords[2];
            for (pt_idx = ibounds[0]; pt_idx < ibounds[1];
                 pt_idx++, last_px_screen[0] = i_coords[0],
                     last_px_screen[1] = i_coords[1]) {

                double pt_xaxis[2] = { pt_idx, zero_point[1] };
                double pt_xaxis_world[2];
                transform_vec_2d(pt_xaxis_world, transform_to_world, pt_xaxis);

                struct point_2d pt = sample_plot(plot, pt_xaxis_world[0]);
                if (isfinite(pt.x) && isfinite(pt.y)) {
                    spectra_scalar world_coords[2] = {
                        pt.x, pt.y
                    };

                    spectra_scalar screen_coords[2];
                    transform_vec_2d(screen_coords, transform_to_screen, world_coords);

                    i_coords[0] = pt_idx;
                    i_coords[1] = screen_coords[1];
                    connect_plot_pixels(target, ibounds, sig_px, last_px_screen, i_coords[1]);
                    set_px_locked_bounds(target, i_coords[0], i_coords[1], ibounds, sig_px);
                } else {
                    i_coords[0] = -1;
                    i_coords[1] = -1;
                }
            }
        }

        SDL_UnlockSurface(target);
    } else {
        warnx("failrue to lock backbuffer for write access");
    }
}

static void
render_plot_generic(SDL_Surface *target,
                    TTF_Font *font_p,
                    SpectraVector **signal,
                    unsigned signal_len,
                    double const screen_bounds[4],
                    spectra_scalar sample_period,
                    char const *title,
                    GArray*(*plot_gen_fn)(SpectraVector **,
                                          unsigned,
                                          unsigned,
                                          spectra_scalar,
                                          spectra_scalar)) {
    SDL_Color text_color = { 0xff, 0xff, 0xff, 0xff };
    double to_world[9], to_screen[9];
    double adjusted_bounds[4] = {
        screen_bounds[0] + 1, screen_bounds[1] - 1,
        screen_bounds[2] + 17, screen_bounds[3] - 1
    };

    GArray *plot = plot_gen_fn(signal, signal_len,
                               adjusted_bounds[1] - adjusted_bounds[0] + 1,
                               0.0, sample_period);
    spectra_scalar plot_bounds[4];
    spectra_plot_bounds(plot, plot_bounds, plot_bounds + 2);

    if (isfinite(plot_bounds[0]) && isfinite(plot_bounds[1]) &&
        isfinite(plot_bounds[2]) && isfinite(plot_bounds[3]) &&
        plot_bounds[0] != DBL_MAX && plot_bounds[0] != -DBL_MAX &&
        plot_bounds[1] != DBL_MAX && plot_bounds[1] != -DBL_MAX &&
        plot_bounds[2] != DBL_MAX && plot_bounds[2] != -DBL_MAX &&
        plot_bounds[3] != DBL_MAX && plot_bounds[3] != -DBL_MAX) {
        double plot_range[2] = {
            plot_bounds[2] - plot_bounds[0],
            plot_bounds[3] - plot_bounds[1]
        };

        /***********************************************************************
         **
         ** force an arbitrary minimum range of 0.2 * plot_bounds[1] if the
         ** y-coordinate boundaries are flat (horizontal line).  If
         ** fabs(plot_range[1]) is too small, it won't render correctly because
         ** every row will have the exact same value.
         **
         ** TODO: 1.0/32.0 is a placeholder value, it probably can and should be a
         ** lot smaller than this
         **
         **********************************************************************/
        if (fabs(plot_range[1]) < 0.001) {
            plot_bounds[3] += 0.01;
            plot_bounds[1] -= 0.001;
            plot_range[1] = plot_bounds[3] - plot_bounds[1];
        }

        /***********************************************************************
         **
         ** if plot bounds do not include the x-axis then expand the
         ** vertical range.
         **
         **********************************************************************/
        if (!(plot_bounds[3] > 0 && plot_bounds[1] < 0)) {
            double old_plot_bounds = plot_bounds[1];
            plot_bounds[1] = -0.1 * plot_bounds[3];
            printf("plot_bounds[1] adjusted from %f to %f\n", old_plot_bounds, plot_bounds[1]);
        }

        /*
         * expand max a little for the sake of "flat" plots so the
         * line doesn't get cut off by the frame
         */
        plot_bounds[3] *= 1.1;

        double plot_world[4] = {
            plot_bounds[0] - 0.1 * plot_range[0],
            plot_bounds[2] + 0.1 * plot_range[0],
            plot_bounds[1] - 0.1 * plot_range[1],
            plot_bounds[3] + 0.1 * plot_range[1]
        };

        log_verbose("plot bounds: { %f, %f, %f, %f }\n",
                    plot_bounds[0], plot_bounds[1],
                    plot_bounds[2], plot_bounds[3]);
        log_verbose("world->screen window: { %f, %f, %f, %f }\n",
                    plot_world[0], plot_world[1],
                    plot_world[2], plot_world[3]);
        screen_to_world_matrix_full(to_world, plot_world, adjusted_bounds);
        world_to_screen_matrix_full(to_screen, plot_world, adjusted_bounds);

        do_render_plot(target, font_p, plot, 0.0, sample_period,
                       adjusted_bounds, plot_bounds, to_screen, to_world);

        double midpoint[2] = {
            (screen_bounds[0] + screen_bounds[1]) * 0.5,
            (screen_bounds[2] + screen_bounds[3]) * 0.5
        };

        unsigned text_pos[2] = { midpoint[0], screen_bounds[2] };

        blit_text_center_justified_locked(target, font_p,
                                          title, text_pos, text_color);
    } else {
        unsigned text_pos[2] = {
            (adjusted_bounds[0] + adjusted_bounds[1]) * 0.5,
            (adjusted_bounds[2] + adjusted_bounds[3]) * 0.5
        };
        blit_text_center_justified_locked(target, font_p,
                                          "ERROR - ALL FUNCTION VALUES ARE "
                                          "INVALID OR OUT-OF-RANGE",
                                          text_pos, text_color);
    }

    // draw the frame
    int frame[][2] = {
        { adjusted_bounds[0], adjusted_bounds[2] },
        { adjusted_bounds[0], adjusted_bounds[3] },
        { adjusted_bounds[1], adjusted_bounds[3] },
        { adjusted_bounds[1], adjusted_bounds[2] }
    };

    SDL_LockSurface(target);
    int ibounds[4] = { screen_bounds[0], screen_bounds[1], screen_bounds[2], screen_bounds[3] };
    draw_line_locked(target, frame[0], frame[1], ibounds,
                     SDL_MapRGBA(target->format, 0x00, 0x00, 0xff, 0xff));
    draw_line_locked(target, frame[1], frame[2], ibounds,
                     SDL_MapRGBA(target->format, 0x00, 0x00, 0xff, 0xff));
    draw_line_locked(target, frame[2], frame[3], ibounds,
                     SDL_MapRGBA(target->format, 0x00, 0x00, 0xff, 0xff));
    draw_line_locked(target, frame[3], frame[0], ibounds,
                     SDL_MapRGBA(target->format, 0x00, 0x00, 0xff, 0xff));
    SDL_UnlockSurface(target);

    g_array_unref(plot);
}
                              
static void render_real_plot(SDL_Surface *target,
                             TTF_Font *font_p,
                             SpectraVector **signal,
                             unsigned signal_len,
                             double const screen_bounds[4],
                             spectra_scalar sample_period) {
    render_plot_generic(target, font_p, signal, signal_len, screen_bounds,
                        sample_period, "real", spectra_signal_real_plot);
}

static void render_imaginary_plot(SDL_Surface *target,
                                  TTF_Font *font_p,
                                  SpectraVector **signal,
                                  unsigned signal_len,
                                  double const screen_bounds[4],
                                  spectra_scalar sample_period) {
    render_plot_generic(target, font_p, signal, signal_len, screen_bounds,
                        sample_period, "imaginary", spectra_signal_imaginary_plot);
}

static void render_mag_plot(SDL_Surface *target,
                             TTF_Font *font_p,
                             SpectraVector **signal,
                             unsigned signal_len,
                             double const screen_bounds[4],
                             spectra_scalar sample_period) {
    render_plot_generic(target, font_p, signal, signal_len, screen_bounds,
                        sample_period, "magnitude", spectra_signal_mag_plot);
}

static void render_phase_plot(SDL_Surface *target,
                             TTF_Font *font_p,
                             SpectraVector **signal,
                             unsigned signal_len,
                             double const screen_bounds[4],
                             spectra_scalar sample_period) {
    render_plot_generic(target, font_p, signal, signal_len, screen_bounds,
                        sample_period, "phase", spectra_signal_phase_plot);
}

static void refresh_window(SDL_Window *winp, SDL_Surface *back_surf) {
    SDL_Surface *win_surf = SDL_GetWindowSurface(winp);
    if (win_surf) {
        // copy data
        SDL_BlitSurface(back_surf, NULL, win_surf, NULL);
        SDL_UpdateWindowSurface(winp);
    } else {
        warnx("failure to copy backbuffer to window surface");
    }
}

static GString* spectra_real_to_string(SpectraVector *vec) {
    GString *ret = g_string_new("");
    g_string_printf(ret, "%f", spectra_vector_index(vec, 0));
    return ret;
}

SpectraVector* spectra_real_add(SpectraVector *lhs, SpectraVector *rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_REAL(lhs), NULL);

    SpectraVector *rhs_as_real = spectra_vector_as_real(rhs);
    spectra_scalar self = spectra_vector_index(lhs, 0);
    if (rhs_as_real) {
        SpectraVector *ret =
            spectra_vector_new(SPECTRA_TYPE_REAL,
                               self +
                               spectra_vector_index(rhs_as_real, 0));
        g_object_unref(rhs_as_real);
        return ret;
    } else if (SPECTRA_VECTOR_IS_COMPLEX(rhs)) {
        return spectra_complex_new(self + spectra_vector_index(rhs, 0),
                                   spectra_vector_index(rhs, 1));
    } else
        return NULL; // unimplemented
}

SpectraVector* spectra_real_sub(SpectraVector *lhs, SpectraVector *rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_REAL(lhs), NULL);

    SpectraVector *rhs_as_real = spectra_vector_as_real(rhs);
    spectra_scalar self = spectra_vector_index(lhs, 0);
    if (rhs_as_real) {
        SpectraVector *ret =
            spectra_vector_new(SPECTRA_TYPE_REAL,
                               self -
                               spectra_vector_index(rhs_as_real, 0));
        g_object_unref(rhs_as_real);
        return ret;
    } else if (SPECTRA_VECTOR_IS_COMPLEX(rhs)) {
        return spectra_complex_new(self - spectra_vector_index(rhs, 0),
                                   -spectra_vector_index(rhs, 1));
    } else
        return NULL; // unimplemented
}

SpectraVector* spectra_real_mul(SpectraVector *lhs, SpectraVector *rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_REAL(lhs), NULL);

    SpectraVector *rhs_as_real = spectra_vector_as_real(rhs);
    spectra_scalar self = spectra_vector_index(lhs, 0);
    if (rhs_as_real) {
        SpectraVector *ret =
            spectra_vector_new(SPECTRA_TYPE_REAL,
                               self *
                               spectra_vector_index(rhs_as_real, 0));
        g_object_unref(rhs_as_real);
        return ret;
    } else if (SPECTRA_VECTOR_IS_COMPLEX(rhs)) {
        return spectra_complex_new(self * spectra_vector_index(rhs, 0),
                                   self * spectra_vector_index(rhs, 1));
    } else
        return NULL; // unimplemented
}

SpectraVector* spectra_real_div(SpectraVector *lhs, SpectraVector *rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_REAL(lhs), NULL);

    SpectraVector *rhs_as_real = spectra_vector_as_real(rhs);
    if (rhs_as_real) {
        SpectraVector *ret =
            spectra_vector_new(SPECTRA_TYPE_REAL,
                               spectra_vector_index(lhs, 0) /
                               spectra_vector_index(rhs_as_real, 0));
        g_object_unref(rhs_as_real);
        return ret;
    } else if (SPECTRA_VECTOR_IS_COMPLEX(rhs)) {
        spectra_scalar rhs_mag = spectra_vector_mag(rhs);
        spectra_scalar rhs_angle = spectra_vector_phase(rhs);
        spectra_scalar res_mag = spectra_vector_index(lhs, 0) / rhs_mag;
        spectra_scalar res_angle = -rhs_angle;
        return spectra_complex_new(res_mag * cos(res_angle), res_mag * sin(res_angle));
    } else
        return NULL; // unimplemented
}

SpectraVector *spectra_real_scale(SpectraVector *lhs, spectra_scalar rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_REAL(lhs), NULL);
    return spectra_vector_new(SPECTRA_TYPE_REAL, spectra_vector_index(lhs, 0) * rhs);
}

SpectraVector *spectra_real_exp(SpectraVector *logarithm) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_REAL(logarithm), NULL);
    return spectra_real_new(exp(spectra_vector_index(logarithm, 0)));
}

SpectraVector *spectra_real_ln(SpectraVector *logarithm) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_REAL(logarithm), NULL);
    return spectra_real_new(log(spectra_vector_index(logarithm, 0)));
}

SpectraVector *spectra_real_as_real(SpectraVector *vecp) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_REAL(vecp), NULL);
    return spectra_real_new(spectra_vector_index(vecp, 0));
}

static SpectraVector *spectra_complex_as_real(SpectraVector *vecp) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(vecp), NULL);

    /*
     * TODO: allow for vectors where
     * fabs(spectra_vector_phase(vecp)) < TOLERANCE to be converted to real
     * numbers.
     */
    if (spectra_vector_index(vecp, 1) == 0.0)
        return spectra_real_new(spectra_vector_index(vecp, 0));
    return NULL;
}

static GString *spectra_complex_to_string(SpectraVector *vecp) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(vecp), NULL);
    GString *ret = g_string_new("");
    spectra_scalar real = spectra_vector_index(vecp, 0);
    spectra_scalar complex = spectra_vector_index(vecp, 1);
    if (real != 0.0 && complex != 0.0)
        g_string_printf(ret, "%f + j*%f", real, complex);
    else if (real != 0.0)
        g_string_printf(ret, "%f", real);
    else if (complex != 0.0)
        g_string_printf(ret, "j*%f", complex);
    else
        g_string_printf(ret, "0.0");
    return ret;
}

SpectraVector *spectra_complex_add(SpectraVector *lhs, SpectraVector *rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(lhs), NULL);

    spectra_scalar self[2] = {
        spectra_vector_index(lhs, 0),
        spectra_vector_index(lhs, 1)
    };
    spectra_scalar other[2];

    if (SPECTRA_VECTOR_IS_COMPLEX(rhs)) {
        other[0] = spectra_vector_index(rhs, 0);
        other[1] = spectra_vector_index(rhs, 1);

    } else if (SPECTRA_VECTOR_IS_REAL(rhs)) {
        other[0] = spectra_vector_index(rhs, 0);
        other[1] = 0.0;
    } else {
        return NULL;
    }

    return spectra_complex_new(self[0] + other[0], self[1] + other[1]);
}

SpectraVector *spectra_complex_sub(SpectraVector *lhs, SpectraVector *rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(lhs), NULL);

    spectra_scalar self[2] = {
        spectra_vector_index(lhs, 0),
        spectra_vector_index(lhs, 1)
    };
    spectra_scalar other[2];

    if (SPECTRA_VECTOR_IS_COMPLEX(rhs)) {
        other[0] = spectra_vector_index(rhs, 0);
        other[1] = spectra_vector_index(rhs, 1);

    } else if (SPECTRA_VECTOR_IS_REAL(rhs)) {
        other[0] = spectra_vector_index(rhs, 0);
        other[1] = 0.0;
    } else {
        return NULL;
    }

    return spectra_complex_new(self[0] - other[0], self[1] - other[1]);
}

SpectraVector *spectra_complex_mul(SpectraVector *lhs, SpectraVector *rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(lhs), NULL);

    spectra_scalar self[2] = {
        spectra_vector_index(lhs, 0),
        spectra_vector_index(lhs, 1)
    };
    spectra_scalar other[2];

    if (SPECTRA_VECTOR_IS_COMPLEX(rhs)) {
        other[0] = spectra_vector_index(rhs, 0);
        other[1] = spectra_vector_index(rhs, 1);

    } else if (SPECTRA_VECTOR_IS_REAL(rhs)) {
        other[0] = spectra_vector_index(rhs, 0);
        other[1] = 0.0;
    } else {
        return NULL;
    }

    return spectra_complex_new(self[0] * other[0] - self[1] * other[1],
                               self[0] * other[1] + self[1] * other[0]);
}

SpectraVector *spectra_complex_div(SpectraVector *lhs, SpectraVector *rhs) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(lhs), NULL);

    double lhs_mag = spectra_vector_mag(lhs);
    double lhs_phase = spectra_vector_phase(lhs);
    double rhs_mag = spectra_vector_mag(rhs);
    double rhs_phase;

    if (SPECTRA_VECTOR_IS_COMPLEX(rhs))
        rhs_phase = spectra_vector_phase(rhs);
    else if (SPECTRA_VECTOR_IS_REAL(rhs))
        rhs_phase = 0.0;
    else
        return NULL;

    double result_phase = lhs_phase - rhs_phase;
    double result_mag = lhs_mag / rhs_mag;
    return spectra_complex_new(result_mag * cos(result_phase),
                               result_mag * sin(result_phase));
}

SpectraVector *spectra_complex_scale(SpectraVector *vecp,
                                     spectra_scalar scale) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(vecp), NULL);
    return spectra_complex_new(spectra_vector_index(vecp, 0) * scale,
                               spectra_vector_index(vecp, 1) * scale);
}

SpectraVector* spectra_complex_exp(SpectraVector *logarithm) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(logarithm), NULL);

    spectra_scalar real = spectra_vector_real(logarithm);
    spectra_scalar imag = spectra_vector_imaginary(logarithm);

    spectra_scalar mag = exp(real);
    return spectra_complex_new(mag * cos(imag), mag * sin(imag));
}

SpectraVector* spectra_complex_ln(SpectraVector *value) {
    g_return_val_if_fail(SPECTRA_VECTOR_IS_COMPLEX(value), NULL);

    spectra_scalar in_mag = spectra_vector_mag(value);
    spectra_scalar in_phase = spectra_vector_phase(value);

    return spectra_complex_new(log(in_mag), in_phase);
}

int main(int argc, char **argv) {
    double s_freq = 44100;
    int ch;
    double n_seconds = 1.0;
    unsigned n_harmonics = 10;
    unsigned win_sz = 44100;
    static bool n_harmonics_override = false;
    SpectraSignal *sigp = NULL;
    char *prog_txt = NULL;
    bool play_audio = false;
    GError *error = NULL;
    gchar const *independent_variable = "t";

    /***************************************************************************
     **
     ** instantiate vector types
     **
     **************************************************************************/
    struct spectra_vector_vtable real_vtable = {
        .to_str = spectra_real_to_string,
        .add = spectra_real_add,
        .sub = spectra_real_sub,
        .mult = spectra_real_mul,
        .div = spectra_real_div,
        .scale = spectra_real_scale,
        .as_real = spectra_real_as_real,
        .exp = spectra_real_exp,
        .ln = spectra_real_ln
    }, complex_vtable = {
        .to_str = spectra_complex_to_string,
        .add = spectra_complex_add,
        .sub = spectra_complex_sub,
        .mult = spectra_complex_mul,
        .div = spectra_complex_div,
        .scale = spectra_complex_scale,
        .as_real = spectra_complex_as_real,
        .exp = spectra_complex_exp,
        .ln = spectra_complex_ln
    };
    spectra_vector_type("spectra_real", real_vtable, "1", NULL);
    spectra_vector_type("spectra_complex", complex_vtable, "1", "j", NULL);

    bool mag_phase_force_enable = false;
    FILE *outfile = NULL;

    while ((ch = getopt(argc, argv, "i:s:l:h:ve:apro:")) != -1) {
        switch (ch) {
        case 'v':
            verbose_mode = true;
            break;
        case 's':
            errno = 0;
            s_freq = strtod(optarg, NULL);
            if (errno)
                err(1, "failure to parse sampling frequency argument");
            break;
        case 'l':
            errno = 0;
            n_seconds = strtod(optarg, NULL);
            if (errno)
                err(1, "failure to parse length argument");
            break;
        case 'h':
            errno = 0;
            n_harmonics = strtoul(optarg, NULL, 0);
            n_harmonics_override = true;
            if (errno)
                err(1, "failure to parse harmonic-count argument");
            break;
        case 'e':
            prog_txt = optarg;
            break;
        case 'p':
            play_audio = true;
            break;
        case 'i':
            independent_variable = optarg;
            break;
        case 'r':
            /*
             * render all signals as complex magnitude/phase pairs even if they
             * re real
             */
            mag_phase_force_enable = true;
            break;
        case 'o':
            outfile = fopen(optarg, "w");
            if (!outfile)
                err(1, "failure to open %s\n", optarg);
            break;
        default:
            errx(1, "bad arg %c", ch);
        }
    }

    if (strchr(independent_variable, ','))
        g_warning("multivariate functions not supported (for now)");

    argc -= optind;
    argv += optind;

    if (argc != 1)
        errx(1, "missing program");

    time_t compile_start, compile_end;
    time(&compile_start);
    printf("program compilation begins at %s\n", asctime(localtime(&compile_start)));

    struct parser parser;
    struct tokenizer tokenizer;
    tokenizer_init(&tokenizer);
    parser_init(&parser);

    {
        char const *ch = *argv;
        while (*ch)
            if (tokenizer_push(&tokenizer, *ch++, &error) != 0)
                spectra_error(error);
    }
    if (tokenizer_finalize(&tokenizer, &error) != 0) {
        spectra_error(error);
    }

    if (parser_parse(&parser, &tokenizer, &error) != 0)
        spectra_error(error);
    if (!parser_complete(&parser))
        errx(1, "failure to parse");

    {
        SpectraConstantBuffer *cbuf = spectra_constant_buffer_new();
        SpectraVector *pi_vecp = spectra_real_new(M_PI);
        SpectraVector *e_vecp = spectra_real_new(M_E);
        SpectraVector *j_vecp = spectra_complex_new(0.0, 1.0);

        spectra_constant_buffer_define(cbuf,
                                       "pi", SPECTRA_CONSTANT_TYPE_VECTOR, pi_vecp,
                                       "e", SPECTRA_CONSTANT_TYPE_VECTOR, e_vecp,
                                       "j", SPECTRA_CONSTANT_TYPE_VECTOR, j_vecp,

                                       "floor", SPECTRA_CONSTANT_TYPE_FUNCTION,
                                       UNARY_WRAPPER(floor),
                                       "ceil", SPECTRA_CONSTANT_TYPE_FUNCTION,
                                       UNARY_WRAPPER(ceil),
                                       "sqrt", SPECTRA_CONSTANT_TYPE_FUNCTION,
                                       UNARY_WRAPPER(sqrt),
                                       "mag", SPECTRA_CONSTANT_TYPE_FUNCTION,
                                       spectra_mag,
                                       "print", SPECTRA_CONSTANT_TYPE_FUNCTION,
                                       spectra_print,
                                       "sample", SPECTRA_CONSTANT_TYPE_FUNCTION,
                                       spectra_sample,
                                       NULL);
        sigp = parser_get_signal(&parser, cbuf, independent_variable, &error);
        g_object_unref(j_vecp);
        g_object_unref(e_vecp);
        g_object_unref(pi_vecp);
        g_object_unref(cbuf);
    }

    if (!SPECTRA_IS_SIGNAL(sigp))
        spectra_error(error);

    parser_cleanup(&parser);
    tokenizer_cleanup(&tokenizer);

    SpectraProgram *test_prog = spectra_signal_compile(sigp);
    time(&compile_end);
    printf("program compilation ends at %s\n", asctime(localtime(&compile_end)));
    if (!test_prog)
        errx(1, "failure to compile");

    double compile_time = difftime(compile_end, compile_start);

    printf("program was compiled in %f seconds\n", compile_time);

    guint output_addr = spectra_program_output_index(test_prog);

    printf("****** The output address of hte compiled program is %08x\n", output_addr);

    if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS))
        errx(1, "failure to init SDL: %s", SDL_GetError());

    if (TTF_Init() != 0)
        errx(1, "failure to init SDL_TTF: %s", TTF_GetError());

    char const *font_path = choose_font_path();

    printf("loading font %s\n", font_path);
    TTF_Font *font_p = TTF_OpenFont(font_path, 12);
    if (!font_p)
        warnx("unable to open font %s\n", font_path);

    // create 1-second recording
    unsigned total_n_samples = s_freq * n_seconds;
    SpectraVector **samples = g_malloc0_n(total_n_samples, sizeof(SpectraVector*));
    double sample_period = 1.0 / s_freq;

    if (verbose_mode)
        spectra_program_print(test_prog);

    SpectraProgramContext *ctxt = spectra_program_context_new(test_prog);
    if (generate_signal(samples, test_prog, ctxt,
                        total_n_samples, sample_period, &error) != 0) {
        spectra_error(error);
    }

    GString *sig_as_str = spectra_signal_to_string(sigp);
    printf("SIGNAL UNDER ANALYSIS: \"%s\"\n", sig_as_str->str);
    g_string_free(sig_as_str, TRUE);
    sig_as_str = NULL;

    g_object_unref(G_OBJECT(sigp));
    g_object_unref(G_OBJECT(ctxt));
    sigp = NULL;

    struct signal_spec spec = {
        .n_samples = total_n_samples,
        .s_period = sample_period,
        .n_harmonics = n_harmonics_override ? n_harmonics : total_n_samples
    };

    struct signal_analysis stats;
    analyze_signal(&stats, &spec, samples);

    printf("SIGNAL ANALYSIS:\n");
    printf("INPUT RANGE: [%f, %f]\n", stats.min[0], stats.max[0]);
    printf("OUTPUT RANGE: [%f, %f]\n", stats.min[1], stats.max[1]);
    printf("%u samples over %f seconds\n", total_n_samples, stats.time);
    printf("sampling frequency: %f\n", stats.s_freq);

    /*
     * if the signal has a rate-of-change of 0 then set the output-interval to 1
     */
    if (fabs(stats.max[1] - stats.min[1]) <= DBL_EPSILON) {
        double center = 0.5 * (stats.max[1] + stats.min[1]);
        stats.min[1] = center - 0.5;
        stats.max[1] = center + 0.5;
    }

    SDL_Window *winp = SDL_CreateWindow("spectra", SDL_WINDOWPOS_UNDEFINED,
                                        SDL_WINDOWPOS_UNDEFINED,
                                        WIN_WIDTH, WIN_HEIGHT, 0);
    if (!winp)
        errx(1, "failure to create SDL window");

    Uint32 pixfmt = SDL_GetWindowPixelFormat(winp);
    if (pixfmt == SDL_PIXELFORMAT_UNKNOWN)
        warnx("failure obtaining window pixel format");
    SDL_Surface *back_surf = SDL_CreateRGBSurfaceWithFormat(0, WIN_WIDTH, WIN_HEIGHT, 32, pixfmt);
    if (!back_surf)
        errx(1, "failure to create SDL backbuffer surface");

    double range = stats.max[1] - stats.min[1];
    double to_world[9], to_screen[9];

    clear_screen(back_surf);
    if (mag_phase_force_enable) {
        double mag_bounds[4] = {
            0.0, back_surf->w,
            0.0, back_surf->h * 0.5
        };
        double phase_bounds[4] = {
            0.0, back_surf->w,
            back_surf->h * 0.5, back_surf->h
        };

        render_mag_plot(back_surf,  font_p, samples, total_n_samples,
                        mag_bounds, sample_period);
        render_phase_plot(back_surf,  font_p, samples, total_n_samples,
                        phase_bounds, sample_period);

        if (outfile) {
            guint index;
            fprintf(outfile, "# t    magnitude    phase\n");
            for (index = 0; index < total_n_samples; index++) {
                SpectraVector *sample = samples[index];
                fprintf(outfile, "%f    %f    %f\n",
                        sample_period * index,
                        spectra_vector_mag(sample),
                        spectra_vector_phase(sample));
            }
            fclose(outfile);
        }

    } else if (stats.type == SPECTRA_SIGNAL_TYPE_COMPLEX) {
        double real_bounds[4] = {
            0.0, back_surf->w,
            0.0, back_surf->h * 0.5
        };
        double imaginary_bounds[4] = {
            0.0, back_surf->w,
            back_surf->h * 0.5, back_surf->h
        };

        render_real_plot(back_surf, font_p, samples, total_n_samples, real_bounds, sample_period);
        render_imaginary_plot(back_surf, font_p, samples, total_n_samples, imaginary_bounds, sample_period);

        if (outfile) {
            guint index;
            fprintf(outfile, "# t    real    imaginary\n");
            for (index = 0; index < total_n_samples; index++) {
                SpectraVector *sample = samples[index];
                fprintf(outfile, "%f    %f    %f\n",
                        sample_period * index,
                        spectra_vector_real(sample),
                        spectra_vector_imaginary(sample));
            }
            fclose(outfile);
        }
    } else if (stats.type == SPECTRA_SIGNAL_TYPE_REAL) {
        double real_bounds[4] = {
            0.0, back_surf->w,
            0.0, back_surf->h
        };
        render_real_plot(back_surf, font_p, samples, total_n_samples,
                         real_bounds, sample_period);
        if (outfile) {
            guint index;
            fprintf(outfile, "# t    real\n");
            for (index = 0; index < total_n_samples; index++) {
                SpectraVector *sample = samples[index];
                fprintf(outfile, "%f    %f\n",
                        sample_period * index,
                        spectra_vector_real(sample));
            }
            fclose(outfile);
        }
    }

    struct audio_output ao;
    if (play_audio) {
        GPtrArray *samp_arr =
            g_ptr_array_new_take((gpointer*)samples, total_n_samples,
                                 g_object_unref);
        if (audio_init(&ao, 1, s_freq, samp_arr) == 0)
            audio_start(&ao);
        g_ptr_array_unref(samp_arr);
    } else {
        g_free(samples);
    }
    samples = NULL;

    SDL_Event event;
    printf("entering window event loop\n");
    while (SDL_WaitEvent(&event)) {
        switch (event.type) {
        case SDL_QUIT:
            printf("quit event received\n");
            break;
        case SDL_WINDOWEVENT:
            switch (event.window.event) {
            case SDL_WINDOWEVENT_EXPOSED:
                printf("expose event received, redrawing!\n");
                refresh_window(winp, back_surf);
            }
            continue;
        default:
            continue;
        }
        break;
    }

    if (back_surf)
        SDL_FreeSurface(back_surf);

    if (font_p)
        TTF_CloseFont(font_p);

    printf("exiting.\n");
    TTF_Quit();
    SDL_Quit();
}

