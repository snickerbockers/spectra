/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef DFT_H_
#define DFT_H_

#include <glib.h>
#include "signal.h"

#define SPECTRA_TYPE_DFT_SIGNAL (spectra_dft_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraDftSignal, spectra_dft_signal,
                     SPECTRA, DFT_SIGNAL, SpectraSampledSignal)

SpectraSignal *spectra_dft_ctor(SpectraConstantBuffer *cbuf,
                                gchar const *ident,
                                GPtrArray *prereq);

#endif
