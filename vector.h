/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef VECTOR_H_
#define VECTOR_H_

#include <glib.h>
#include <glib-object.h>

typedef double spectra_scalar;

#define SPECTRA_TYPE_VECTOR (spectra_vector_get_type())
G_DECLARE_DERIVABLE_TYPE(SpectraVector, spectra_vector,
                         SPECTRA, VECTOR, GObject)

struct spectra_vector_vtable {
    GString*(*to_str)(SpectraVector *vec);
    SpectraVector*(*add)(SpectraVector *lhs, SpectraVector *rhs);
    SpectraVector*(*sub)(SpectraVector *lhs, SpectraVector *rhs);
    SpectraVector*(*mult)(SpectraVector *lhs, SpectraVector *rhs);
    SpectraVector*(*div)(SpectraVector *lhs, SpectraVector *rhs);
    SpectraVector*(*exp)(SpectraVector *logarithm);
    SpectraVector*(*ln)(SpectraVector *val);

    SpectraVector*(*scale)(SpectraVector *lhs, spectra_scalar rhs);

    SpectraVector*(*as_real)(SpectraVector *vecp);
};

struct _SpectraVectorClass {
    GObjectClass parent_class;

    struct spectra_vector_vtable vtable;

    GPtrArray *axes;
    gchar *name;
};

enum spectra_vector_spec_prop_id {
    SPECTRA_VECTOR_DATA = 1
};

SpectraVector *spectra_vector_new_full(GType type, GArray *data);

SpectraVector *spectra_vector_new(GType type, ...);

/*******************************************************************************
 **
 ** get value of a given dimension, by index
 **
 ******************************************************************************/
spectra_scalar spectra_vector_index(SpectraVector *vec, guint index);

guint spectra_vector_len(SpectraVector const *vec);

/*
 * returns NULL if index is out-of-bounds,
 * otherwise returns pointer to axis name.
 *
 * ownership of the returned pointer **DOES NOT** belong to the caller.
 */
gchar const *spectra_vector_axis(SpectraVector const *vec, guint index);

/*******************************************************************************
 **
 ** define a new vector type with the given name
 **
 ******************************************************************************/
GType spectra_vector_type_full(gchar const *name,
                               struct spectra_vector_vtable vtable, GPtrArray *axes);


/*******************************************************************************
 **
 ** return a vector type based on its name.  If it does not exist then it will
 ** not be created.
 **
 ******************************************************************************/
GType spectra_vector_find_type_by_name(gchar const *name);

/*
 * wrapper for spectra_vector_type_full
 * it will automatically construct a GPtrArray of axis names from the
 * variable arguments, pass it to spectra_Vector_type_full, then unref
 * it.
 *
 * The final argument must be NULL.
 */
GType spectra_vector_type(gchar const *name, struct spectra_vector_vtable vtable, ...);


/*******************************************************************************
 **
 ** operators
 ** these all return a new vector
 **
 ******************************************************************************/
SpectraVector *spectra_vector_add(SpectraVector *lhs, SpectraVector *rhs);
SpectraVector *spectra_vector_sub(SpectraVector *lhs, SpectraVector *rhs);
SpectraVector *spectra_vector_mult(SpectraVector *lhs, SpectraVector *rhs);
SpectraVector *spectra_vector_div(SpectraVector *lhs, SpectraVector *rhs);
SpectraVector *spectra_vector_scale(SpectraVector *lhs, spectra_scalar rhs);
SpectraVector *spectra_vector_exp(SpectraVector *logarithm);
SpectraVector *spectra_vector_ln(SpectraVector *value);

spectra_scalar spectra_vector_phase(SpectraVector *vecp);
spectra_scalar spectra_vector_mag(SpectraVector *vecp);

GString *spectra_vector_gstring(SpectraVector *vecp);
gchar *spectra_vector_str(SpectraVector *vecp); // you must g_free the pointer

/*******************************************************************************
 **
 ** SpectraReal - one-dimensional vector representing a real number
 **               this type is instantiated at runtime from main.  These
 **               functions exist only for convenience.
 **
 ******************************************************************************/
#define SPECTRA_TYPE_REAL (spectra_vector_find_type_by_name("spectra_real"))
#define SPECTRA_VECTOR_IS_REAL(vecp) \
    (G_TYPE_CHECK_INSTANCE_TYPE(vecp, SPECTRA_TYPE_REAL))
SpectraVector *spectra_real_new(spectra_scalar value);

/*******************************************************************************
 **
 ** SpectraComplex - two-dimensional vector representing the sum of a real value
 ** and an imaginary value.
 **
 ** Note that there is no "SpectraImaginary" type, so all imaginary numbers are
 ** complex in spectra.
 **
 ******************************************************************************/
#define SPECTRA_TYPE_COMPLEX (spectra_vector_find_type_by_name("spectra_complex"))
#define SPECTRA_VECTOR_IS_COMPLEX(vecp)                         \
    (G_TYPE_CHECK_INSTANCE_TYPE(vecp, SPECTRA_TYPE_COMPLEX))
SpectraVector *spectra_complex_new(spectra_scalar real,
                                   spectra_scalar imaginary);

SpectraVector *spectra_complex_exponential_new(spectra_scalar magnitude,
                                               spectra_scalar phase_angle);

/*
 * returns a copy of vecp as a real, if possible.  If not possible,
 * returns NULL.
 *
 * if vecp already is real, it will return a new vector that is a copy
 * of vecp (so that it has the same semantics as cases when vecp is
 * not real).
 *
 * If it is not possible for the vector to be represented as a real
 * value, this function will return NULL.
 */
SpectraVector *spectra_vector_as_real(SpectraVector *vecp);

/*******************************************************************************
 **
 ** returns the real part of the given vector.
 ** WARNING: THIS FUNCTION IS ENTIRELY UNRELATED TO
 ** spectra_vector_as_real.  This function returns the real-part of
 ** vecp and discards the imaginary part. spectra_vector_as_real
 ** attempts to represent the given vector without changing the value,
 ** if possible.
 **
 ******************************************************************************/
spectra_scalar spectra_vector_real(SpectraVector *vecp);
spectra_scalar spectra_vector_imaginary(SpectraVector *vecp);

#endif
