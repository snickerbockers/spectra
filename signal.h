/*******************************************************************************
 *
 *
 *    Copyright (C) 2024 snickerbockers
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 ******************************************************************************/

#ifndef SIGNAL_H_
#define SIGNAL_H_

#include <glib.h>
#include <glib-object.h>

#include "cbuf.h"
#include "vector.h"

#include "program.h"

G_BEGIN_DECLS

struct symbol_table; // see parser.h
struct prog_sym;     // see parser.h

/*******************************************************************************
 **
 ** signal - high-level abstraction for data source
 **          generally it's goign to be either a set of sampled data
 **          or a symbolic equation that's been parsed out and compiled into
 **          a program (see tokens.[ch] and parser.[ch]).
 **
 **          The ones that are symbolic are continuous functions that can be
 **          evaluated at any point.  The ones that are sampled would've either
 **          been loaded from disc (like an audio file) or they would've  been
 **          sampled from another signal which may or may not have been
 **          symbolic.
 **
 **          ALL SIGNALS ARE IMMUTABLE AND CANNOT BE CHANGED AFTER
 **          THEY ARE INITIALLY CONSTRUCTED.  As a result of this, it
 **          is possible for more than one signal to share the same
 **          subtree.
 **
 **          There are several *_new and *_new_full functions which
 **          accept GPtrArrays of prequisite signals.  Once one of
 **          these is called you must never ever make a change to that
 **          GPtrArray ever again because doing so would violate the
 **          immutability of all Signals that hold references to that array.
 **
 ******************************************************************************/
#define SPECTRA_TYPE_SIGNAL (spectra_signal_get_type())
G_DECLARE_DERIVABLE_TYPE(SpectraSignal, spectra_signal, SPECTRA, SIGNAL, GObject)

struct _SpectraSignalClass {
    GObjectClass parent_class;

    /***************************************************************************
     **
     ** compile_node points to a function which will compile the given
     ** signal into the given program.  This node will not be the only
     ** node compiled into the given program.  Note that, because a
     ** signle node may be part of more than one parent-Signal, this
     ** function may be called more than once and all implementations
     ** must keep in mind that they are not necessarily the only or
     ** the first node compiled into the program.
     **
     ** sigp           - signal to compile; the "this pointer" in C++ parlance
     ** prog           - the destination program that this node is going to be
     **                  compiled into.  Note that this will be a
     **                  different program on subsequent invocations
     **                  because signals are allowed to share prerequisite nodes
     ** prereq_outputs - array of guint indices representing the
     **                  outputs of all prerequisite nodes.  All
     **                  prerequisite nodes will already have been
     **                  compiled before this function is invoked.
     **
     ** return value   - index of where the output of this node is written to
     **                  to program memory.  This address will be
     **                  referenced by subsequent nodes to which sigp
     **                  is a prerequisite.
     **
     **************************************************************************/
    guint(*compile_node)(SpectraSignal *sigp, SpectraProgram *prog,
                         GArray *prereq_outputs);

    // to_str should return a string that could be compiled into a new statement
    GString*(*to_str)(SpectraSignal *sigp);

    /* summary should return a string that conveys what the signal represents to
     * the user, but is not necessarily a valid spectra expression.  For
     * example, you might use unicode characters to get all the special symbols
     * instead of using textual representations, or you might put in a '...' to
     * abbreviate a long and repetitive sequence.  Or maybe, you could simplify
     * long complicated statements by introducing substitution variables.  The
     * possibilities are truly endless.
     *
     * this function is optional as long as to_str is implemented.
     * SpectraSignal has a default implementation which just passes the buck
     * along to to_str.
     *
     * Also each signal's implementation is still responsible for recursively
     * integrating its prerequisite signals into the string just like to_str
     * does; however since the rules are more relaxed for summary, you may
     * bypass the prerequisites and insert your own representations for them.
     * just make sure it works and does not change the meaning of the original
     * signals.
     */
    GString*(*summary)(SpectraSignal *sigp);

    /***************************************************************************
     **
     ** default behavior - recurse into all prerequisite nodes'
     ** accumulate_inputs implementations and accumulate them all together.
     ** Certain signals may have specific requirements, but most will only need
     ** to combine the inputs of their prerequisites.
     **
     ** When overriding this method, take care to not allow any input to be
     ** present more than once - the inputs array is meant to be a *set*.
     ** Implementations that need to override the default implementation must
     ** recurse into prerequisite nodes themselves if they don't chain-up to the
     ** default implementation or don't allow for prerequisites.
     **
     **************************************************************************/
    void (*accumulate_inputs)(SpectraSignal *sigp, GPtrArray *inputs);

    /*
     * padding, in case i ever find myself locked into maintaining a consistent
     * ABI for some currently-unknown reason.  Not that I ever expect to need this.
     */
    gpointer vtable_unused[8];
};

/*
 * THE VALUE RETURNED BY THIS FUNCTION MAY BE 0.0 !!!!
 *
 * this would generally be the case for symbolic signals, as they are all
 * continuous.
 */
spectra_scalar spectra_signal_sample_period(SpectraSignal *sigp);

/*******************************************************************************
 **
 ** compiles this signal into a program and returns the given program
 **
 ** The program compiled will be cached for the lifetime of the signal (this is
 ** safe to do since all signals and programs are immutable) and thus subsequent
 ** calls to spectra_signal_compile will merely return a new references to the
 ** same SpectraProgram.
 **
 ******************************************************************************/
SpectraProgram *spectra_signal_compile(SpectraSignal *sigp);

/*******************************************************************************
 **
 ** return all of the inputs accepted by this signal, in string form.
 **
 ** THE POINTER RETURNED BY THIS FUNCTION BELONGS TO THE CALLER AND MUST BE
 ** FREE'D BY THE CALLER USING g_free.
 **
 ******************************************************************************/
gchar *spectra_signal_all_inputs(SpectraSignal *sigp);

// returns true if the given input is on sigp's inputs list
gboolean spectra_signal_check_input(SpectraSignal *sigp,
                                    gchar const *input_name);

guint spectra_signal_prereq_count(SpectraSignal *sigp);
SpectraSignal *spectra_signal_prereq(SpectraSignal *sigp, guint index);

GString *spectra_signal_to_string(SpectraSignal *sigp);

GString *spectra_signal_summary(SpectraSignal *sigp);

// does not return a new reference
SpectraConstantBuffer *spectra_signal_cbuf(SpectraSignal *sigp);

/*
 * returns a new reference to a GPtrArray of *all* inputs needed by this signal
 * and its prerequisites.  don't forget to g_ptr_array_unref it when you're
 * done.
 */
GPtrArray* spectra_signal_inputs(SpectraSignal *sigp);

/*******************************************************************************
 **
 ** SampledSignal - a discrete train of impulses
 **
 ** There are a few different ways to get one of these but they're all mostly
 ** the same once they've been generated/loaded so we make this class visible
 ** outside of signal.c and allow for derived classes
 **
 ******************************************************************************/
#define SPECTRA_TYPE_SAMPLED_SIGNAL (spectra_sampled_signal_get_type())
G_DECLARE_DERIVABLE_TYPE(SpectraSampledSignal, spectra_sampled_signal, SPECTRA,
                         SAMPLED_SIGNAL, SpectraSignal)

struct _SpectraSampledSignalClass {
    SpectraSignalClass parent_class;

    /*
     * padding, in case i ever find myself locked into maintaining a consistent
     * ABI for some currently-unknown reason.  Not that I ever expect to need
     * this.
     */
    gpointer vtable_unused[8];
};

enum spectra_sampled_signal_prop_id {
    SPECTRA_SAMPLED_SIGNAL_PROP_VECS = 1,
};

guint spectra_sampled_signal_len(SpectraSampledSignal *sigp);

/*
 * This returns a new reference.  YOU MUST CALL g_object_unref ON THE
 * RETURNED VALUE.
 */
SpectraVector *spectra_sampled_signal_index(SpectraSampledSignal *sigp, guint index);

/*
 * This returns a new reference.  YOU MUST CALL g_ptr_array_unref ON THE
 * RETURNED VALUE.
 */
GPtrArray *spectra_sampled_signal_data(SpectraSampledSignal *sigp);

/*******************************************************************************
 **
 ** LiteralSignal - just a constant value
 **
 ******************************************************************************/
#define SPECTRA_TYPE_LITERAL_SIGNAL (spectra_literal_signal_get_type())
G_DECLARE_FINAL_TYPE(SpectraLiteralSignal, spectra_literal_signal, SPECTRA,
                     LITERAL_SIGNAL, SpectraSignal)

// literal-signal doesn't need inputs because it can never have prerequisites
SpectraSignal *spectra_literal_signal_new(SpectraConstantBuffer *cbuf, SpectraVector *litp);
SpectraVector *spectra_literal_signal_value(SpectraSignal *sigp);

/*******************************************************************************
 **
 ** SumSignal: A + B + C + ...
 **
 ** Note that there is no subtraction signal, just fucking multiply
 ** one of the terms by -1.
 **
 ******************************************************************************/
SpectraSignal *spectra_sum_signal_new_full(SpectraConstantBuffer *cbuf,
                                           GPtrArray *prereq);
SpectraSignal *spectra_sum_signal_new(SpectraConstantBuffer *cbuf,
                                      guint n_operands,
                                      ...);

/*******************************************************************************
 **
 ** MultSignal: A * B * C * ...
 **
 ******************************************************************************/
SpectraSignal *spectra_mult_signal_new_full(SpectraConstantBuffer *cbuf,
                                            GPtrArray *prereq);

SpectraSignal *spectra_mult_signal_new(SpectraConstantBuffer *cbuf,
                                       guint n_operands,
                                       ...);

/*******************************************************************************
 **
 ** DivSignal: dividend / divisor
 **
 ******************************************************************************/
SpectraSignal *spectra_div_signal_new(SpectraConstantBuffer *cbuf,
                                      SpectraSignal *dividend,
                                      SpectraSignal *divisor);

/*******************************************************************************
 **
 ** SpectraSymbolSignal - signal representing a function or variable
 **
 ******************************************************************************/
// no inputs parameter because this class will never have any sub-classes
SpectraSignal *spectra_symbol_signal_new(SpectraConstantBuffer *cbuf,
                                         gchar const *symbol);

/*******************************************************************************
 **
 ** InvokeSignal - calls a function
 **
 ******************************************************************************/
SpectraSignal *spectra_invoke_signal_new_full(SpectraConstantBuffer *cbuf,
                                              gchar const *symbol,
                                              GPtrArray *arguments);

/*******************************************************************************
 **
 ** StringSignal - a pseudo-signal for wrapping strings
 **                it has a constant "value" that is visible to its
 **                superior node, but the real purpose is to
 **                encapsulate a string.  strings are only allowed to
 **                exist as literals within the context of a function
 **                invocation.
 **
 ******************************************************************************/
SpectraSignal *
spectra_string_signal_new_full(SpectraConstantBuffer *cbuf, gchar const *text);
SpectraSignal *spectra_string_signal_new(SpectraConstantBuffer *cbuf,
                                         gchar const *text);

/*
 * "generic plugin" operations which syntactically resemble function calls
 *
 * this wraps a string->pointer map where we look up the given
 * function name to get a pointer to a constructor function  and
 * call the constructor to return a new SpectraSignal representing that
 * function.
 *
 * this differs from the SpectraInvokeSignal in that SpectraInvokeSignal merely
 * passes raw samples to an opaque C-function whereas this represents a means to
 * perform symbolic operations and simplify expressions using known identity
 * rules for specific functions.
 *
 * Implementations can return any type of SpectraSignal they want, including
 * SpectraInvokeSignal.  In fact, all the SpectraInvokeSignals come from here.
 * SpectraInvokeSignal is just the lazy, imprecise way to implement a function
 * call.
 *
 */
SpectraSignal *spectra_function_signal_new_full(SpectraConstantBuffer *cbuf,
                                                gchar const *ident,
                                                GPtrArray *prereq);

void
spectra_signal_add_input_to_set(GPtrArray *set, gchar const *input);

G_END_DECLS

#endif
